/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 25.08.2004
 * Time: 10:33:05
 */
package cs.impl.config;

import cs.api.directory.DirectoryService;
import cs.api.logging.MessageLogger;
import cs.api.validation.ValidationException;
import cs.impl.util.GenericMBeanWrapper;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dom4j.DocumentException;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.net.URL;
import java.security.Provider;
import java.security.Security;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

public class ConfigurationImpl implements ConfigurationImplMBean {
    private static final Logger LOG = Logger.getLogger(ConfigurationImpl.class);

    public static final ObjectName NAME;
    static {
        try {
            NAME = new ObjectName("cs.impl:service=Configuration");
        } catch (MalformedObjectNameException e) {
            throw new Error(e);
        }
    }

    private Config instance;
    private String redirectURL;
    private URL messageTypesLocation;

    private ObjectName dsName;
    private DirectoryService dsImpl;

    private ObjectName logName;
    private MessageLogger logImpl;
    private String administrationEMail;
    private boolean providerLoaded;

    public ConfigurationImpl() {
    }

    public void start() {
        instance = new Config();
        final Provider provider = Security.getProvider("BC");
        final BouncyCastleProvider bcProv = new BouncyCastleProvider();
        if (bcProv.getVersion() < 1.57d) {
            throw new RuntimeException(String.format("Setup Error: BouncyCastle version (%s) is outdated. Startup canceled.", bcProv.getVersion()));
        }
        if (provider == null) {
            providerLoaded = true;
            Security.addProvider(bcProv);
        } else if (provider.getVersion() != bcProv.getVersion()) {
            if (provider.getVersion() < bcProv.getVersion()) {
                LOG.error(String.format("BouncyCastle version differs from expected version: %s vs. %s", provider.getVersion(), bcProv.getVersion()));
            } else {
                LOG.warn(String.format("BouncyCastle version differs from expected version: %s vs. %s", provider.getVersion(), bcProv.getVersion()));
            }
            if (Boolean.getBoolean("cs.crypto.replace-outdated-bcprov")) {
                Security.removeProvider("BC");
                Security.addProvider(bcProv);
                providerLoaded = true;
                LOG.info(String.format("System's (%s) BouncyCastle Provider replaced with %s", provider.getClass().getProtectionDomain().getCodeSource().getLocation(), bcProv.getClass().getProtectionDomain().getCodeSource().getLocation()));
            }
        }

        // solve strange classloading problem by early-init here
        de.esslingen.mediakomm.osci.org.apache.xml.security.Init.init();

        if (!hasUnlimitedCryptoStrength()) {
            LOG.error("Unlimited Crypto Strength not available. Please install Java Unrestricted Cryptography Policy files.");
            throw new RuntimeException("Java Setup Error: Unlimited Crypto Strength not available. Startup canceled.");
        }
    }

    public static boolean hasUnlimitedCryptoStrength() {
        final String TEST_ALGORITHM = "AES";
        final int TEST_KEYSIZE = 256;
        try {
            javax.crypto.Cipher c = javax.crypto.Cipher.getInstance(TEST_ALGORITHM);
            javax.crypto.KeyGenerator kg = javax.crypto.KeyGenerator.getInstance(TEST_ALGORITHM);
            kg.init(TEST_KEYSIZE);
            java.security.Key k = kg.generateKey();
            c.init(javax.crypto.Cipher.ENCRYPT_MODE, k);
            return true;
        } catch (Exception e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Probing crypto strength failed", e);
            }
        }
        return false;
    }

    public void stop() {
        final Provider provider = Security.getProvider("BC");
        if (providerLoaded && provider != null && provider.getClass().getClassLoader() == getClass().getClassLoader()) {
            Security.removeProvider("BC");
        }
        instance = null;
        dsImpl = null;
        logImpl = null;
    }

    public static Configuration getInstance() {
        try {
            return (Configuration)GenericMBeanWrapper.create(NAME, Configuration.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setAdministrationEMail(String address) {
        administrationEMail = address;
    }

    public String getAdministrationEMail() {
        return administrationEMail;
    }

    public boolean isRelaxedInvalidMessagePolicy() {
        return instance.isAcceptInvalidMessages();
    }

    public void setRelaxedInvalidMessagePolicy(boolean b) {
        instance.setAcceptInvalidMessages(b);
    }

    public String getMessageIdPolicy() {
        return String.valueOf(instance.getMessageIdPolicy());
    }

    public String getMessageIdPrefix() {
        return instance.getMessageIdPrefix();
    }

    public void setMessageIdPolicy(String p) {
        instance.setMessageIdPolicy(Config.MessageIdPolicy.valueOf(p));
    }

    public void setMessageIdPrefix(String s) {
        instance.setMessageIdPrefix(s);
    }

    public URL getResourceURL() {
        return instance.getResourceURL();
    }

    public void setResourceURL(URL resourceURL) throws Exception {
        instance = new Config(resourceURL);
        messageTypesLocation = null;
    }

    public Collection getMessageTypes() {
        return instance.getMessageTypes();
    }

    public ContextMgmtData getClientMgmtData() {
        return instance.getClientMgmtData();
    }

    public ContextMgmtData getImMgmtData() {
        return instance.getImMgmtData();
    }

    public boolean isMessageValidation() {
        return instance.isMessageValidation();
    }

    public void setMessageValidation(boolean messageValidation) {
        instance.setMessageValidation(messageValidation);
    }

    public OSCIContext getClientContext(URL imURL, String imAlias, String originator) throws OSCIContextConfigurationException {
        return instance.getClientContext(imURL, imAlias, originator);
    }

    public OSCIContext getSupplierContext() throws OSCIContextConfigurationException {
        return instance.getSupplierContext();
    }

    public DirectoryService getDirectoryServiceImpl() throws ConfigException {
        if (dsImpl == null || !getDirectoryService().equals(dsName)) {
            dsName = getDirectoryService();

            try {
                return dsImpl = (DirectoryService)GenericMBeanWrapper.create(dsName, DirectoryService.class);
            } catch (Exception e) {
                throw new ConfigException(e);
            }
        } else {
            return dsImpl;
        }
    }

    public ObjectName getDirectoryService() {
        return instance.getDirectoryService();
    }

    public void setDirectoryService(ObjectName directoryService) {
        instance.setDirectoryService(directoryService);
    }

    public MessageLogger getLoggerImpl() throws ConfigException {
        if (logImpl == null || !getLoggingService().equals(logName)) {
            logName = getLoggingService();

            try {
                return logImpl = (MessageLogger)GenericMBeanWrapper.create(logName, MessageLogger.class);
            } catch (Exception e) {
                throw new ConfigException(e);
            }
        } else {
            return logImpl;
        }
    }

    public ObjectName getLoggingService() {
        return instance.getLoggingService();
    }

    public boolean isEncryptContent() {
        return instance.isEncryptContent();
    }

    public void setEncryptContent(boolean b) {
        instance.setEncryptContent(b);
    }

    public boolean isSignContent() {
        return instance.isSignContent();
    }

    public void setSignContent(boolean b) {
        instance.setSignContent(b);
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String url) {
        if (url != null) {
            redirectURL = url.length() == 0 || url.equals("null") ? null : url;
        } else {
            redirectURL = null;
        }
    }

    public URL getMessageTypesLocation() {
        return messageTypesLocation;
    }

    public void setMessageTypesLocation(URL url) throws DocumentException, ValidationException, ConfigException {
        messageTypesLocation = url;

        if (url != null) {
            LOG.info("MessageTypesLocation = " + url);
            instance.setMessageTypes(Config.loadDocument(url));
        } else {
            LOG.info("Resetting MessageTypes from built-in configuration");
            instance.resetMessageTypes();
        }
    }

    public void setLoggingService(ObjectName n) {
        instance.setLoggingService(n);
    }

    public URL getImURL() {
        return instance.getImURL();
    }

    public void setImURL(URL imURL) {
        instance.setImURL(imURL);
    }

    public String getImAlias() {
        return instance.getImAlias();
    }

    public void setImAlias(String imAlias) {
        instance.setImAlias(imAlias);
    }

    public URL getSupplierURL(String uri) {
        return instance.getSupplierURL(uri);
    }

    public void setSupplierURL(String uri, URL supplierURL) {
        instance.setSupplierURL(uri, supplierURL);
    }

    public Map getSupplierMappings() {
        return instance.supplierMap;
    }

    public void setSupplierMappings(Map map) {
        instance.supplierMap.clear();
        instance.supplierMap.putAll(map);
    }

    public OSCIContext getIntermediaryContext(Properties p) throws OSCIContextConfigurationException {
        return instance.getIntermediaryContext(p);
    }
}