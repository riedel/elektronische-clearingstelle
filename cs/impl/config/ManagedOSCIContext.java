/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 18:22:37
 */
package cs.impl.config;

import cs.impl.util.Tracing;
import de.esslingen.mediakomm.osci.impl.transport.OSCIContextImpl;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;

import java.util.*;

public class ManagedOSCIContext extends OSCIContextImpl {

    private static final Map contextCache = new HashMap();

    private static final Observer cacheCleaner = new Observer() {
        public void update(Observable o, Object arg) {
            synchronized(ManagedOSCIContext.class) {
                final Collection collection = contextCache.values();
                for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
                    ManagedOSCIContext context = (ManagedOSCIContext)iterator.next();
                    if (o == context.mgmtData) {
                        // ensure context is picked up by IntermediaryRuntime.getInstance()
                        context.invalidate();
                        iterator.remove();
                    }
                }
            }
        }
    };

    private boolean myIsValid = true;
    private void invalidate() {
        myIsValid = false;
    }

    private final String myOriginator;
    private final ContextMgmtData mgmtData;

    public static synchronized OSCIContext getInstance(Properties properties, String originator, ContextMgmtData mgmtData)
            throws OSCIContextConfigurationException
    {
        Tracing.startOperation("Create OSCI Context");
        try {
            String imURI = properties.getProperty("intermediary.uri");
            if (imURI == null && "true".equals(properties.getProperty("isIntermediary"))) {
                imURI = "urn:intermediary";
            }
            final String imAlias = properties.getProperty("intermediary.cert.alias");
            final String imKey = imAlias + "@" + imURI;

            final String key = originator != null ? originator + "@" + imKey : imKey;
            if (contextCache.containsKey(key)) {
                return (OSCIContext)contextCache.get(key);
            } else {
                final ManagedOSCIContext context = new ManagedOSCIContext(properties, originator, mgmtData);
                contextCache.put(key, context);
                mgmtData.addObserver(cacheCleaner);
                return context;
            }
        } finally {
            Tracing.endOperation("Create OSCI Context");
        }
    }

    private ManagedOSCIContext(Properties properties, String originator, final ContextMgmtData mgmtData) throws OSCIContextConfigurationException {
        super(new Properties(properties) {
            public String getProperty(String key) {
                return mgmtData.getProperty(key, super.getProperty(key));
            }

            public String getProperty(String key, String def) {
                return mgmtData.getProperty(key, super.getProperty(key, def));
            }
        });

        myOriginator = originator;
        this.mgmtData = mgmtData;
    }
/*
    public String getProperty(String key, String dflt) {
        if (mgmtData != null) {
            return mgmtData.getProperty(key, super.getProperty(key, dflt));
        } else {
            return super.getProperty(key, dflt);
        }
    }
*/
    public String getOriginator() {
        return myOriginator != null ? myOriginator : mgmtData.getOsciKeyAlias();
    }

    public boolean isCheckSignatures() {
        return mgmtData.isCheckSignatures();
    }

    public boolean isSigningTransport() {
        return mgmtData.isSigningTransport();
    }

    public boolean isEncryptingTransport() {
        return mgmtData.isEncryptingTransport();
    }

    public boolean isValidatingContext() {
        return mgmtData.isValidatingContext();
    }

    public boolean equals(Object o) {
        return myIsValid && super.equals(o);
    }

    public boolean isValid() {
        return myIsValid;
    }
}
