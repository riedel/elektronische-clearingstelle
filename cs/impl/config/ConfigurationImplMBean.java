/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 25.08.2004
 * Time: 10:30:46
 */
package cs.impl.config;

public interface ConfigurationImplMBean extends Configuration {
    void start() throws Exception;
    void stop() throws Exception;
}