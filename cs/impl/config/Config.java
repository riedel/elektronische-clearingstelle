/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 15:17:16
 */
package cs.impl.config;

import cs.api.validation.ValidationException;
import cs.impl.osci.*;
import cs.impl.util.Tracing;
import cs.impl.validation.DefaultMessageValidator;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;
import de.esslingen.mediakomm.osci.transport.user.supplier.OSCIContextRegistry;
import org.apache.log4j.Logger;
import org.dom4j.*;
import org.dom4j.io.SAXReader;

import javax.management.ObjectName;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

class Config {
    private static final Logger LOG = Logger.getLogger(Config.class);

    public static final String CONFIG_NAMESPACE = "http://ns.dzbw.de/clearingstelle/config/1.0";

    private final URL resourceURL;
    private final List messageTypes = new ArrayList();

    private boolean messageValidation;
    private ObjectName directoryService;
    private ObjectName messageLogger;

    private final ContextMgmtData clientMgmtData = new ContextMgmtData();
    private URL imURL;
    private String imAlias;

    private boolean encryptContent;
    private boolean signContent;
    private boolean acceptInvalidMessages;

    private MessageIdPolicy myMessageIdPolicy = MessageIdPolicy.STANDARD;
    private String myMessageIdPrefix;

    private final ContextMgmtData imMgmtData = new ContextMgmtData();
    final Map supplierMap = new HashMap();

    Config(URL resURL) throws MalformedURLException, DocumentException, ConfigException, ValidationException {
        loadConfig(resourceURL = resURL);
    }

    Config() {
        Tracing.startOperation("Load Config");
        try {
            loadConfig(resourceURL = getDefaultResource());
        } catch (DocumentException e) {
            throw new Error("Cannot load config file", e);
        } catch (MalformedURLException e) {
            throw new Error("Cannot load config file", e);
        } catch (ConfigException e) {
            throw new Error("Cannot load config file", e);
        } catch (ValidationException e) {
            throw new Error("Cannot load config file", e);
        } finally {
            Tracing.endOperation("Load Config");
        }
    }

    private URL getDefaultResource() throws MalformedURLException {
        final String property = System.getProperty(getClass().getName() + ".config-file");
        return property != null ? new URL(property) : getClass().getResource("config.xml");
    }

    public URL getResourceURL() {
        return resourceURL;
    }

    private void loadConfig(final URL resourceURL) throws DocumentException, MalformedURLException, ConfigException, ValidationException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loading config from " + resourceURL);
        }

        final Document confDoc = loadDocument(resourceURL);

        setMessageTypes(confDoc);

        XPath xPath = confDoc.createXPath("/cs:config/cs:osci/*");
        final List list = xPath.selectNodes(confDoc);
        for (int i = 0; i < list.size(); i++) {
            loadOsci(null, (Element)list.get(i));
        }

        xPath = confDoc.createXPath("/cs:config/cs:directoryservice");
        loadDirectory((Element)xPath.selectSingleNode(confDoc));

        xPath = confDoc.createXPath("/cs:config/cs:loggingservice");
        loadLogger((Element)xPath.selectSingleNode(confDoc));

        xPath = confDoc.createXPath("/cs:config/cs:message-validation");
        final Element element = ((Element)xPath.selectSingleNode(confDoc));
        if (element != null) {
            messageValidation = "true".equals(element.getTextTrim());
        }
    }

    public static Document loadDocument(URL resourceURL) throws DocumentException, ValidationException, ConfigException {
        final DocumentFactory df = createDocumentFactory();
        final Document confDoc = new SAXReader(df).read(resourceURL);
        validate(confDoc);

        return confDoc;
    }

    public static DocumentFactory createDocumentFactory() {
        final DocumentFactory df = new DocumentFactory();
        df.setXPathNamespaceURIs(new HashMap());
        df.getXPathNamespaceURIs().put("cs", CONFIG_NAMESPACE);

        return df;
    }

    public static void validate(Document confDoc) throws ValidationException, ConfigException {
        final DefaultMessageValidator validator = new DefaultMessageValidator() {
            protected String getSchemaName() {
                return getClass().getResource("config.rng").toExternalForm();
            }
        };
        try {
            if (!validator.validate(confDoc)) {
                throw new ConfigException("Config file is invalid");
            }
        } catch (ValidationException e) {
            if (e.isValidityError()) {
                throw new ConfigException("Config file is invalid", e.getCause());
            } else {
                throw e;
            }
        }
    }

    public void resetMessageTypes() throws ConfigException, ValidationException, DocumentException {
        if (resourceURL != null) {
            setMessageTypes(loadDocument(resourceURL));
        } else {
            LOG.warn("Config Resource URL is null. MessageTypes can not be reset to their default values.");
        }
    }

    public void setMessageTypes(Document doc) {
        synchronized (messageTypes) {
            messageTypes.clear();
            final XPath xPath = doc.createXPath("//cs:messagetypes//cs:type");
            final List list = xPath.selectNodes(doc);
            for (int i = 0; i < list.size(); i++) {
                messageTypes.add(new MessageType((Element)list.get(i)));
            }
        }
    }

    private void loadLogger(Element element) throws ConfigException {
        if (element != null) {
            try {
                setLoggingService(new ObjectName(element.getTextTrim()));
            } catch (Exception e) {
                LOG.error("Cannot load MessageLogger " + element.getTextTrim(), e);
                throw new ConfigException(e);
            }
        }
    }

    private void loadDirectory(Element element) throws ConfigException {
        if (element != null) {
            try {
                setDirectoryService(new ObjectName(element.getTextTrim()));
            } catch (Exception e) {
                LOG.error("Cannot load DirectoryService " + element.getTextTrim(), e);
                throw new ConfigException(e);
            }
        }
    }

    private void loadOsci(ContextMgmtData d, Element element) throws MalformedURLException {
        final String name = element.getQName().getName();
        final String parent = element.getParent().getName();

        if ("keystore".equals(name)) {
            final String urlspec = element.attributeValue("url");
            final URL url;
            if (urlspec.indexOf(':') == -1) {
                url = new URL(resourceURL, urlspec);
            } else {
                url = new URL(urlspec);
            }
            d.setOsciKeystore(url);
            d.setOsciKeystoreType(element.attributeValue("type"));
            d.setOsciKeystorePassword(element.attributeValue("password"));
            loadOsciChildren(d, element);
        } else if ("mapping".equals(name) && "supplier".equals(parent)) {
            supplierMap.put(element.element("uri").getTextTrim(), new URL(element.element("url").getTextTrim()));
        } else if ("intermediary".equals(name) && "client".equals(parent)) {
            imAlias = element.attributeValue("alias");
            imURL = new URL(element.element("url").getTextTrim());
        } else if ("sign".equals(name) && "content-security".equals(parent)) {
            signContent = "true".equals(element.getTextTrim());
        } else if ("encrypt".equals(name) && "content-security".equals(parent)) {
            encryptContent = "true".equals(element.getTextTrim());
        } else if ("key".equals(name) && "keystore".equals(parent)) {
            d.setOsciKeyAlias(element.attributeValue("alias"));
            d.setOsciKeyPassword(element.attributeValue("password"));
        } else if ("sign".equals(name) && "transport".equals(parent)) {
            d.setSigningTransport("true".equals(element.getTextTrim()));
        } else if ("encrypt".equals(name) && "transport".equals(parent)) {
            d.setEncryptingTransport("true".equals(element.getTextTrim()));
        } else if ("checkSignatures".equals(name) && "transport".equals(parent)) {
            d.setCheckSignatures("true".equals(element.getTextTrim()));
        } else if ("validate".equals(name) && "transport".equals(parent)) {
            d.setValidatingContext("true".equals(element.getTextTrim()));
        } else if ("property".equals(name)) {
            d.getProperties().put(element.attributeValue("name"), element.attributeValue("value"));
        } else if ("accept-filter".equals(parent)) {
            d.getProperties().put("intermediary.filter." + name + "." + element.attributeValue("type"), element.getTextTrim());
        } else {
            if ("client".equals(name)) {
                d = clientMgmtData;
            } else if ("intermediary".equals(name)) {
                d = imMgmtData;
            }
            loadOsciChildren(d, element);
        }
    }

    private void loadOsciChildren(ContextMgmtData d, Element element) throws MalformedURLException {
        final List list = element.elements();
        for (int i = 0; i < list.size(); i++) {
            Element e = (Element)list.get(i);
            loadOsci(d, e);
        }
    }


    public Collection getMessageTypes() {
        synchronized (messageTypes) {
            return Collections.unmodifiableCollection(messageTypes);
        }
    }

    public ContextMgmtData getClientMgmtData() {
        return clientMgmtData;
    }

    public ContextMgmtData getImMgmtData() {
        return imMgmtData;
    }

    public boolean isMessageValidation() {
        return messageValidation;
    }

    public void setMessageValidation(boolean messageValidation) {
        this.messageValidation = messageValidation;
    }

    public boolean isAcceptInvalidMessages() {
        return acceptInvalidMessages;
    }

    public void setAcceptInvalidMessages(boolean b) {
        acceptInvalidMessages = b;
    }

    public OSCIContext getClientContext(URL imURL, String imAlias, String originator) throws OSCIContextConfigurationException {
        final Properties p = new Properties();
        p.setProperty("intermediary.uri", imURL != null ? imURL.toExternalForm() : "");
        p.setProperty("intermediary.cert.alias", imAlias);
        p.setProperty("accept-plain-text-content", "true"); // GOVELLO-INTEROP
        p.setProperty("directory-provider.impl", DirectoryProvider.Client.class.getName());
        p.setProperty("cryptography-provider.impl", CryptoProvider.Client.class.getName());
        p.setProperty("transport-cryptography-provider.impl", CryptoProvider.Client.class.getName());
        p.setProperty("invalid-message-provider.impl", InvalidMessageHandler.class.getName());

        p.setProperty("accept-invalid-messages", String.valueOf(acceptInvalidMessages));

        if (Tracing.isEnabled()) {
            p.setProperty("transport-provider.proxy", TracingTransportProvider.class.getName());
        }

        return ManagedOSCIContext.getInstance(p, originator, clientMgmtData);
    }

    public OSCIContext getIntermediaryContext(Properties p) throws OSCIContextConfigurationException {
        String path = p.getProperty("http.path-info");
        if (path != null) {
            if (path.length() > 1 && path.endsWith("/")) {
                path = path.substring(0, path.length() - 1);
            }
            String imPathInfo = imMgmtData.getProperty("http.path-info", null);
            if (imPathInfo != null && imPathInfo.trim().length() > 0) {
                if (!imPathInfo.startsWith("/")) {
                    imPathInfo = "/" + imPathInfo;
                }
                if (imPathInfo.length() > 1 && imPathInfo.endsWith("/")) {
                    imPathInfo = imPathInfo.substring(0, imPathInfo.length() - 1);
                }

                if (!path.equals(imPathInfo)) {
                    LOG.warn("Requested Intermediary-Path '" + path + "' does not match configured path '" + imPathInfo + "'");
                    return null;
                }
            }
        }

        p.setProperty("isIntermediary", "true");
        p.setProperty("directory-provider.impl", DirectoryProvider.Intermediary.class.getName());
        p.setProperty("cryptography-provider.impl", CryptoProvider.Intermediary.class.getName());
        p.setProperty("transport-cryptography-provider.impl", CryptoProvider.Intermediary.class.getName());
        p.setProperty("persistence-provider.impl", PersistenceProvider.class.getName());
        p.setProperty("timestamp-provider.impl", TimestampProvider.class.getName());
        p.setProperty("invalid-message-provider.impl", InvalidMessageHandler.class.getName());

        p.setProperty("accept-invalid-messages", String.valueOf(acceptInvalidMessages));

        if (myMessageIdPolicy == MessageIdPolicy.GUID) {
            p.setProperty("intermediary.msg-id-generator.class", GuidMessageIdGenerator.class.getName());
        } else if (myMessageIdPolicy == MessageIdPolicy.PREFIX) {
            p.setProperty("intermediary.msg-id-prefix", getMessageIdPrefix());
        } else {
            assert myMessageIdPolicy == MessageIdPolicy.STANDARD;
        }

        if (Tracing.isEnabled()) {
            p.setProperty("transport-provider.proxy", TracingTransportProvider.class.getName());
        }

        return ManagedOSCIContext.getInstance(p, null, imMgmtData);
    }

    public OSCIContext getSupplierContext() throws OSCIContextConfigurationException {
        final Properties p = new Properties();
        p.setProperty("directory-provider.impl", DirectoryProvider.Client.class.getName());
        p.setProperty("cryptography-provider.impl", CryptoProvider.PassiveClient.class.getName());
        p.setProperty("transport-cryptography-provider.impl", CryptoProvider.PassiveClient.class.getName());
        p.setProperty("timestamp-provider.impl", TimestampProvider.class.getName());
        p.setProperty("invalid-message-provider.impl", InvalidMessageHandler.class.getName());

        p.setProperty("accept-invalid-messages", String.valueOf(acceptInvalidMessages));

        final OSCIContext context = ManagedOSCIContext.getInstance(p, null, clientMgmtData);
        final OSCIContextRegistry registry = new OSCIContextRegistry() {
            public OSCIContext getContext(String id) {
                return context;
            }

            public void setContext(String s, OSCIContext context) {
            }
        };
        final PassiveHandler handler = new PassiveHandler();
        context.setAcceptDeliveryHandler(handler, registry);
        context.setProcessDeliveryHandler(handler, registry);
        return context;
    }

    public ObjectName getDirectoryService() {
        return directoryService;
    }

    public void setDirectoryService(ObjectName directoryService) {
        this.directoryService = directoryService;
    }

    public URL getImURL() {
        return imURL;
    }

    public void setImURL(URL imURL) {
        this.imURL = imURL;
    }

    public String getImAlias() {
        return imAlias;
    }

    public void setImAlias(String imAlias) {
        this.imAlias = imAlias;
    }

    public URL getSupplierURL(String uri) {
        return (URL)supplierMap.get(uri);
    }

    public void setSupplierURL(String uri, URL supplierURL) {
        if (supplierURL != null) {
            supplierMap.put(uri, supplierURL);
        } else {
            supplierMap.remove(uri);
        }
    }

    public ObjectName getLoggingService() {
        return messageLogger;
    }

    public void setLoggingService(ObjectName objectName) {
        messageLogger = objectName;
    }

    public boolean isEncryptContent() {
        return encryptContent;
    }

    public boolean isSignContent() {
        return signContent;
    }

    public void setEncryptContent(boolean b) {
        encryptContent = b;
    }

    public void setSignContent(boolean b) {
        signContent = b;
    }

    public String getMessageIdPrefix() {
        return myMessageIdPrefix == null ? "ecs-msg-id#" : myMessageIdPrefix;
    }

    public MessageIdPolicy getMessageIdPolicy() {
        return myMessageIdPolicy;
    }

    public void setMessageIdPolicy(MessageIdPolicy policy) {
        if (policy == null) {
            myMessageIdPolicy = MessageIdPolicy.STANDARD;
            return;
        }
        myMessageIdPolicy = policy;
    }

    public void setMessageIdPrefix(String messageIdPrefix) {
        myMessageIdPrefix = messageIdPrefix;
    }

    public static class MessageIdPolicy extends cs.impl.util.Enumeration {
        public static final MessageIdPolicy STANDARD = new MessageIdPolicy("STANDARD");
        public static final MessageIdPolicy PREFIX = new MessageIdPolicy("PREFIX");
        public static final MessageIdPolicy GUID = new MessageIdPolicy("GUID");

        protected MessageIdPolicy(String value) {
            super(value);
        }

        public static MessageIdPolicy valueOf(String s) {
            return (MessageIdPolicy)_valueOf(MessageIdPolicy.class, s);
        }
    }
}
