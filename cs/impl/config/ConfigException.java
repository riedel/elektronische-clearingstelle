/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 29.07.2004
 * Time: 11:03:13
 */
package cs.impl.config;

public class ConfigException extends Exception {

    public ConfigException(Throwable exception) {
        super(exception);
    }

    public ConfigException(String msg) {
        super(msg);
    }

    public ConfigException(String s, Throwable cause) {
        super(s, cause);
    }
}