/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 28.07.2004
 * Time: 12:49:02
 */
package cs.impl.config;

import java.util.Map;
import java.util.HashMap;
import java.util.Observable;
import java.net.URL;

public class ContextMgmtData extends Observable {

    private final Map properties = new HashMap();

    private boolean isCheckSignatures;
    private boolean isSigningTransport;
    private boolean isEncryptingTransport;
    private boolean isValidatingContext;

    private URL osciKeystore;
    private String osciKeystorePassword;
    private String osciKeyAlias;
    private String osciKeyPassword;
    private String osciKeystoreType;

    public ContextMgmtData() { }

    public URL getOsciKeystore() {
        return osciKeystore;
    }

    public String getOsciKeystorePassword() {
        return osciKeystorePassword;
    }

    public String getOsciKeyAlias() {
        return osciKeyAlias;
    }

    public String getOsciKeyPassword() {
        return osciKeyPassword;
    }

    public void setOsciKeystore(URL osciKeystore) {
        this.osciKeystore = osciKeystore;
        notifyObservers();
    }

    public void setOsciKeystorePassword(String osciKeystorePassword) {
        this.osciKeystorePassword = osciKeystorePassword;
        notifyObservers();
    }

    public void setOsciKeystoreType(String s) {
        osciKeystoreType = s;
        notifyObservers();
   }

    public String getOsciKeystoreType() {
        return osciKeystoreType != null ? osciKeystoreType : "JKS";
    }

    public void setOsciKeyAlias(String osciKeyAlias) {
        this.osciKeyAlias = osciKeyAlias;
        notifyObservers();
  }

    public void setOsciKeyPassword(String osciKeyPassword) {
        this.osciKeyPassword = osciKeyPassword;
        notifyObservers();
  }

    public boolean isCheckSignatures() {
        return isCheckSignatures;
    }

    public boolean isSigningTransport() {
        return isSigningTransport;
    }

    public boolean isEncryptingTransport() {
        return isEncryptingTransport;
    }

    public boolean isValidatingContext() {
        return isValidatingContext;
    }

    public void setCheckSignatures(boolean checkSignatures) {
        isCheckSignatures = checkSignatures;
  }

    public void setSigningTransport(boolean signingTransport) {
        isSigningTransport = signingTransport;
  }

    public void setEncryptingTransport(boolean encryptingTransport) {
        isEncryptingTransport = encryptingTransport;
  }

    public void setValidatingContext(boolean validatingContext) {
        isValidatingContext = validatingContext;
   }

    public Map getProperties() {
        return properties;
    }

    public void setProperties(Map p) {
        properties.clear();
        properties.putAll(p);
        notifyObservers();
   }

    public String getProperty(String key, String property) {
        return (String)(properties.containsKey(key) ? properties.get(key) : property);
    }

    public void notifyObservers() {
        // TODO: only notify if data is actually changed
        setChanged();
        super.notifyObservers();
    }

    @Override
    public String toString() {
        return "ContextMgmtData{" +
                "isCheckSignatures=" + isCheckSignatures +
                ", isSigningTransport=" + isSigningTransport +
                ", isEncryptingTransport=" + isEncryptingTransport +
                ", isValidatingContext=" + isValidatingContext +
                ", osciKeystore=" + osciKeystore +
                ", osciKeystorePassword='" + osciKeystorePassword + '\'' +
                ", osciKeyAlias='" + osciKeyAlias + '\'' +
                ", osciKeyPassword='" + osciKeyPassword + '\'' +
                ", osciKeystoreType='" + osciKeystoreType + '\'' +
                '}';
    }
}