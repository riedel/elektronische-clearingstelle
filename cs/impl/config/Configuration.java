/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 25.08.2004
 * Time: 10:40:22
 */
package cs.impl.config;

import cs.api.directory.DirectoryService;
import cs.api.logging.MessageLogger;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;

import javax.management.ObjectName;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

public interface Configuration {
    URL getResourceURL();
    void setResourceURL(URL resourceURL) throws Exception;

    Collection getMessageTypes();

    ContextMgmtData getClientMgmtData();
    ContextMgmtData getImMgmtData();

    boolean isMessageValidation();
    void setMessageValidation(boolean messageValidation);

    OSCIContext getClientContext(URL imURL, String imAlias, String originator) throws OSCIContextConfigurationException;
    OSCIContext getSupplierContext() throws OSCIContextConfigurationException;
    OSCIContext getIntermediaryContext(Properties p) throws OSCIContextConfigurationException;

    ObjectName getDirectoryService();
    void setDirectoryService(ObjectName directoryService) throws Exception;
    DirectoryService getDirectoryServiceImpl() throws ConfigException;

    ObjectName getLoggingService();
    void setLoggingService(ObjectName n);
    MessageLogger getLoggerImpl() throws ConfigException;

    URL getImURL();
    void setImURL(URL imURL);

    String getImAlias();
    void setImAlias(String imAlias);

    URL getSupplierURL(String uri);
    void setSupplierURL(String uri, URL supplierURL);

    Map getSupplierMappings();
    void setSupplierMappings(Map map);

    boolean isEncryptContent();
    void setEncryptContent(boolean b);

    boolean isSignContent();
    void setSignContent(boolean b);

    String getRedirectURL();
    void setRedirectURL(String url);

    URL getMessageTypesLocation();
    void setMessageTypesLocation(URL url) throws Exception;

    void setAdministrationEMail(String address);
    String  getAdministrationEMail();

    boolean isRelaxedInvalidMessagePolicy();
    void setRelaxedInvalidMessagePolicy(boolean b);

    String getMessageIdPolicy();
    void setMessageIdPolicy(String p);

    String getMessageIdPrefix();
    void setMessageIdPrefix(String s);
}