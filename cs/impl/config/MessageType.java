/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 15:16:22
 */
package cs.impl.config;

import cs.api.validation.MessageValidator;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.xpath.DefaultNamespaceContext;
import org.joda.time.format.ISODateTimeFormat;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

/**
 * The class that describes a message type. A message type is configured by an XML
 * fragment from the configuration file. It uses simple XPath queries to determine if a
 * Document instance matches a specific type.
 * <p/>
 * A MessageType has the following properties:
 * <ul>
 * <li> A type <tt>Name</tt>. This name can either be statically configured by a
 * <code>name</code>-attribute at the type-element, or dynamically by specifying an XPath
 * expression in the <code>select</code> attribute.
 *
 * <li> A condition that determines if the type matches a certain document. This is an
 * XPath expression that's taken from the <code>condition</code> attribute. The result of
 * this expression is evaluated as a boolean value.
 *
 * <li> An XPath expression that extracts the <tt>sender</tt> from the document.
 *
 * <li> An XPath expression that extracts the <tt>receiver</tt> from the document.
 *
 * <li> An optional <code>MessageValidator</code> instance, that can check a document's validity.
 * This is specified as a classname in the <code>validator</code>-tag. Any attribute on this
 * element is passed to the instance if it exposes a property of the same name according to
 * the JavaBeans specification.
 * </ul>
 *
 * An example message type:
 * <pre>
 * &lt;type name="sometype" condition="/bar/@type='sometype'">
 *   &lt;sender select="/bar/sender"/>
 *   &lt;receiver select="/bar/receiver"/>
 *
 *   &lt;validator>foor.bar.MyValidatorClass&lt;/validator>
 * &lt;/type>
 * </pre>
 */
public class MessageType {
    private static final Logger LOG = Logger.getLogger(MessageType.class);

    private final Element typeElement;
    private final Element receiverXPath;
    private final Element senderXPath;
    private final Element accountingXPath;
    private final Element idXPath;
    private final String family;
    private final Map myMetadata;

    private MessageValidator validator;

    public MessageType(Element typeElement) {
        // namespace context for XPath queries
        this.typeElement = typeElement;
        this.family = typeElement.attributeValue("family");
        this.receiverXPath = (Element)typeElement.selectSingleNode("cs:receiver");
        this.senderXPath = (Element)typeElement.selectSingleNode("cs:sender");
        this.accountingXPath = (Element)typeElement.selectSingleNode("cs:accounting");
        this.idXPath = (Element)typeElement.selectSingleNode("cs:message-id");

        final Element valNode = (Element)typeElement.selectSingleNode("cs:validator");
        loadValidator(valNode);

        final Element metaNode = (Element)typeElement.selectSingleNode("cs:metadata");
        myMetadata = loadMetadata(metaNode);
    }

    private Map loadMetadata(Element metaNode) {
        if (metaNode == null) {
            return Collections.EMPTY_MAP;
        }

        final Map metadata = new HashMap();
        final List nodes = metaNode.selectNodes("cs:value");
        for (int i = 0; i < nodes.size(); i++) {
            final Element element = (Element)nodes.get(i);
            final String name = element.attributeValue("name");

            metadata.put(name, new MetaElement(element));
        }
        return Collections.unmodifiableMap(metadata);
    }

    private void loadValidator(final Element valNode) {
        if (valNode != null && valNode.getTextTrim().length() > 0) {
            try {
                final Class valClass = Class.forName(valNode.getTextTrim());
                final MessageValidator validator = (MessageValidator)valClass.newInstance();

                final Map map = new HashMap();
                final PropertyDescriptor[] descriptors = Introspector.getBeanInfo(valClass).getPropertyDescriptors();
                for (int i = 0; i < descriptors.length; i++) {
                    PropertyDescriptor descriptor = descriptors[i];
                    if (descriptor.getWriteMethod() != null) {
                        map.put(descriptor.getName(), descriptor.getWriteMethod());
                    }
                }
                for (Iterator it = valNode.attributeIterator(); it.hasNext();) {
                    final Attribute attribute = ((Attribute)it.next());
                    final String name = attribute.getName();
                    if (map.containsKey(name)) {
                        ((Method)map.get(name)).invoke(validator, new Object[]{attribute.getValue()});
                    } else {
                        LOG.warn("Validator: Attribute '" + name + "' does not have a setter in " + valClass.getName());
                    }
                }
                this.validator = validator;
            } catch (Exception e) {
                LOG.error("Cannot load Validator class " + valNode.getText(), e);
            }
        }
    }

    /**
     * Tests if a Document instance matches this MessageType.
     *
     * @param doc
     * @return
     */
    public boolean matches(Document doc) {
        final String xpathCondition = typeElement.attributeValue("condition");
        final XPath xPath = createXPath(doc, typeElement, "boolean(" + xpathCondition + ")");
        return "true".equals(xPath.valueOf(doc));
    }

    private static XPath createXPath(Document doc, final Element context, final String xpath) {
        final XPath xPath = doc.createXPath(xpath);
        xPath.setNamespaceContext(new DefaultNamespaceContext(context));
        return xPath;
    }

    public String getTypeName(Document doc) {
        final String typeName = typeElement.attributeValue("name");
        if (typeName != null) {
            return typeName;
        } else {
            final XPath xPath = createXPath(doc, typeElement, typeElement.attributeValue("select"));
            return xPath.valueOf(doc);
        }
    }

    public String getSender(Document doc) {
        return createXPath(doc, senderXPath, senderXPath.attributeValue("select")).valueOf(doc);
    }

    public String getReceiver(Document doc) {
        final XPath xPath = createXPath(doc, receiverXPath, receiverXPath.attributeValue("select"));
        final Object value = xPath.evaluate(doc);
        if (value == null) return null;
        return value instanceof String ? (String)value : xPath.valueOf(doc);
    }

    public String getMessageId(Document doc) {
        return idXPath != null ?
                createXPath(doc, idXPath, idXPath.attributeValue("select")).valueOf(doc) : null;
    }

    public MessageValidator getValidator() {
        return validator;
    }

    public Map getMetadata(Document doc) {
        final Map metadata = new HashMap();
        for (Iterator iterator = myMetadata.entrySet().iterator(); iterator.hasNext(); ) {
            final Map.Entry entry = (Map.Entry)iterator.next();
            metadata.put(entry.getKey(), ((MetaElement)entry.getValue()).getValue(doc));
        }
        return Collections.unmodifiableMap(metadata);
    }

    public String getFamily() {
        return family;
    }

    public String getOsciContainerId() {
        final String id = typeElement.attributeValue("osci-container-id");
        return id != null && id.length() > 0 ? id : "XMELD_DATA"; // backward compatibility
    }

    public String getOsciMessageType() {
        final String type = typeElement.attributeValue("osci-communication-type");
        return type != null && type.length() > 0 ? type : null;
    }

    public Boolean getEncrypt() {
        final String v = typeElement.attributeValue("osci-encrypt");
        return v != null && v.length() > 0 ? "true".equals(v) : null;
    }

    public Boolean getSign() {
        final String v = typeElement.attributeValue("osci-sign");
        return v != null && v.length() > 0 ? "true".equals(v) : null;
    }

    public String getOsciSubject(Document document) {
        final String subjectXPath = typeElement.attributeValue("osci-subject-expr");
        return subjectXPath != null ?
                createXPath(document, typeElement, subjectXPath).valueOf(document) : null;
    }

    public int getAccountingMultiplier(final Document document) {
        if (accountingXPath == null) {
            return 1;
        }
        final String _min;
        final int min;
        if ((_min = accountingXPath.attributeValue("minimum")) != null) {
            min = Integer.parseInt(_min);
        } else {
            min = 0;
        }
        final Object o = createXPath(document, accountingXPath, accountingXPath.attributeValue("select")).evaluate(document);
        return Math.max(min, o instanceof Number ? ((Number)o).intValue() : 0);
    }


    public String toString() {
        return "MessageType{" +
                "typeElement=" + typeElement +
                ", receiverXPath=" + receiverXPath +
                ", senderXPath=" + senderXPath +
                ", idXPath=" + idXPath +
                ", validator=" + validator +
                ", family='" + family + '\'' +
                '}';
    }

    private static class MetaElement {
        private final Element myElement;
        private final TypeMapper myValueAccess;

        interface TypeMapper {
            TypeMapper STRING = new TypeMapper() {
                public Object map(String value) {
                    return value;
                }
            };
            TypeMapper DATE = new TypeMapper() {
                public Object map(String value) {
                    return value != null && value.length() > 0 ? ISODateTimeFormat.dateParser().parseDateTime(value).toDate() : null;
                }
            };
            TypeMapper DATETIME = new TypeMapper() {
                public Object map(String value) {
                    return value != null && value.length() > 0 ? ISODateTimeFormat.dateTimeParser().parseDateTime(value).toDate() : null;
                }
            };
            TypeMapper BOOLEAN = new TypeMapper() {
                public Object map(String value) {
                    return Boolean.valueOf(value);
                }
            };
            TypeMapper DECIMAL = new TypeMapper() {
                public Object map(String value) {
                    return value != null && value.length() > 0 ? new BigDecimal(value) : null;
                }
            };
            Object map(String value);
        }

        public MetaElement(Element element) {
            myElement = element;

            final String type = myElement.attributeValue("type");
            if (type == null || "xs:string".equals(type)) {
                myValueAccess = TypeMapper.STRING;
            }  else if ("xs:date".equals(type)) {
                myValueAccess = TypeMapper.DATE;
            } else if ("xs:dateTime".equals(type)) {
                myValueAccess = TypeMapper.DATETIME;
            } else if ("xs:boolean".equals(type)) {
                myValueAccess = TypeMapper.BOOLEAN;
            } else if ("xs:decimal".equals(type)) {
                myValueAccess = TypeMapper.DECIMAL;
            } else {
                LOG.error("Invalid type: " + type);
                myValueAccess = TypeMapper.STRING;
            }
        }

        public Object getValue(Document document) {
            try {
                final String select = myElement.attributeValue("select");
                if (select != null) {
                    return myValueAccess.map(createXPath(document, myElement, select).valueOf(document));
                } else {
                    return myValueAccess.map(myElement.getTextTrim());
                }
            } catch (Exception e) {
                LOG.error("Failed to extract metadata for " + myElement.attributeValue("name"), e);
                return null;
            }
        }
    }
}