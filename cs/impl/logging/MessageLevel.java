/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 31.08.2004
 * Time: 10:32:52
 */
package cs.impl.logging;

import org.apache.log4j.Level;

public class MessageLevel extends Level {
    public static final MessageLevel DEFAULT_LOGLEVEL = new MessageLevel();

    private static final String PREFIX = "cs.impl.logging.MessageLevel";

    private static final int DEFAULT_LEVEL = DEBUG_INT - 100;
    private static final String DEFAULT_LEVEL_NAME = "MESSAGE";
    private static final int DEFAULT_SYSLOG_LEVEL = -1;

    /**
     * Default confguration via system properties
     */
    private MessageLevel() {
        super(Integer.getInteger(PREFIX + ".level", new Integer(DEFAULT_LEVEL)).intValue(),
                System.getProperty(PREFIX + ".name", DEFAULT_LEVEL_NAME),
                Integer.getInteger(PREFIX + ".syslog-level", new Integer(DEFAULT_SYSLOG_LEVEL)).intValue());
    }

    public MessageLevel(int level, String name, int sysloglevel) {
        super(level, name, sysloglevel);
    }
}