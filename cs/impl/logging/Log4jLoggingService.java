/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 07.09.2004
 * Time: 16:53:37
 */
package cs.impl.logging;

import cs.api.logging.LogMessage;
import cs.persistence.management.PersistenceSupport;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Simple Log4j-based MessageLogger implementation.
 * This class just logs a message with a specific level
 * to the log4j subsystem.
 *
 * The level-priority and name can be adjusted by the management interface.
 *
 * @see MessageLevel
 */
public class Log4jLoggingService extends PersistenceSupport implements Log4jLoggingServiceMBean {
    private static final Logger LOG = Logger.getLogger(Log4jLoggingService.class);

    private MessageLevel level = MessageLevel.DEFAULT_LOGLEVEL;

    public void logMessage(LogMessage message) {
        LOG.log(level, message);
    }

    public void logStatistics(Date date, LogMessage.Direction direction, String errorCode, String messageId, String type, String sender, String receiver, String typeSpecificId) {
        LOG.log(level, "[STATISTICS] " + date + ", " + direction + ", " + errorCode + ", " + messageId + ", " +
                type + ", " + sender + ", " + receiver + ", " + typeSpecificId);
    }

    public String getLevelName() {
        return level.toString();
    }

    public int getMessageLevel() {
        return level.toInt();
    }

    public int getSyslogLevel() {
        return level.getSyslogEquivalent();
    }

    public void setLevelName(String name) {
        level = new MessageLevel(getMessageLevel(), name, getSyslogLevel());
    }

    public void setMessageLevel(int lvl) {
        level = new MessageLevel(lvl, getLevelName(), getSyslogLevel());
    }

    public void setSyslogLevel(int lvl) {
        level = new MessageLevel(getMessageLevel(), getLevelName(), lvl);
    }
}