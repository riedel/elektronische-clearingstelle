/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 07.09.2004
 * Time: 17:11:18
 */
package cs.impl.logging;

import cs.api.logging.MessageLogger;

import javax.management.PersistentMBean;

public interface Log4jLoggingServiceMBean extends MessageLogger, PersistentMBean {
    void start() throws Exception;
    void stop() throws Exception;

    int getMessageLevel();
    void setMessageLevel(int lvl);

    String getLevelName();
    void setLevelName(String name);

    int getSyslogLevel();
    void setSyslogLevel(int lvl);
}