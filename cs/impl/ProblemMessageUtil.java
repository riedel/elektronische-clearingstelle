/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 14.09.2006
 * Time: 14:40:57
 */
package cs.impl;

import cs.api.*;
import cs.api.transport.osci.OSCICommunicationType;
import cs.api.transport.osci.OSCIInfo;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.osci.ActiveHandler;
import cs.impl.util.GenericMBeanWrapper;
import cs.impl.util.TransactionRunner;
import cs.persistence.monitoring.ejb.client.ProblemMessageLookup;
import cs.persistence.monitoring.ejb.client.ProblemMessageManager;
import cs.persistence.monitoring.ejb.client.ProblemMessageVO;
import de.esslingen.mediakomm.osci.impl.transport.interfaces.OSCIDocumentMessage;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.OSCIMessage;
import de.esslingen.mediakomm.osci.transport.document.OSCIDocument;
import de.esslingen.mediakomm.osci.transport.document.OSCIMessageDocument;
import de.esslingen.mediakomm.osci.transport.user.client.OSCIResponseToFetchDelivery;
import edu.emory.mathcs.backport.java.util.concurrent.Callable;
import org.apache.log4j.Logger;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.*;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ProblemMessageUtil {
    private static final Logger LOG = Logger.getLogger(ProblemMessageUtil.class);

    // TODO: place into JNDI (or access via stateful session bean?)
    private static final ThreadLocal<ProblemMessageVO> RESENDING = new ThreadLocal<ProblemMessageVO>();
    private static final ThreadLocal<ProblemMessageVO> UPDATED = new ThreadLocal<ProblemMessageVO>();

    public static final ThreadLocal<ErrorCode> LAST_ERROR = new ThreadLocal<ErrorCode>();

    public static final ObjectName AUTO_SEND_MANAGER_NAME;
    static {
        try {
            AUTO_SEND_MANAGER_NAME = new ObjectName("cs.monitoring:service=AutoSendManager");
        } catch (MalformedObjectNameException e) {
            throw new Error(e);
        }
    }

    public interface MessageSender {
        boolean isEnabled();
        void setEnabled(boolean enabled);
        void scheduleForDelivery(ProblemMessageVO id) throws RemoteException;
    }

    public static final Set<String> RESENDABLE_CODES = new HashSet<String>();
    static {
        ProblemMessageUtil.RESENDABLE_CODES.add(ErrorCode.IO_FAILURE.toString());
        ProblemMessageUtil.RESENDABLE_CODES.add(ErrorCode.PLUGIN_FAILURE.toString());
        ProblemMessageUtil.RESENDABLE_CODES.add(ErrorCode.UNKNOWN_DESTINATION.toString());
        ProblemMessageUtil.RESENDABLE_CODES.add(ErrorCode.UNKNOWN_TYPE.toString());
        ProblemMessageUtil.RESENDABLE_CODES.add(ErrorCode.OSCI_FAILURE.toString());
    }

    public static void resendMessage(ProblemMessageVO msg, ClearingstelleImplMBean ecs) throws CsException {
        final String id = msg.getId();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Resending message <" + id + ">");
        }

        final Object o = RESENDING.get();
        if (o != null) {
            throw new AssertionError("Resend already in progress for current thread. Cannot continue.");
        }

        synchronized (resendLock(id)) {
            _resendMessage(msg, ecs);
        }
    }

    private static void _resendMessage(ProblemMessageVO msg, ClearingstelleImplMBean ecs) throws CsException {
        final ProblemMessageManager problemManager = ProblemMessageLookup.getManager();
        try {
            if (problemManager.findMessage(msg.getId()) == null) {
                // ignore messages that have been handled somewhere else
                return;
            }
        } catch (RemoteException e) {
            LOG.error("findMessage() failed for <" + msg.getId() + ">", e);
        }

        try {
            RESENDING.set(msg);

            try {
                if (msg.getBaseType().isXml()) {
                    final String senderId = msg.getSenderId();
                    final int mode = msg.getDirection() == ProblemMessageVO.Direction.INCOMING ?
                            Clearingstelle.MODE_LOCAL_ONLY : Clearingstelle.MODE_REMOTE_ONLY;

                    final Message m = loadMetadata(msg);
                    final byte[] messageBytes;
                    final String originator;
                    if (m != null) {
                        messageBytes = m.getMessage();
                        originator = m instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)m).getOriginator() : null;
                    } else {
                        messageBytes = loadMessage(msg);
                        originator = null;
                    }

                    final String type = msg.getType();
                    final ProblemMessageVO.Direction direction = msg.getDirection();
                    try {
                        ecs.sendMessage(senderId, messageBytes, mode, msg.getJobID(), originator);
                    } catch (CsException e) {
                        if (m != null) {
                            createMessage(senderId, msg.getJobID(), m, direction, e.getCode(), e);
                        } else {
                            createMessage(senderId, msg.getJobID(), msg.getId(), messageBytes, type, direction, e.getCode(), e);
                        }
                        throw e;
                    } catch (Exception e) {
                        if (m != null) {
                            createMessage(senderId, msg.getJobID(), m, direction, ErrorCode.valueOf(msg.getError()), e);
                        } else {
                            createMessage(senderId, msg.getJobID(), msg.getId(), messageBytes, type, direction, ErrorCode.valueOf(msg.getError()), e);
                        }
                        throw new RuntimeException("Cannot redeliver message '" + msg.getId() + "'", e);
                    }
                } else {
                    assert msg.getBaseType() == ProblemMessageVO.Type.OSCI;

                    final Configuration config = ConfigurationImpl.getInstance();
                    final OSCIContext osciContext = config.getClientContext(config.getImURL(), config.getImAlias(), null);
                    final byte[] bytes = ProblemMessageUtil.getMessageBytes(msg);
                    final OSCIDocument osciDocument = osciContext.getStorageManager().loadMessage(new ByteArrayInputStream(bytes));
                    final OSCIMessage osciMessage = osciContext.getClientDeliveryFactory().createMessage((OSCIMessageDocument)osciDocument);
                    if (osciMessage instanceof OSCIResponseToFetchDelivery) {
                        ActiveHandler.getInstance().processResponse((OSCIResponseToFetchDelivery)osciMessage);
                    } else {
                        throw new RuntimeException("OSCI-Nachricht kann nicht verarbeitet werden: " + osciMessage.getClass().getName());
                    }
                }
            } catch (RuntimeException e) {
                LOG.error("Cannot redeliver message '" + msg.getId() + "': " + e.toString());
                throw e;
            } catch (CsException e) {
                LOG.error("Cannot redeliver message '" + msg.getId() + "': " + e.toString());
                throw e;
//            } catch (ConnectorException e) {
//                LOG.error("Cannot redeliver message '" + msg.getId() + "'" + e.toString());
//                throw e;
            } catch (Exception e) {
                throw new RuntimeException("Cannot redeliver message '" + msg.getId() + "'", e);
            }

            if (problemManager.findMessage(msg.getId()).getLastAccess().equals(msg.getLastAccess())) {
                problemManager.deleteMessage(msg.getId());
            } else {
                throw new RuntimeException("Not deleting <" + msg.getId() + "> - message has been updated during resend");
            }

        } catch (RemoteException e) {
            LOG.error("Cannot delete message '" + msg.getId() + "'", e);
        } finally {
            RESENDING.set(null);
            UPDATED.remove();
        }
    }

    public static byte[] getMessageBytes(ProblemMessageVO vo) {
        try {
            final Message m = loadMetadata(vo);
            if (m != null) {
                return m.getMessage();
            }
        } catch (Exception e) {
            LOG.error("Cannot reconstruct message meta information", e);
        }

        return loadMessage(vo);
    }

    private static byte[] loadMessage(ProblemMessageVO vo) {
        final byte[] message = vo.getCachedMessage();
        if (message != null) {
            return message;
        }
        try {
            final ProblemMessageManager problemManager = ProblemMessageLookup.getManager();
            final byte[] bytes = problemManager.loadMessageBytes(vo.getId());
            vo.setCachedMessage(bytes);
            return bytes;
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public static Message loadUnwrappedMetadata(ProblemMessageVO vo) throws IOException {
        final Message msg = loadMetadata(vo);
        if (msg instanceof MyMessage) {
            return ((MyMessage)msg).myMsg;
        }
        return msg;
    }
    
    public static Message loadMetadata(ProblemMessageVO vo) throws IOException {
        synchronized (vo) {
            final Object o = vo.getCachedMetadata();
            if (o instanceof Message) {
                return (Message)o;
            }

            if (vo.isHasMetaData()) {
                final ProblemMessageManager problemManager = ProblemMessageLookup.getManager();
                final byte[] bytes = problemManager.loadMetaBytes(vo.getId());
                if (bytes != null) {
                    final Message m;
                    try {
                        m = (Message)new ObjectInputStream(new ByteArrayInputStream(bytes)).readObject();
                        vo.setCachedMetadata(m);
                        return m;
                    } catch (Throwable e) {
                        LOG.error("Could not load message object for <" + vo.getId() + ">", e);
                    }
                } else {
                    LOG.warn("Message metadata for <" + vo.getId() + "> not found");
                }
            }
            return null;
        }
    }

    public static ProblemMessageVO updateMessage(String senderId, String jobID, Message msg, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        if (msg instanceof MyMessage) {
            return createMessage(senderId, jobID, msg, direction, error, exception);
        }
        return null;
    }
    
    public static ProblemMessageVO createMessage(String senderId, String jobID, Message msg, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        return createMessage(senderId, jobID, msg, msg.getFamily() + ": " + msg.getType(), direction, error, exception);
    }

    public static ProblemMessageVO createMessage(String senderId, String jobID, Message msg, String type, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        final ProblemMessageManager problemManager = ProblemMessageLookup.getManager();
        try {
            final String id;
            if (msg instanceof OSCIInfo.Provider) {
                id = ((OSCIInfo.Provider)msg).getOsciInfo().getMessageId();
            } else if (msg instanceof MyMessage) {
                id = ((MyMessage)msg).getId();
            } else {
                id = null; // assign auto-generated id later
            }

            final ProblemMessageVO problemMessage = createMessage(senderId, jobID, id, msg.getMessage(), type, direction, error, exception);
            try {
                if (id == null) {
                    msg = new MyMessage(msg, problemMessage.getId());
                }
                final ByteArrayOutputStream os = new ByteArrayOutputStream();
                final ObjectOutputStream oos = new ObjectOutputStream(os);
                oos.writeObject(msg);
                oos.close();

                problemManager.saveMetaBytes(problemMessage.getId(), os.toByteArray());
                problemManager.updateMessage(problemMessage, false);
            } catch (IOException e) {
                LOG.error("Cannot save message metadata", e);
            }

            return problemMessage;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Cannot create ProblemMessage", e);
        }
    }

    public static ProblemMessageVO createMessage(String senderId, String jobID, String messageId, byte[] bytes, ProblemMessageVO.Type type, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        return createMessage(senderId, jobID, messageId, bytes, type.toString(), direction, error, exception);
    }
    public static ProblemMessageVO createMessage(String senderId, String jobID, String messageId, byte[] bytes, String type, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        try {
            final StringWriter sw = new StringWriter();
            if (exception != null) {
                exception.printStackTrace(new PrintWriter(sw));
            }

            final ProblemMessageManager problemManager = ProblemMessageLookup.getManager();
            final ProblemMessageVO o = RESENDING.get();
            if (o != null && (o.getId().equals(messageId) || messageId == null)) {
                if (UPDATED.get() != o) {
                    o.setError(error != null ? error.getValue() : null);
                    o.setStacktrace(sw.toString());
                    o.setRedeliverAttempts(o.getRedeliverAttempts() + 1);
                    problemManager.updateMessage(o, true);
                    UPDATED.set(o);
                } else {
                    LOG.debug("Update skipped: " + o.getId() + " - " + error, exception);
                }
                return o;
            } else {
                final ProblemMessageVO vo = new ProblemMessageVO(senderId, jobID, messageId, type, direction, error != null ? error.getValue() : null, new Date(), sw.toString());
                final boolean resendable = ProblemMessageVO.RESENDABLE.get() != Boolean.FALSE;
                if (resendable) {
                    LAST_ERROR.set(error);
                } else {
                    LAST_ERROR.set(ErrorCode.UNKNOWN);
                }
                return problemManager.createMessage(vo, bytes, resendable);
            }
        } catch (IOException e) {
            throw new RuntimeException("Cannot create ProblemMessage", e);
        }
    }

    public static ProblemMessageVO createMessage(String senderId, String jobID, String messageId, OSCIDocumentMessage osciMsg, ProblemMessageVO.Direction direction, ErrorCode error, Throwable exception) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            osciMsg.getDocument().marshal().writeTo(out);
        } catch (Exception e) {
            LOG.error("Cannot get OSCI message source", e);
        }
        return createMessage(senderId, jobID, messageId, out.toByteArray(), ProblemMessageVO.Type.OSCI, direction, error, exception);
    }

    public static String resendLock(String id) {
        return ("##lock##-ecs-resend-" + id).intern();
    }

    public static String createQueuedMessage(final String sender, final String jobID, final Message m) throws CsException {
        try {
            final MessageSender ms = (MessageSender)GenericMBeanWrapper.create(AUTO_SEND_MANAGER_NAME, MessageSender.class);
            if (!ms.isEnabled()) {
                throw new CsException("Cannot queue message: AutoSendManager is disabled");
            }

            final ProblemMessageVO run = (ProblemMessageVO)new TransactionRunner(jobID, new Callable() {
                public Object call() {
                    final ProblemMessageVO message = createMessage(sender, jobID, m, ProblemMessageVO.Direction.OUTGOING, null, null);
                    synchronized (resendLock(message.getId())) {
                        message.setNextReSend(new Date());
                        message.setRedeliverAttempts(-1);

                        try {
                            ProblemMessageLookup.getManager().updateMessage(message, false);
                        } catch (RemoteException e) {
                            throw new RuntimeException(e);
                        }

                        return message;
                    }
                }
            }).run();

            ms.scheduleForDelivery(run);

            return run.getId();
        } catch (RuntimeException e) {
            throw e;
        } catch (CsException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class MyMessage implements Message, OSCIRoutingInfo {
        private static final long serialVersionUID = 0L;

        private final Message myMsg;
        private final String myId;

        public MyMessage(Message msg, String id) {
            myMsg = msg;
            myId = id;
        }

        public int getAccountingMultiplier() {
            return myMsg.getAccountingMultiplier();
        }

        public String getFamily() {
            return myMsg.getFamily();
        }

        public byte[] getMessage() {
            return myMsg.getMessage();
        }

        public String getReceiver() {
            return myMsg.getReceiver();
        }

        public String getSender() {
            return myMsg.getSender();
        }

        public String getType() {
            return myMsg.getType();
        }

        public String getMessageId() {
            return myMsg.getMessageId();
        }

        public Object getMetaData(String name) {
            return myMsg.getMetaData(name);
        }

        public String[] getMetaKeys() {
            return myMsg.getMetaKeys();
        }

        public boolean isValid() {
            return myMsg.isValid();
        }

        public String getId() {
            return myId;
        }

        public String getContainerId() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getContainerId() : null;
        }

        public OSCICommunicationType getCommunicationType() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getCommunicationType() : null;
        }

        public String getSubject() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getSubject() : null;
        }

        public String getOriginator() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getOriginator() : null;
        }

        @Override
        public Boolean getEncrypt() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getEncrypt() : null;
        }

        @Override
        public Boolean getSign() {
            return myMsg instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)myMsg).getSign() : null;
        }
    }
}