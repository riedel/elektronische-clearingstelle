/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 09:31:29
 */
package cs.impl;

import cs.api.Message;
import cs.api.UnknownTypeException;
import cs.api.ValidityException;
import cs.api.directory.AccessDescriptor;
import cs.api.transport.MessageImpl;
import cs.api.transport.osci.OSCICommunicationType;
import cs.api.transport.osci.OSCIRoutingInfoImpl;
import cs.api.validation.ValidationException;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.config.MessageType;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;

import java.io.ByteArrayInputStream;
import java.security.cert.X509Certificate;
import java.util.Iterator;

public class MessageWrapper {

    private static final Logger LOG = Logger.getLogger(MessageWrapper.class);

    private Message theMessage;
    private Document document;
    private ValidationException exception;
    private X509Certificate certificate;
    private AccessDescriptor accessDescriptor;
    private boolean signed;
    private String messageId;
    private Throwable validityError;

    private MessageWrapper(byte[] message) throws DocumentException, UnknownTypeException {
        this.document = new org.dom4j.io.SAXReader().read(new ByteArrayInputStream(message));
        theMessage = extractInfo(message);
    }

    public boolean isValid() throws ValidationException {
        if (exception != null) {
            throw exception;
        }
        return theMessage.isValid();
    }

    public Throwable getValidityError() {
        if (validityError == null) {
            return null;
        }
        return ValidityException.create(validityError);
    }

    private boolean isValid(MessageType type) throws ValidationException {
        if (type == null) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Unknown MessageType");
            }
            return true;
        } else if (type.getValidator() == null) {
            if (LOG.isInfoEnabled()) {
                LOG.info("No Validator configured for type " + type.getTypeName(document));
            }
            return true;
        } else {
            return type.getValidator().validate(document);
        }
    }

    public Document getDocument() {
        return document;
    }

    public Message getMessage() {
        return theMessage;
    }

    private Message extractInfo(byte[] message) throws UnknownTypeException {
        final Configuration config = ConfigurationImpl.getInstance();
        UnknownTypeException last = null;
        for (Iterator iterator = config.getMessageTypes().iterator(); iterator.hasNext();) {
            MessageType type = (MessageType)iterator.next();
            if (type.matches(document)) {
                final String typeName = type.getTypeName(document);
                final String family = type.getFamily();
                final String sender = type.getSender(document);
                final String receiver = type.getReceiver(document);

                if (isEmpty(typeName) || isEmpty(family) || isEmpty(sender) || receiver == null) {
                    LOG.warn("Message only partially matches type definition: " + type);
                    LOG.warn("typeName = " + typeName);
                    LOG.warn("family = " + family);
                    LOG.warn("sender = " + sender);
                    LOG.warn("receiver = " + receiver);
                    last = new UnknownTypeException("Message only partially matches type definition: " + type, message);
                    continue;
                }

                boolean valid = false;
                try {
                    valid = !config.isMessageValidation() || isValid(type);
                } catch (ValidationException e) {
                    if (e.isValidityError()) {
                        validityError = e.getCause();
                    } else {
                        exception = e;
                    }
                }
                return new MessageImpl(message,
                        typeName,
                        family,
                        type.getMessageId(document),
                        sender,
                        receiver,
                        valid,
                        type.getAccountingMultiplier(document),
                        new OSCIRoutingInfoImpl(type.getOsciContainerId(), OSCICommunicationType.valueOf(type.getOsciMessageType()),
                                type.getOsciSubject(document), type.getEncrypt(), type.getSign()),
                        type.getMetadata(document));
            }
        }
        throw last == null ? new UnknownTypeException(message) : last;
    }

    private static boolean isEmpty(String s) {
        return s == null || s.length() == 0 || s.equals(":");
    }

    public static MessageWrapper createMessage(byte[] message)
            throws DocumentException, UnknownTypeException {
        return new MessageWrapper(message);
    }

    public void setOriginatingCertificate(X509Certificate signer) {
        certificate = signer;
    }

    public X509Certificate getOriginatingCertificate() {
        return certificate;
    }

    public void setAccessDescriptor(AccessDescriptor accessDescriptor) {
        this.accessDescriptor = accessDescriptor;
    }

    public AccessDescriptor getAccessDescriptor() {
        return accessDescriptor;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageId() {
        return messageId;
    }
}