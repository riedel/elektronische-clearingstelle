/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 16:36:49
 */
package cs.impl;

import cs.api.Connector;
import org.apache.log4j.Logger;

import java.util.*;

class ConnectorInfo {
    private static final Logger LOG = Logger.getLogger(ConnectorInfo.class);

    public final Connector connector;
    final String id;
    final Map<String, Set<String>> serviceAndAGS = new HashMap<String, Set<String>>();

    ConnectorInfo(Connector connector, String id, String serviceType, Set<String> agsSet) {
        this.connector = connector;
        this.id = id.intern();
        addService(serviceType, agsSet);
    }

    public void addService(String serviceType, Set<String> agsSet) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("[" + id + "] Adding service " + serviceType + " = " + agsSet);
        }

        this.serviceAndAGS.put(serviceType.intern(), agsSet);
    }

    public boolean matches(String serviceType, String ags) {
        final Set<String> set = serviceAndAGS.get(serviceType);
        final boolean b = (set != null && (set.contains(ags) || set.contains("*")))
                || (!"*".equals(serviceType) && matches("*", ags));
        if (LOG.isDebugEnabled()) {
            LOG.debug("[" + id + "].matches(" + serviceType + ", " + ags + ") = " + b);
        }

        return b;
    }

    public boolean hasService(String serviceType) {
        final boolean b = serviceAndAGS.containsKey(serviceType) || serviceAndAGS.containsKey("*");
        if (LOG.isDebugEnabled()) {
            LOG.debug("[" + id + "].hasService(" + serviceType + ") = " + b);
        }

        return b;
    }

    public String toString() {
        return serviceAndAGS.toString();
    }

    public void addAGS(String serviceType, Set<String> agsSet) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("[" + id + "] Adding " + agsSet + " to service " + serviceType);
        }

        serviceAndAGS.get(serviceType).addAll(agsSet);
    }

    public boolean intersects(String serviceType, Set<String> agsSet) {
        final Set<String> set;
        if (!hasService(serviceType)) {
            if ("*".equals(serviceType)) {
                set = new HashSet<String>();
                final Collection<Set<String>> s = serviceAndAGS.values();
                for (final Set<String> ags : s) {
                    set.addAll(ags);
                }
            } else {
                return false;
            }
        } else {
            set = serviceAndAGS.containsKey(serviceType) ? serviceAndAGS.get(serviceType) : serviceAndAGS.get("*");
        }
        assert set != null;

        final Set<String> _set = new HashSet<String>(set);
        _set.retainAll(agsSet);

        // relaxed
        if (true) return _set.size() > 0;

        if (_set.size() > 0) {
            return true;
        } else {
            // no intersection, check if any refers to a wildcard
            return set.contains("*")/* || agsSet.contains("*")*/;
        }
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConnectorInfo that = (ConnectorInfo)o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    public int hashCode() {
        return id.hashCode();
    }

    static class Comparator implements java.util.Comparator<ConnectorInfo> {
        private final String myServiceType;

        public Comparator(String serviceType) {
            myServiceType = serviceType;
        }

        public int compare(ConnectorInfo o1, ConnectorInfo o2) {
            final Set<String> s1 = o1.serviceAndAGS.get(myServiceType);
            final Set<String> s2 = o2.serviceAndAGS.get(myServiceType);
            
            if (s1 == null && s2 == null) {
                return 0;
            } else if (s1 != null && s2 == null) {
                return -1;
            } else if (s1 == null) {
                return 1;
            } else {
                //noinspection ConstantConditions
                assert s1 != null && s2 != null;

                if (hasWildcard(s1) && hasWildcard(s2)) {
                    if (s1.size() == s2.size()) {
                        return o1.id.compareTo(o2.id);
                    } else {
                        return s1.size() - s2.size();
                    }
                } else if (hasWildcard(s1)) {
                    return 1;
                } else if (hasWildcard(s2)) {
                    return -1;
                }
                return o1.id.compareTo(o2.id);
            }
        }

        private static boolean hasWildcard(Set<String> set) {
            return (set.contains("*") || set.contains("from:*"));
        }
    }
}