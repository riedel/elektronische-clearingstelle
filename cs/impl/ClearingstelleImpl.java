/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 22.07.2004
 * Time: 15:18:58
 */
package cs.impl;

import cs.api.*;
import cs.api.directory.DirectoryService;
import cs.api.directory.NegativeDVDVLookup;
import cs.api.directory.UnknownDestinationException;
import cs.api.logging.LogMessage;
import cs.api.logging.MessageLogger;
import cs.api.transport.MessageImpl;
import cs.api.transport.Result;
import cs.api.transport.Transport;
import cs.api.transport.osci.OSCIMessage;
import cs.api.transport.osci.OSCIResult;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.api.transport.osci.OSCIRoutingInfoImpl;
import cs.api.validation.ValidationException;
import cs.impl.config.ConfigException;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.osci.OSCIConnector;
import cs.impl.osci.OSCIHandler;
import cs.impl.util.*;
import cs.persistence.monitoring.ejb.client.ProblemMessageVO;
import edu.emory.mathcs.backport.java.util.concurrent.*;
import edu.emory.mathcs.backport.java.util.concurrent.atomic.AtomicReference;
import org.apache.log4j.Level;
import org.dom4j.DocumentException;

import javax.management.MBeanNotificationInfo;
import javax.management.MalformedObjectNameException;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;
import java.util.*;

/** @noinspection DuplicateThrows*/
public class ClearingstelleImpl extends NotificationBroadcasterSupport implements ClearingstelleImplMBean {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ClearingstelleImpl.class);

    public static final ObjectName NAME;
    static {
        try {
            NAME = new ObjectName("cs.impl:service=Clearingstelle");
        } catch (MalformedObjectNameException e) {
            throw new Error(e);
        }
    }

    private static final int ASYNC_WAIT_TIMEOUT = Integer.getInteger("ecs.impl.async-wait-timeout", 20).intValue();

    private final Map<String, ConnectorInfo> connectors = new LinkedHashMap<String, ConnectorInfo>();
    private final OSCIConnector osciConnector = OSCIConnector.getInstance();
    private final NotificationManager nmgr = new NotificationManager(this);

    private Configuration configuration;
    private DirectoryService directory;
    private AtomicReference myExecutorService = new AtomicReference();

    public void start() {
        configuration = ConfigurationImpl.getInstance();
    }

    public void stop() {
        shutdown();
        configuration = null;
        directory = null;

        final ExecutorService service = (ExecutorService)myExecutorService.get();
        if (service != null && myExecutorService.compareAndSet(service, null)) {
            final List jobs = service.shutdownNow();
            if (jobs.size() != 0) {
                LOG.warn("Canceled " + jobs.size() + " pending jobs!");
            }
        }
    }

    public static ClearingstelleImplMBean getInstance() {
        try {
            return (ClearingstelleImplMBean)GenericMBeanWrapper.create(NAME, ClearingstelleImplMBean.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void registerConnector(final Connector connector, String serviceType, Set<String> agsSet) {
        final String id = getConnectorId(connector);
        if (LOG.isInfoEnabled()) {
            LOG.info("Registering connector [" + id + "]: " + serviceType + " = " + agsSet);
        }

       /* if ("*".equals(serviceType) && connectors.size() > 0) {
            throw new IllegalArgumentException("Cannot add connector for wildcard service type because there's already a connector registered");
        } else */if (connectors.containsKey(id)) {
            // ID already registered, but not for this service?
            final ConnectorInfo ci = connectors.get(id);
            try {
                // check if the registered connector is still alive
                ci.connector.getID();  

                if (ci.hasService(serviceType)) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Adding additional AGS for service: " + serviceType);
                    }
                    ci.addAGS(serviceType, agsSet);
                } else {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Adding additional service: " + serviceType);
                    }
                    ci.addService(serviceType, agsSet);
                }
            } catch (ConnectorException e) {
                LOG.warn("Cannot contact already registered connector with ID = " + id + ". Connector has been replaced by the new instance.", e);
                final ConnectorInfo info = new ConnectorInfo(connector, id, serviceType, agsSet);
                connectors.put(id, info);
            }
        } else {
            final ConnectorInfo info = new ConnectorInfo(connector, id, serviceType, agsSet);
            final Collection<ConnectorInfo> collection = connectors.values();
            for (ConnectorInfo _info : collection) {
                if (_info.intersects(serviceType, agsSet)) {
                    throw new IllegalArgumentException("Connector for service " + serviceType + " and an intersecting AGS-Set is already registered: [" + _info.id + "]: " + _info);
                }
            }
            connectors.put(id, info);
        }
        nmgr.notifyConnectorRegistered(id);
    }

    public ConnectorInfo findConnector(String serviceType, String senderAGS, String receiverAGS, LookupMode mode) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Finding connector for <" + serviceType + ">, " + senderAGS + "->" + receiverAGS + " - "  + mode);
        }

        final ArrayList<ConnectorInfo> collection = new ArrayList<ConnectorInfo>(connectors.values());
        Collections.sort(collection, new ConnectorInfo.Comparator(serviceType));
        if (mode != LookupMode.XTA_ONLY) {
            for (ConnectorInfo ci : collection) {
                if (ci.matches(serviceType, receiverAGS)) {
                    return ci;
                }
            }
        }

        if (mode != LookupMode.NO_XTA) {
            DirectoryService _directory = directory;
            if (_directory == null) {
                try {
                    _directory = directory = configuration.getDirectoryServiceImpl();
                } catch (ConfigException e) {
                    throw new RuntimeException(e);
                }
            }

            final String service = _directory.translateMessageTypeToService(serviceType);
            final String xtaService = "xta:" + service;
            for (ConnectorInfo ci : collection) {
                if (ci.matches(xtaService, receiverAGS)) {
                    return ci;
                }
            }
        }

        if (mode != LookupMode.XTA_ONLY && mode != LookupMode.NO_FROM) {
            // Check "from:" after *all* other plugins have been checked to prevent the ClientConnector to catch
            // messages from an OSCI-enabled sender that should be delivered locally instead of being delivered by OSCI.
            final String _senderAGS = "from:" + senderAGS;
            for (ConnectorInfo ci : collection) {
                if (ci.matches(serviceType, _senderAGS) || ci.matches(serviceType, "from:*")) {
                    return ci;
                }
            }
        }

        return null;
    }

    public void unregisterConnector(final Connector connector) {
        unregisterConnector(getConnectorId(connector));
    }

    public void unregisterConnector(final String id) {
        if (!connectors.containsKey(id)) {
            throw new IllegalArgumentException("Connector with ID [" + id + "] not registered");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Unregistering connector [" + id + "]");
        }
        nmgr.notifyConnectorUnregistered(id);
        connectors.remove(id);
    }

    public byte[] sendMessage(String sender, byte[] message, String jobID)
            throws CsException, UnknownDestinationException, ConnectorException, UnknownTypeException, InvalidMessageException, ValidationException {
        try {
            return sendMessage(sender, message, MODE_STANDARD, jobID);
        } catch (CsException e) {
            LOG.fatal("sendMessage failed", e);
            throw e;
        }
    }

    public byte[] sendMessage(String sender, byte[] message, int mode, String jobID)
            throws CsException, UnknownDestinationException, ConnectorException, UnknownTypeException, InvalidMessageException, ValidationException {
        if (message == null) {
            throw new IllegalArgumentException("message cannot be null");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("sendMessage(" + sender + ", " + new String(message) + ")");
        }
        try {
            final MessageWrapper mw = MessageWrapper.createMessage(message);
            if (!mw.isValid()) {
                nmgr.notifySendMsgError(mw.getMessage(), "Got an invalid message");
                throw new InvalidMessageException(mw.getValidityError(), mw.getMessage());
            }

            final Result result = handleSendMessage(sender, mw.getMessage(), mode, jobID);
            final byte[] bytes = result.getResponse();
            if (LOG.isDebugEnabled()) {
                if (bytes != null) {
                    LOG.debug("sendMessage::return = " + new String(bytes));
                } else {
                    LOG.debug("sendMessage::<no return value>");
                }
            }
            return bytes;
        } catch (DocumentException e) {
            throw new InvalidMessageException(e, message);
        }
    }

    public byte[] sendMessage(String sender, byte[] message, int mode, String jobID, String originator)
            throws CsException, ConnectorException, UnknownTypeException, InvalidMessageException, UnknownDestinationException, ValidationException
    {
        if (message == null) {
            throw new IllegalArgumentException("message cannot be null");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("sendMessage(" + sender + ", " + new String(message) + ")");
        }
        try {
            final MessageWrapper mw = MessageWrapper.createMessage(message);
            final MessageImpl _m = (MessageImpl)mw.getMessage();

            final OSCIRoutingInfo routingInfo = OSCIRoutingInfoImpl.withOriginator(_m, originator);
            final Message m = new MessageImpl(_m.getMessage(), _m.getType(), _m.getFamily(), _m.getMessageId(), _m.getSender(), _m.getReceiver(), true, _m.getAccountingMultiplier(), routingInfo, _m.getMetaData());

            // no validation here, this is used for (pre-validated) rts messages

            final Result result = handleSendMessage(sender, m, mode, jobID);
            final byte[] bytes = result.getResponse();
            if (LOG.isDebugEnabled()) {
                if (bytes != null) {
                    LOG.debug("sendMessage::return = " + new String(bytes));
                } else {
                    LOG.debug("sendMessage::<no return value>");
                }
            }
            return bytes;
        } catch (DocumentException e) {
            throw new InvalidMessageException(e, message);
        }
    }

    public Result handleSendMessage(String sender, Message m, int mode, String jobID)
            throws CsException, ConnectorException, UnknownTypeException, InvalidMessageException, UnknownDestinationException, ValidationException {

        Tracing.startOperation("Processing Message");
        org.apache.log4j.NDC.push("[handling message " +
                "MessageID = " + m.getMessageId() + ", " +
                "SenderID = " + sender + ", " +
                "Mode = " + mode +
                ")]");

        try {
            return _handleSendMessage(sender, m, mode, jobID);
        } finally {
            org.apache.log4j.NDC.pop();
            Tracing.endOperation("Processing Message");
        }
    }

    private Result _handleSendMessage(final String sender, final Message m, final int mode, final String jobID) throws CsException, ConnectorException {
        ProblemMessageUtil.LAST_ERROR.set(null);

        if ((mode & MODE_ASYNC) != 0) {
            if ((mode & MODE_SILENT) == 0) {
                throw new IllegalArgumentException("MODE_ASYNC must be combined with MODE_SILENT");
            }
        }
        if (mode == MODE_SILENT) {
            // sanity check
            throw new IllegalArgumentException("MODE_SILENT may only be used together with other flags, e.g. MODE_STANDARD");
        }

        final Transport transport;
        final LookupMode _mode = LookupMode.fromMode(mode);

        final ConnectorInfo ci = (mode & MODE_LOCAL) != 0 ? findConnector(m.getType(), m.getSender(), m.getReceiver(), _mode) : null;
        if (ci == null || (ci.id.equals(sender) && sender.equals("XTA"))) {
            if ((mode & MODE_REMOTE) != 0) {
                transport = osciConnector;
            } else {
                // local delivery, i.e. to connectors only, NO default (OSCI)
                nmgr.notifySendMsgError(m, "Could not find a local Connector for this message");

                ProblemMessageUtil.createMessage(sender, jobID, m, ProblemMessageVO.Direction.INCOMING, ErrorCode.UNKNOWN_DESTINATION, null);

                throw new UnknownDestinationException(m.getType(), m.getReceiver());
            }
        } else {
            try {
                transport = new LocalTransport(ci.connector, getConnectorId(ci.connector));
            } catch (UnreachableConnectorException e) {
                LOG.error("Connector [" + ci.id + "] could not be contacted. Connector will have to re-register", e);
                unregisterConnector(ci.id);

                ProblemMessageUtil.createMessage(sender, jobID, m, ProblemMessageVO.Direction.INCOMING, ErrorCode.UNKNOWN_DESTINATION, e);

                throw new UnknownDestinationException(m.getType(), m.getReceiver());
            }
        }
        if (LOG.isInfoEnabled()) {
            LOG.log(Level.INFO, "Message handled by connector [" + transport.getName() + "]");
        }

        if ((mode & MODE_ASYNC) != 0) {
            if ((mode & MODE_PERSISTENT) == 0 || Boolean.getBoolean("ecs.disable-persistent-mode")) {
                LOG.debug("Submitting message for async delivery");

                final ClassLoader cl = Thread.currentThread().getContextClassLoader();
                final Future future = getExecutorService().submit(new Callable() {
                    public Object call() throws Exception {
                        final ClassLoader before = Thread.currentThread().getContextClassLoader();
                        Thread.currentThread().setContextClassLoader(cl);
                        try {
                            return doSend(transport, m, sender, mode, jobID);
                        } finally {
                            Thread.currentThread().setContextClassLoader(before);
                        }
                    }
                });

                try {
                    return (Result)future.get(ASYNC_WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    if (e.getCause() instanceof CsException) {
                        throw (CsException)e.getCause();
                    } else if (e.getCause() instanceof ConnectorException) {
                        throw (ConnectorException)e.getCause();
                    } else if (e.getCause() instanceof RuntimeException) {
                        throw (RuntimeException)e.getCause();
                    }
                    throw new RuntimeException(e.getCause());
                } catch (TimeoutException e) {
                    LOG.debug("Async message not finished yet");
                    return Result.ASYNC_RESULT;
                }
            } else {
                try {
                    final String queuedMessage = ProblemMessageUtil.createQueuedMessage(sender, jobID, m);
                    LOG.info("Created queued message with ID <" + queuedMessage + ">");
                } catch (CsException e) {
                    throw e;
                } catch (Exception e) {
                    throw new CsException(e);
                }
            }
            return Result.ASYNC_RESULT;
        } else {
            return doSend(transport, m, sender, mode, jobID);
        }
    }

    private ExecutorService getExecutorService() {
        final ExecutorService executorService = (ExecutorService)myExecutorService.get();
        if (executorService != null) {
            return executorService;
        }
        final Integer maxThreadCount = Integer.getInteger("ecs.core.max-thread-count", 5);
        final Integer maxQueueSize = Integer.getInteger("ecs.core.max-queue-size", 30);
        final ExecutorService service = Boolean.getBoolean("ecs.core.no-async-send-limit") ? Executors.newCachedThreadPool() :
                new ThreadPoolExecutor(1, maxThreadCount.intValue(), 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(maxQueueSize.intValue()), new ThreadPoolExecutor.CallerRunsPolicy() {
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Asynchronous send queue limit reached. Using blocking sendMessage()", new Throwable("<stack trace>"));
                        } else {
                            LOG.info("Asynchronous send queue limit reached. Using blocking sendMessage()");
                        }

                        super.rejectedExecution(r, e);
                    }
                });
        if (!myExecutorService.compareAndSet(null, service)) {
            service.shutdown();
        }
        return (ExecutorService)myExecutorService.get();
    }

    private Result doSend(Transport transport, Message m, String sender, int mode, String jobID) throws ConnectorException, CsException {
        ProblemMessageUtil.LAST_ERROR.set(null);

        nmgr.notifySendMsg(m, transport.getName());

        LogMessage.Direction direction;
        if (transport.getName().equals(OSCIConnector.ID)) {
            direction = LogMessage.Direction.OUTGOING;
        } else if (sender.startsWith(OSCIHandler.ID_PREFIX)) {
            direction = LogMessage.Direction.INCOMING;
        } else {
            direction = LogMessage.Direction.INTERNAL;
        }

        final String messageId = m instanceof OSCIMessage ?
                ((OSCIMessage)m).getOsciInfo().getMessageId() : null;

        try {
            // deliver to connector and return response
            if (jobID != null) {
                LOG.info("Sending message <" + m.getMessageId() + "> for job <" + jobID + ">");
            } else {
                LOG.info("Sending message <" + m.getMessageId() + ">");
            }

            Result result;
            try {
                result = transport.send(sender, jobID, m);
            } catch (RejectedMessageException e) {
                result = Result.NULL_RESULT;
                direction = LogMessage.Direction.INCOMING_REJECTED;
            }

            final String id = messageId == null ? (result instanceof OSCIResult ?
                    ((OSCIResult)result).getOsciInfo().getMessageId() : null) : messageId;

            Statistics.message(direction, (String)null, id, m);

            if ((mode & MODE_LOG) != 0) {
                try {
                    final MessageLogger logger = configuration.getLoggerImpl();

                    logger.logMessage(new LogMessage(m, result, direction, sender, transport.getName(), jobID));
                } catch (ConfigException e) {
                    nmgr.notifySendMsgError(m, "Non-fatal: Failed to get a logger implementation");
                    LOG.error("Cannot get logger implementation", e);
                } catch (Throwable e) {
                    nmgr.notifySendMsgError(m, "Non-fatal: Logger threw an exception");
                    LOG.error("Logger threw an exception", e);
                }
            } else {
                LOG.warn("Message was sent without being logged!");
            }

            notifyConnector(sender, m, jobID, result, null);

            return result;
        } catch (UnhandledMessageException e) {
            if (transport.isLocal() && e.getErrorCode() == null && (mode & MODE_REMOTE) != 0 && LookupMode.fromMode(mode) != LookupMode.NO_FROM) {
                LOG.info("Message rejected by " + transport.getName() + ". Attempting remote OSCI delivery.");
                return doSend(osciConnector, e.getProblemMessage(), sender, mode, jobID);
            }

            LOG.fatal("Connector " + transport.getName() + " did not successfully handle the message", e);

            final String code = e.getErrorCode();
            Statistics.message(direction, code, messageId, m);

            if (e.isPutIntoMonitoring()) {
                final ErrorCode error = ErrorCode.valueOf(code);
                if (error != null && !ProblemMessageUtil.RESENDABLE_CODES.contains(error.getValue())) {
                    notifyConnector(sender, m, jobID, null, error.getValue());
                }
                ProblemMessageUtil.createMessage(sender, jobID, m, ProblemMessageVO.Direction.INCOMING, error, e);
            } else {
                notifyConnector(sender, m, jobID, null, code);
            }

            return Result.NULL_RESULT;
        } catch (ConnectorException e) {
            LOG.fatal("Connector could not handle the message", e);
            nmgr.notifySendMsgError(m, "Connector could not handle the message");

            final Throwable cause = e.getCause();
            final ErrorCode errorCode = cause instanceof CsException ? ((CsException)cause).getCode() : ErrorCode.PLUGIN_FAILURE;
            if (transport.isLocal()/* && !"cs.client.server.ClientConnector".equals(ci.id)*/) {
                LOG.debug("[" + transport + "]: Incoming Message (" + messageId + "), [" + errorCode + "] -> Monitoring", e);
                ProblemMessageUtil.createMessage(sender, jobID, m, ProblemMessageVO.Direction.INCOMING, errorCode, e);
            }

            if (cause instanceof CsException) {
                Statistics.message(direction, ((CsException)cause).getCode(), null, m);
            } else {
                Statistics.message(direction, errorCode, null, m);
            }

            final ErrorCode error = (ErrorCode)ProblemMessageUtil.LAST_ERROR.get();
            if (error != null && !ProblemMessageUtil.RESENDABLE_CODES.contains(error.getValue())) {
                notifyConnector(sender, m, jobID, null, error.getValue());
            }

            if ((mode & MODE_SILENT) == 0) {
                // unwrap exceptions like UnknownDestinationException
                if (cause instanceof CsException) {
                    throw (CsException)cause;
                } else {
                    throw e;
                }
            } else if (cause instanceof NegativeDVDVLookup) {
                try {
                    returnToSender(m, sender, ErrorCode.NO_DVDV_ENTRY.getValue(), jobID);
                } catch (ConnectorException e1) {
                    LOG.error("returnToSender([" + sender + "]) threw an exception. Message is put into monitoring table.", e1);
                    ProblemMessageUtil.createMessage(sender, jobID, m, ProblemMessageVO.Direction.OUTGOING, ErrorCode.NO_DVDV_ENTRY, e);

                    // TODO: revisit: Does this violate the "silent" contract if the returnToSender-exception is thrown back to the caller?
                    throw e1;
                }
            }

            return Result.NULL_RESULT;
        } catch (RuntimeException e) {
            LOG.fatal("sendMessage", e);
            nmgr.notifySendMsgError(m, e.getMessage());
            throw e;
        } catch (Error e) {
            LOG.fatal("sendMessage", e);
            nmgr.notifySendMsgError(m, e.getMessage());
            throw e;
        } finally {
            ProblemMessageUtil.LAST_ERROR.set(null);
        }
    }

    private void notifyConnector(String sender, Message m, String jobID, Result result, String errorCode) throws ConnectorException {
        final ConnectorInfo senderConnector = findConnector(sender);
        if (senderConnector != null) {
            if (isInstanceOf(senderConnector.connector, ConnectorEx2.class)) {
                if (result != null && result != Result.NULL_RESULT) {
                    ((ConnectorEx2)senderConnector.connector).notifyDelivery(m, jobID, result);
                } else {
                    ((ConnectorEx2)senderConnector.connector).notifyFailedDelivery(m, jobID, errorCode);
                }
            } else if (sender.equals("XTA")) {
                LOG.error("XTA connector must implement ConnectorEx2: " + sender);
            }
        } else if (!sender.startsWith("osci.")) {
            LOG.warn("No connector registered for sender ID '" + sender + "'. Connector will not be notified.");
        }
    }

    public void notifyException(Throwable e) {
        nmgr.notifyException(e);
    }

    public void shutdown() {
        nmgr.notifyShutdown();

        final Collection<ConnectorInfo> collection = connectors.values();
        for (ConnectorInfo info : collection) {
            Connector connector = info.connector;
            try {
                connector.notifyShutdown();
            } catch (ConnectorException e) {
                try {
                    LOG.error("Error in [" + getConnectorId(connector) + "].notifyShutdown()", e);
                } catch (UnreachableConnectorException ex) {
                    // give up
                }
            }
        }
        connectors.clear();
    }

    private static class UnreachableConnectorException extends RuntimeException {
        public UnreachableConnectorException(final ConnectorException e) {
            super(e);
        }

        public UnreachableConnectorException(String message) {
            super(message);
        }
    }

    private String getConnectorId(Connector connector) {
        try {
            return connector.getID();
        } catch (ConnectorException e) {
            LOG.fatal("Cannot get ID for connector: " + connector);
            throw new UnreachableConnectorException(e);
        }
    }

    private ConnectorInfo findConnector(String sender) {
        return (connectors.get(sender));
    }

    public Set<String> listConnectors() {
        return connectors.keySet();
    }

    public void returnToSender(Message m, String senderId, String errorCode, String jobID) throws ConnectorException {
        LOG.info("ReturnToSender[id=" + m.getMessageId() + "] - Type: " + m.getType() + ",From: " + m.getSender() + ", To: " + m.getReceiver() + ", Error: " + errorCode + ", SendJob-ID: " + jobID + ", Plugin-ID: " + senderId);

        final ConnectorInfo info = findConnector(senderId);
        if (info != null) {
            if (isInstanceOf(info.connector, ConnectorEx.class)) {
                ((ConnectorEx)info.connector).processReturnedMessage(m, errorCode, jobID);
            } else {
                throw new UnreachableConnectorException("Connector with ID [" + senderId + "] does not implement the required ConnectorEx interface");
            }
        } else {
            throw new UnreachableConnectorException("Plugin for Connector-ID [" + senderId + "] is not registered");
        }
    }

    public boolean handleInvalidMessage(Message message, ErrorCode errorCode, Throwable exception) throws ConnectorException {
        final ConnectorInfo connector = findConnector(message.getType(), message.getSender(), message.getReceiver(), LookupMode.DEFAULT);
        if (connector == null) {
            return false;
        }
        if (!isInstanceOf(connector.connector, ConnectorEx.class)) {
            LOG.info("Connector [" + connector.id + "] does not implement ConnectorEx. Message with error code " + errorCode + " cannot be handled.");
            return false;
        }
        if (((ConnectorEx)connector.connector).processInvalidMessage(message, errorCode.toString(), exception)) {
            return true;
        }
        return false;
    }

    public Map<String, Set<String>> getConnectorInfo(String id) {
        return (connectors.get(id)).serviceAndAGS;
    }

    public MBeanNotificationInfo[] getNotificationInfo() {
        return nmgr.getNotificationInfo();
    }

    private static boolean isInstanceOf(Connector c, Class clazz) throws ConnectorException {
        if (c instanceof RemoteWrapper) {
            return ((RemoteWrapper)c).isInstanceOf(clazz);
        } else {
            return clazz.isInstance(c);
        }
    }

    final static class LookupMode extends cs.impl.util.Enumeration {
        static final LookupMode DEFAULT = new LookupMode("DEFAULT");
        static final LookupMode XTA_ONLY = new LookupMode("XTA_ONLY");
        static final LookupMode NO_XTA = new LookupMode("NO_XTA");
        static final LookupMode NO_FROM = new LookupMode("NO_FROM");

        protected LookupMode(String value) {
            super(value);
        }

        public static LookupMode fromMode(int mode) {
            if ((mode & MODE_XTA) == MODE_XTA) {
                return XTA_ONLY;
            } else if ((mode & MODE_NO_XTA) == MODE_NO_XTA) {
                return NO_XTA;
            } else if ((mode & MODE_NO_FROM) == MODE_NO_FROM) {
                return NO_FROM;
            }
            return DEFAULT;
        }
    }
}
