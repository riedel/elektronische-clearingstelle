/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 11.11.2004
 * Time: 18:22:27
 */
package cs.impl.weblogic;

import cs.impl.ClearingstelleImpl;
import cs.impl.config.ConfigurationImpl;
import cs.impl.directory.DirectoryServiceImpl;
import cs.impl.logging.Log4jLoggingService;
import cs.impl.util.weblogic.MultiMBeanLoader;
import weblogic.application.ApplicationLifecycleEvent;
import weblogic.application.ApplicationLifecycleListener;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * Loader for implemenation and default MBeans.
 */
public final class CoreLoader extends ApplicationLifecycleListener {

    private static final String IMPL_DOMAIN = "cs.impl";
    private static final String CONFIGURATION = IMPL_DOMAIN + ":service=Configuration";
    private static final String CLEARINGSTELLE = IMPL_DOMAIN + ":service=Clearingstelle";

    private static final String SERVICES_DOMAIN = "cs.services";
    private static final String DIRECTORY_SERVICE = SERVICES_DOMAIN + ":service=DirectoryService";
    private static final String LOGGING_SERVICE = SERVICES_DOMAIN + ":service=LoggingService";


    public void preStart(ApplicationLifecycleEvent event) {
        final MultiMBeanLoader loader = MultiMBeanLoader.getInstance();

        try {
            loader.addMBean(new ObjectName(CONFIGURATION), new ConfigurationImpl());
            loader.addMBean(new ObjectName(CLEARINGSTELLE), new ClearingstelleImpl());

            loader.addMBean(new ObjectName(DIRECTORY_SERVICE), new DirectoryServiceImpl());
            loader.addMBean(new ObjectName(LOGGING_SERVICE), new Log4jLoggingService());
        } catch (MalformedObjectNameException e) {
            throw new Error(e);
        }
    }
}