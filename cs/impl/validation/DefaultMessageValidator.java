/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.08.2004
 * Time: 11:26:04
 */
package cs.impl.validation;

import com.sun.msv.verifier.ValidityViolation;
import com.sun.msv.verifier.jarv.TheFactoryImpl;
import cs.api.validation.MessageValidator;
import cs.api.validation.ValidationException;
import cs.impl.util.Tracing;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Namespace;
import org.dom4j.QName;
import org.dom4j.io.SAXWriter;
import org.iso_relax.verifier.Schema;
import org.iso_relax.verifier.Verifier;
import org.iso_relax.verifier.VerifierConfigurationException;
import org.iso_relax.verifier.VerifierFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Simple, configurable schema validator.
 */
public class DefaultMessageValidator implements MessageValidator {
    private static final Logger LOG = Logger.getLogger(DefaultMessageValidator.class);

    private static final Namespace SCHEMA_NAMESPACE = new Namespace(null, "http://www.w3.org/2001/XMLSchema-instance");
    private static final QName SCHEMA_LOCATION = new QName("schemaLocation", SCHEMA_NAMESPACE);

    private static final String DEFAULT_SEARCH_PATH = "schemas/";

    private String schemaName = null;
    private String[] searchPath = new String[]{ DEFAULT_SEARCH_PATH };

    private final Map schemas = new HashMap();

    public boolean validate(Document d) throws ValidationException {
        Tracing.startOperation("Validate Message");
        try {
            String schemaName = getSchemaName();
            if (schemaName == null) {
                schemaName = guessFromDocument(d);
            }
            if (schemaName == null) {
                LOG.error("Missing schemaName attribute.", new Throwable("<stacktrace>"));
                return false;
            }
            try {
                final Schema s = getSchema(schemaName);
                if (s == null) {
                    LOG.info("No schema found, validation disabled");
                    return true;
                }
                final Verifier verifier = s.newVerifier();

                new SAXWriter(verifier.getVerifierHandler()).write(d);

                return true;
            } catch (ValidityViolation e) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("[ValidityViolation] Document is invalid. ", e);
                }

                LOG.error("[ValidityViolation] Document is invalid. " + e.getMessage());
                throw new ValidationException(e, true);
            } catch (SAXException e) {
                throw new ValidationException(e, false);
            } catch (IOException e) {
                throw new ValidationException(e, false);
            } catch (VerifierConfigurationException e) {
                LOG.error("Cannot get Verifier instance", e.getException());
                throw new ValidationException(e, false);
            }
        } finally {
            Tracing.endOperation("Validate Message");
        }
    }


    protected String guessFromDocument(Document d) {
        final String nsURI = d.getRootElement().getNamespaceURI();
        final String s = d.getRootElement().attributeValue(SCHEMA_LOCATION);
        if (s == null) {
            return null;
        }
        final StringTokenizer st = new StringTokenizer(s, " ");
        while (st.hasMoreElements()) {
            String s1 = (String)st.nextElement();
            if (!st.hasMoreElements()) {
                if (LOG.isEnabledFor(Level.WARN)) {
                    LOG.warn("Malformed xsi:schemaLocation: " + s);
                }

                break;
            }
            String s2 = (String)st.nextElement();
            if (s1.equals(nsURI)) {
                if (LOG.isInfoEnabled()) {
                    LOG.info("Guessed schemaName from xsi:schemaLocation: " + s2);
                }

                return s2;
            }
        }
        return null;
    }

    public void setSchemaName(String name) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("SchemaName: " + name);
        }

        schemaName = name;
    }

    public void setSearchPath(String path) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("SearchPath: " + path);
        }

        final StringTokenizer st = new StringTokenizer(path, " ");
        searchPath = new String[st.countTokens() + 1];
        int i=0;
        while (st.hasMoreElements()) {
            String s = (String)st.nextElement();
            s = s.replace('\\', '/');
            getSearchPath()[i++] = (!(s.startsWith("/") || s.matches("[a-zA-Z]:.*")) ? DEFAULT_SEARCH_PATH : "") + s + (!s.endsWith("/") ? "/" : "");
        }
        getSearchPath()[i] = DEFAULT_SEARCH_PATH;
    }

    private synchronized Schema getSchema(String schemaName) throws IOException, VerifierConfigurationException, SAXException {
        Schema schema;
        if ((schema = (Schema)schemas.get(schemaName)) == null) {
            final VerifierFactory factory = new TheFactoryImpl();

            factory.setEntityResolver(new EntityResolver() {
                public InputSource resolveEntity(String publicId, String systemId) throws IOException {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Asked to resolve system-id: " + systemId);
                    }

                    if (systemId == null) {
                        return null;
                    }
                    int x = systemId.lastIndexOf("/");
                    if (x != -1 && systemId.length() > x + 1) {
                        systemId = systemId.substring(x + 1);
                    }

                    final InputSource is;
                    if ((is = getInputSource(systemId)) != null) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("New system-Id: " + is.getSystemId());
                        }

                        return is;
                    } else {
                        LOG.debug("Unknown system-id: " + systemId);
                        return null;
                    }
                }
            });

            final InputSource inputSource = getInputSource(schemaName);
            if (inputSource == null) {
                LOG.error("Could not find schema with name '" + schemaName + "'");
                return null;
            }
            schema = factory.compileSchema(inputSource);
            schemas.put(schemaName, schema);
        }
        return schema;
    }

    private InputSource getInputSource(final String name) throws IOException {
        if (name.indexOf(":") != -1) try {
            return makeInputSource(new URL(name));
        } catch (MalformedURLException e) {
            // no url
        }
        final String[] searchPath = getSearchPath();
        for (int i = 0; i < searchPath.length; i++) {
            String s = searchPath[i];
            final URL resource = getClass().getResource(s + name);
            if (resource != null) {
                return makeInputSource(resource);
            }
            final URL r = Thread.currentThread().getContextClassLoader().getResource(s + name);
            if (r != null) {
                return makeInputSource(r);
            }
            final File file = new File(s + name);
            if (file.exists()) {
                return makeInputSource(file.toURL());
            }
        }
        return null;
    }

    private InputSource makeInputSource(final URL resource) throws IOException {
        final InputSource inputSource = new InputSource(resource.toExternalForm());
        inputSource.setByteStream(resource.openStream());
        return inputSource;
    }

    protected String getSchemaName() {
        return schemaName;
    }

    protected String[] getSearchPath() {
        return searchPath;
    }
}
