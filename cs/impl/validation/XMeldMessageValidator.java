/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 08.09.2004
 * Time: 13:50:02
 */
package cs.impl.validation;

import org.dom4j.Document;

public class XMeldMessageValidator extends DefaultMessageValidator {
    private static final String XMELD_NACHRICHTEN_XSD = "xmeld-nachrichten.xsd";

    private static final String XMELD_11 = "http://www.osci.de/xmeld11";
    private static final String XMELD_12 = "http://www.osci.de/xmeld12";
    private static final String XMELD_131 = "http://www.osci.de/xmeld131";
    private static final String XMELD_132 = "http://www.osci.de/xmeld132";
    private static final String XMELD_132A = "http://www.osci.de/xmeld132a";

    protected String[] getSearchPath() {
        return new String[]{ "schemas/", "schemas/xmeld/" };
    }

    protected String guessFromDocument(Document d) {
        final String uri = d.getRootElement().getNamespaceURI();
        if (XMELD_11.equals(uri)) {
            return "v11/" + XMELD_NACHRICHTEN_XSD;
        } else if (XMELD_12.equals(uri)) {
            return "v12/" + XMELD_NACHRICHTEN_XSD;
        } else if (XMELD_131.equals(uri)) {
            return "v131/" + XMELD_NACHRICHTEN_XSD;
        } else if (XMELD_132.equals(uri)) {
            return "v132/" + XMELD_NACHRICHTEN_XSD;
        } else if (XMELD_132A.equals(uri)) {
            return "v132a/" + XMELD_NACHRICHTEN_XSD;
        }
        return super.guessFromDocument(d);
    }

    public void setSchemaName(String name) {
        throw new UnsupportedOperationException();
    }

    public void setSearchPath(String path) {
        throw new UnsupportedOperationException();
    }
}