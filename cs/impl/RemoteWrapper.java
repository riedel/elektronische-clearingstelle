/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 22.07.2004
 * Time: 17:07:55
 */
package cs.impl;

import cs.api.*;
import cs.api.transport.Result;

import java.rmi.RemoteException;

public class RemoteWrapper implements ConnectorEx, ConnectorEx2 {
    private final RemoteConnector connector;

    public RemoteWrapper(RemoteConnector connector) {
        this.connector = connector;
    }

    public boolean isInstanceOf(Class clazz) throws ConnectorException {
        try {
            return connector.isInstanceOf(clazz);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public void processReturnedMessage(Message m, String errorCode, String jobID) throws ConnectorException {
        try {
            connector.processReturnedMessage(m, errorCode, jobID);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public boolean processInvalidMessage(Message message, String errorCode, Throwable exception) throws ConnectorException {
        try {
            return connector.processInvalidMessage(message, errorCode, exception);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public String getID() throws ConnectorException {
        try {
            return connector.getID();
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public byte[] processMessage(Message message) throws ConnectorException {
        try {
            return connector.processMessage(message);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public void notifyDelivery(Message m, String jobID, Result result) throws ConnectorException {
        try {
            connector.notifyDelivery(m, jobID, result);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public void notifyFailedDelivery(Message m, String jobID, String errorCode) throws ConnectorException {
        try {
            connector.notifyFailedDelivery(m, jobID, errorCode);
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public void notifyShutdown() throws ConnectorException {
        try {
            connector.notifyShutdown();
        } catch (RemoteException e) {
            throw new ConnectorException(e);
        }
    }

    public String toString() {
        return connector.toString();
    }
}