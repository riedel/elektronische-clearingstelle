package cs.impl.directory;


import cs.api.directory.AccessDescriptor;
import cs.api.directory.CertificateReference;
import cs.api.directory.UnknownDestinationException;
import cs.api.directory.XtaConfig;
import cs.api.validation.ValidationException;
import cs.impl.config.ConfigException;
import cs.impl.util.*;
import cs.impl.util.Base64;
import cs.impl.validation.DefaultMessageValidator;
import org.apache.log4j.Logger;
import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.*;
import java.util.Enumeration;

public class SimpleDirectory {
    public static final SimpleDirectory EMPTY_DIRECORY = new SimpleDirectory();
    public static Logger LOG = Logger.getLogger(SimpleDirectory.class);

    private static final Namespace NAMESPACE = Namespace.get("http://ns.dzbw.de/clearingstelle/directory/1.0");

    // no longer static to prevent re-deployment problems due to the thread holding
    // a strong reference to its own class(loader)
    private CRLReloader crlReloader;

    private final URL url;
    private final Map typeMap = new LinkedHashMap();
    private final Map typeMapCache = new HashMap();
    private final Map keystoreMap = new HashMap();
    private final List dvdvServerList = new ArrayList();
    private final List lookupProvider = new ArrayList();
    private final Map permissions = new LinkedHashMap();
    private final Set trustanchors = new HashSet();
    private final List certStores = new ArrayList();
    private final Map lookupEntryCache = new HashMap();

    private InboxConfig myInboxConfig;
    private XtaConfigImpl myXtaConfig = new XtaConfigImpl();
    private CertStore myCRLRootStore;
    private final Object myCRLCacheLock = new Object();

    private SimpleDirectory() {
        this.url = null;
    }

    public SimpleDirectory(URL url) throws ConfigException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loading configuration from URL: " + url);
        }

        this.url = url;

        try {
            final Document document = new SAXReader().read(this.url);
            final DefaultMessageValidator validator = new DefaultMessageValidator() {
                protected String getSchemaName() {
                    return getClass().getResource("directory.rng").toExternalForm();
                }
            };

            try {
                if (!validator.validate(document)) {
                    throw new ConfigException("Directory XML config file is invalid");
                }
            } catch (ValidationException e) {
                if (e.isValidityError()) {
                    throw new ConfigException("Directory XML config file is invalid", e.getCause());
                }
                throw e;
            }

            final Element rootElement = document.getRootElement();
            load(rootElement);
            if (LOG.isInfoEnabled()) {
                LOG.info("Directory loaded.");
                LOG.info("TypeMappings: " + typeMap);
                LOG.info("DVDV-Server-List: " + dvdvServerList);
                LOG.info("LookupList: " + lookupProvider);
                LOG.info("Permissions: " + permissions);
            }
        } catch (DocumentException e) {
            throw new ConfigException(e);
        } catch (ValidationException e) {
            throw new ConfigException(e);
        }
    }

    public void dispose() {
        if (crlReloader != null) {
            crlReloader.interrupt();
            crlReloader = null;
        }
    }

    protected void finalize() throws Throwable {
        dispose();
        super.finalize();
    }

    public URL getURL() {
        return url;
    }

    private void load(Element element) throws ConfigException {
        final List list = element.elements();

        for (int i = 0; i < list.size(); i++) {
            Element e = (Element)list.get(i);
            final String name = e.getName();
            final String parent = e.getParent().getName();

            if ("keystore".equals(name) && "directory".equals(parent)) {
                final KeyStore keyStore = loadKeystore(e);
                final String id = e.attributeValue("id");

                if (id == null) {
                    throw new ConfigException("Top-Level KeyStore configuration is missing the 'id' attribute");
                }
                keystoreMap.put(id, keyStore);
            } else if ("type".equals(name) && "typemap".equals(parent)) {
                loadTypeMapping(e);
            } else if ("dvdv-server".equals(name) && "dvdv-server-list".equals(parent)) {
                loadDVDVServer(e);
            } else if ("lookup".equals(name) && "lookuplist".equals(parent)) {
                loadLookup(e);
            } else if ("service".equals(name) && "permissionlist".equals(parent)) {
                loadPermission(e);
            } else if ("certstore".equals(name) && "certstores".equals(parent)) {
                loadCertStore(e);
            } else if ("inboxes".equals(name) && "directory".equals(parent)) {
                loadInboxes(e);
            } else if ("xta-config".equals(name) && "directory".equals(parent)) {
                loadXtaConfig(e);
            } else {
                load(e);
            }
        }
    }

    private void loadInboxes(Element element) throws ConfigException {
        myInboxConfig = new InboxConfig();

        final List list = element.elements();
        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("issuer".equals(name)) {
                loadIssuer(myInboxConfig.myIssuerMap, e);
            } else if ("certificates".equals(name)) {
                loadCertificates(myInboxConfig.myCertificates, e);
            }
        }

        LOG.info("Inboxes: " + myInboxConfig);
    }

    private void loadXtaConfig(Element element) throws ConfigException {

        final List list = element.elements();
        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("account".equals(name)) {
                final Element sslCert = e.element(QName.get("identified-by", NAMESPACE));
                final HashSet certificates = new HashSet();
                loadCertificates(certificates, sslCert);
                final X509Certificate cert = (X509Certificate)certificates.iterator().next();

                final CertificateReference ref;
                final Element mappedKey = e.element(QName.get("map-to-key", NAMESPACE));
                if (mappedKey != null) {
                    final Element keystore = mappedKey.element(QName.get("keystore", NAMESPACE));
                    final KeyStore keyStore = loadKeystore(keystore);

                    final Element key = mappedKey.element(QName.get("key", NAMESPACE));
                    final String alias = key.attributeValue("alias");
                    final String password = key.attributeValue("password");

                    try {
                        ref = new CertificateReference(keyStore, alias, password);
                    } catch (KeyStoreException e1) {
                        throw new RuntimeException(e1);
                    }
                } else {
                    throw new IllegalStateException("Element map-to-key is required");
                }

                final String n = sslCert.attributeValue("name");
                final String[] strings = n != null ? n.split(",") : new String[]{""};
                for (int j = 0; j < strings.length; j++) {
                    final String prefix = sslCert.attributeValue("prefix");
                    myXtaConfig.addAccount(cert, new XtaConfig.Account(prefix == null ? "" : prefix, strings[j].trim(), ref));
                }
            } else if ("services".equals(name)) {
                final HashSet xtaServices = new HashSet();
                final List services = e.elements(QName.get("service", NAMESPACE));
                for (int j = 0; j < services.size(); j++) {
                    final Element service = (Element)services.get(j);
                    xtaServices.add(service.getTextTrim());
                }
                myXtaConfig.setServices(xtaServices);
            }
        }

        LOG.info("XtaConfig: " + myXtaConfig);
    }

    private void loadCertStore(Element element) throws ConfigException {
        final String type = element.attributeValue("type");

        if ("simple".equals(type)) {
            final List list = element.elements();
            final Set certs = new HashSet();
            KeyStore keyStore = null;

            for (int i = 0; i < list.size(); i++) {
                final Element e = (Element)list.get(i);

                if ("keystore".equals(e.getName())) {
                    keyStore = loadKeystore(e);
                } else if ("certificate".equals(e.getName())) {
                    try {
                        if ("*".equals(e.attributeValue("alias"))) {
                            if (keyStore == null) {
                                throw new ConfigException("Missing keystore");
                            }
                            addAllCertificates(certs, keyStore);
                            handleTrustedCerts(certs, e);
                        } else {
                            final X509Certificate cert = loadCertificate(keyStore, e, KeyStoreUtil.CA_USAGE);

                            if (keyStore != null) {
                                final Certificate[] chain = keyStore.getCertificateChain(keyStore.getCertificateAlias(cert));

                                if (chain != null) {
                                    for (int j = 0; j < chain.length; j++) {
                                        Certificate certificate = chain[j];
                                        certs.add(certificate);
                                    }
                                } else if (cert != null) {
                                    certs.add(cert);
                                } else {
                                    LOG.warn("Alias <" + e.attributeValue("alias") + "> does not point to a CA-capable certificate.");
                                }
                            } else {
                                certs.add(cert);
                            }
                            handleTrustedCert(cert, e);
                        }
                    } catch (KeyStoreException e1) {
                        throw new ConfigException(e1);
                    }
                } else if ("crl".equals(e.getName())) {
                    X509CRL crl = loadCRL(e);
                    certs.add(crl);
                }
            }
            CertStore store = createCertStore("Collection", new CollectionCertStoreParameters(certs));
            certStores.add(store);
        } else if ("ldap".equals(type)) {
            final String server = element.attributeValue("server");
            final String port = element.attributeValue("port");
            if (LOG.isInfoEnabled()) {
                LOG.info("Adding LDAP CertStore on " + server);
            }

            if (port == null) {
                certStores.add(createCertStore("LDAP", new LDAPCertStoreParameters(server)));
            } else {
                certStores.add(createCertStore("LDAP", new LDAPCertStoreParameters(server, Integer.parseInt(port))));
            }
        } else if ("pkcs7".equals(type)) {
            final String file = element.attributeValue("file");

            if (file == null) throw new ConfigException("Missing attribute 'file' for PKCS#7 CertStore");
            if (LOG.isInfoEnabled()) {
                LOG.info("Adding PKCS#7 CertStore from " + file);
            }

            try {
                final InputStream stream;

                if (file.startsWith("/") || file.indexOf(':') == 1) {
                    stream = new FileInputStream(file);
                } else {
                    final URL url = makeURL(file);

                    if (url == null) {
                        throw new ConfigException("CertStore location '" + file + "' did not resolve to a valid URL");
                    }

                    if (LOG.isInfoEnabled()) {
                        LOG.info("PKCS#7 CertStore URL: " + url);
                    }
                    stream = url.openStream();
                }
                final CertPath path;
                try {
                    path = CertificateFactory.getInstance("X.509", "BC").generateCertPath(stream, "PKCS7");
                } finally {
                    stream.close();
                }
                final List certificates = new ArrayList(path.getCertificates());
                final List list = element.elements();

                for (int i = 0; i < list.size(); i++) {
                    Element e = (Element)list.get(i);

                    if ("crl".equals(e.getName())) {
                        X509CRL crl = loadCRL(e);
                        certificates.add(crl);
                    }
                }
                certStores.add(createCertStore("Collection", new CollectionCertStoreParameters(certificates)));

                if ("true".equals(element.attributeValue("trustRoot"))) {

                    for (Iterator iterator = certificates.iterator(); iterator.hasNext();) {
                        Object o = iterator.next();

                        if (o instanceof X509Certificate) {
                            X509Certificate certificate = (X509Certificate)o;

                            if (isIssuer(certificate, certificate)) {
                                if (LOG.isInfoEnabled()) {
                                    LOG.info("Adding <" + certificate.getSubjectDN().getName() + "> as trusted root");
                                }
                                trustanchors.add(new TrustAnchor(certificate, null));
                            }
                        }
                    }
                }
            } catch (ConfigException e) {
                throw e;
            } catch (Exception e) {
                throw (ConfigException)new ConfigException("Unable to create CertStore from PKCS#7 file").initCause(e);
            }
        } else {
            throw new ConfigException("Unknown CerStore type: " + type);
        }
    }

    private X509CRL loadCRL(Element e) throws ConfigException {
        final String url = e.attributeValue("url");
        boolean receiveImmediately = Boolean.valueOf(e.attributeValue("receiveImmediately")).booleanValue();

        if (url != null && url.length() > 0) {
            if (crlReloader == null) {
                crlReloader = new CRLReloader();
                crlReloader.start();
            }
            try {
                return new CachedCRL(makeURL(url), receiveImmediately, crlReloader);
            } catch (MalformedURLException e1) {
                throw new ConfigException(e1);
            }
        }
        throw new ConfigException("crl-Element missing needed attribute 'url'");
    }

    private CertStore createCertStore(final String storeType, final CertStoreParameters params) throws ConfigException {
        try {
            return CertStore.getInstance(storeType, params);
        } catch (GeneralSecurityException e) {
            LOG.error("Unexpected error creating CertStore: " + storeType);
            throw new ConfigException(e);
        }
    }

    private void handleTrustedCerts(Set certs, Element element) {
        final Iterator iterator = certs.iterator();
        while (iterator.hasNext()) {
            X509Certificate certificate = (X509Certificate)iterator.next();
            handleTrustedCert(certificate, element);
        }
    }

    private void handleTrustedCert(X509Certificate certificate, Element element) {
        if ("true".equals(element.attributeValue("isTrustedRoot"))) {
            if (!isIssuer(certificate, certificate)) {
                LOG.warn("Certificate <" + certificate.getSubjectDN().getName() + "> is not a root certificate. isTrustedRoot is ignored.");
            } else {
                if (LOG.isInfoEnabled()) {
                    LOG.info("Adding <" + certificate.getSubjectDN().getName() + "> as trusted root");
                }
                trustanchors.add(new TrustAnchor(certificate, null));
            }
        }
    }

    private void loadPermission(Element element) throws ConfigException {
        final String serviceName = element.attributeValue("name");

        if (serviceName != null) {
            if (!permissions.containsKey(serviceName)) {
                final PermissionEntry pe = new PermissionEntry(serviceName);

                final List list = element.elements();
                for (int i = 0; i < list.size(); i++) {
                    Element e = (Element)list.get(i);

                    if ("unrestricted".equals(e.getName())) {
                        pe.setUnrestricted(true);
                        break;
                    } else if ("disallowed".equals(e.getName())) {
                        loadDisallowed(pe, e);
                    } else if ("allowed".equals(e.getName())) {
                        loadAllowed(pe, e);
                    } else if ("dvdv-server".equals(e.getName())) {
                        if (dvdvServerList.size() == 0) {
                            throw new ConfigException("No DVDV server configured");
                        }
                        pe.setDVDVServer((DVDVServer)dvdvServerList.get(0));
                        final List categories = e.elements();

                        for (int j = 0; j < categories.size(); j++) {
                            Element category = (Element)categories.get(j);
                            pe.addCategory(category.getTextTrim());
                        }
                    }
                }
                permissions.put(pe.getService(), pe);
            } else {
                throw new ConfigException("There is more than one server-entry named '" + serviceName + "' in permission-list");
            }
        } else {
            throw new ConfigException("Service-Element in Permission-List is missing the 'name' attribute");
        }
    }

    private void loadAllowed(PermissionEntry pe, Element element) throws ConfigException {
        loadServicePermissions(pe.getAllowedIssuer(), pe.getAllowedCerificates(), element);
    }

    private void loadDisallowed(PermissionEntry pe, Element element) throws ConfigException {
        loadServicePermissions(pe.getDisallowedIssuer(), pe.getDisallowedCerificates(), element);
    }

    private void loadServicePermissions(Map issuer, Set certificates, Element element) throws ConfigException {
        List list = element.elements();

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("issuer".equals(name)) {
                loadIssuer(issuer, e);
            } else if ("certificates".equals(name)) {
                loadCertificates(certificates, e);
            }
        }
    }

    private void loadCertificates(Set certificates, Element element) throws ConfigException {
        final List list = element.elements();
        KeyStore keyStore = null;

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("keystore".equals(name)) {
                keyStore = loadKeystore(e);
            } else if ("certificate".equals(name)) {
                if ("*".equals(e.attributeValue("alias"))) {
                    if (keyStore == null) {
                        throw new ConfigException("Need a keystore to use certificate wildcards");
                    }
                    addAllCertificates(certificates, keyStore);
                } else {
                    final Certificate certificate = loadCertificate(keyStore, e, KeyStoreUtil.ANY_USAGE);
                    certificates.add(certificate);
                }
            }
        }
    }

    private void addAllCertificates(Set certificates, KeyStore keyStore) throws ConfigException {
        try {
            final Enumeration enumeration = keyStore.aliases();

            while (enumeration.hasMoreElements()) {
                String s = (String)enumeration.nextElement();
                final Certificate cert = keyStore.getCertificate(s);

                if (cert != null) {
                    certificates.add(cert);
                }
            }
        } catch (KeyStoreException e) {
            throw new ConfigException(e);
        }
    }

    private void loadIssuer(Map issuerMap, Element element) throws ConfigException {
        final List list = element.elements();
        KeyStore keyStore = null;

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("keystore".equals(name)) {
                keyStore = loadKeystore(e);
            } else if ("certificate".equals(name)) {
                if ("*".equals(e.attributeValue("alias"))) {
                    if (keyStore == null) {
                        throw new ConfigException("Need a keystore to use certificate wildcards");
                    }
                    addAllIssuerCertificates(issuerMap, keyStore);
                } else {
                    final X509Certificate certificate = loadCertificate(keyStore, e, KeyStoreUtil.ANY_USAGE);
                    issuerMap.put(certificate.getSubjectDN().getName(), certificate.getPublicKey());
                }
            }
        }
    }

    private void addAllIssuerCertificates(Map issuerMap, KeyStore keyStore) throws ConfigException {
        try {
            final Enumeration enumeration = keyStore.aliases();

            while (enumeration.hasMoreElements()) {
                String s = (String)enumeration.nextElement();
                final Certificate cert = keyStore.getCertificate(s);

                if (cert != null) {
                    final X509Certificate x509Certificate = (X509Certificate)cert;
                    issuerMap.put(x509Certificate.getSubjectDN().getName(), x509Certificate.getPublicKey());
                }
            }
        } catch (KeyStoreException e) {
            throw new ConfigException(e);
        }
    }

    private void loadLookup(Element element) throws ConfigException {
        final String service = element.attributeValue("service");
        final String alias = element.attributeValue("key");
        LookupEntryProvider le;

        try {
            Element e = element.element("dvdv-server");
            if (e != null) {
                le = getDVDVLookupEntryProvider(e, service, alias);
            } else {
                le = loadLookupEntry(element, service, alias);
            }
        } catch (MalformedURLException e) {
            throw new ConfigException(e);
        }

        final StringTokenizer st = new StringTokenizer(alias, ",");

        if (st.countTokens() > 1) {
            while (st.hasMoreTokens()) {
                String s = st.nextToken();
                lookupProvider.add(le.copyAs(service, s.trim()));
            }
        } else {
            lookupProvider.add(le);
        }
    }

    private LookupEntryProvider getDVDVLookupEntryProvider(Element element, String service, String alias) throws ConfigException {
        if (dvdvServerList.size() > 0) {
            final DVDVLookupEntryProvider de = new DVDVLookupEntryProvider(service, alias, (DVDVServer)dvdvServerList.get(0));

            List list = element.elements();

            for (int i = 0; i < list.size(); i++) {
                final Element e = (Element)list.get(i);
                final String name = e.getName();

                if ("keystore".equals(name)) {
                    KeyStore store = loadKeystore(e);
                    de.setKeyStore(store);
                } else if ("key-prefix".equals(name)) {
                    de.setKeyPrefix(e.getTextTrim());
                }
            }
            return de;
        } else {
            throw new ConfigException("No DVDV-Server configured.");
        }
    }

    private LookupEntryProvider loadLookupEntry(Element element, String service, String alias) throws ConfigException, MalformedURLException {
        KeyStore ks = null;
        final List list = element.elements();
        final LookupEntry le = new LookupEntry(service, alias);

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            final String name = e.getName();

            if ("keystore".equals(name)) {
                ks = loadKeystore(e);
            } else if ("intermediary".equals(name)) {
                loadIntermediary(ks, le, e.attributeValue("url"), e.elements());
            } else if ("addressee".equals(name)) {
                loadAddressee(ks, le, e);
            }
        }
        return le;
    }

    private void loadAddressee(KeyStore ks, LookupEntry lookupEntry, Element element) throws ConfigException {
        final List list = element.elements();
        lookupEntry.setContentReceiver(element.attributeValue("uri"), Boolean.valueOf("true".equals(element.attributeValue("oneway"))));
        lookupEntry.setKeystore(ks);

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);
            tryLoadCertficate(lookupEntry, ks, e);
        }
    }

    private void tryLoadCertficate(LookupEntry lookupEntry, KeyStore keyStore, Element element) throws ConfigException {
        final String name = element.getName();
        if ("ciphercertificate".equals(name)) {
            lookupEntry.setCipherCert(loadCertificate(keyStore, element, KeyStoreUtil.CIPHER_USAGE));
        } else if ("signaturecertificate".equals(name)) {
            lookupEntry.setSigCert(loadCertificate(keyStore, element, KeyStoreUtil.SIGNATURE_USAGE));
        }
    }

    private void loadIntermediary(KeyStore keyStore, LookupEntry lookupEntry, String url, List list) throws MalformedURLException, ConfigException {
        lookupEntry.setIntermediaryURL(new URL(url));

        for (int i = 0; i < list.size(); i++) {
            final Element e = (Element)list.get(i);

            if ("ciphercertificate".equals(e.getName())) {
                lookupEntry.setIntermediaryCert(loadCertificate(keyStore, e, KeyStoreUtil.CIPHER_USAGE));
            }
        }
    }

    private X509Certificate loadCertificate(KeyStore ks, Element element, int[] purpose) throws ConfigException {
        try {
            final String alias = element.attributeValue("alias");

            if (alias != null) {
                if (ks == null) {
                    throw new ConfigException("Need a keystore to use certificate aliases");
                } else if ("*".equals(alias)) {
                    // will be loaded on request from keystore
                    return null;
                } else {
                    final X509Certificate x509Certificate = (X509Certificate)ks.getCertificate(alias);
                    if (x509Certificate == null) {
                        final List l = new ArrayList();
                        final Enumeration aliases = ks.aliases();
                        while (aliases.hasMoreElements()) {
                            String s = (String)aliases.nextElement();
                            l.add(s);
                        }
                        throw new ConfigException("Cannot find certificate for alias " + alias + ". Valid aliases are: " + l);
                    }
                    return x509Certificate;
                }
            } else {
                final String name = element.attributeValue("name");

                if (name != null && purpose != null) {
                    final X509Certificate cert = KeyStoreUtil.getCertByDN(ks, name, purpose);
                    if (cert == null) {
                        throw new ConfigException("Cannot find cert for DN " + name);
                    }
                    return cert;
                } else if (element.getTextTrim().length() > 0) {
                    final byte[] bytes = Base64.decode(element.getTextTrim());
                    return (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bytes));
                } else {
                    throw new ConfigException("Missing certificate data. Either use alias/name attribute or embed base64 encoded certificate");
                }
            }
        } catch (CertificateException e1) {
            throw new ConfigException(e1);
        } catch (KeyStoreException e1) {
            throw new ConfigException(e1);
        } catch (cs.impl.util.Base64DecodingException e1) {
            throw new ConfigException(e1);
        }
    }

    private void loadTypeMapping(Element element) throws ConfigException {
        final String messageType = element.attributeValue("name");

        if (messageType != null) {
            final Element serviceElement = element.element("service");

            if (serviceElement != null) {
                final String service = serviceElement.getText();

                if (service.length() > 0) {
                    typeMap.put(messageType, service);
                } else {
                    throw new ConfigException("Service-Element of type '" + messageType + "' contains no value");
                }
            } else {
                throw new ConfigException("Type configuration is missing the 'service' element");
            }
        } else {
            throw new ConfigException("Type configuration is missing the 'name' attribute");
        }
    }

    private void loadDVDVServer(Element element) throws ConfigException {
        final String name = element.attributeValue("name");

        if (name != null) {
            final DVDVServer server = new DVDVServer(name);
            final List list = element.elements();


            for (int i = 0; i < list.size(); i++) {
                final Element e = (Element)list.get(i);
                final String wsdl = e.getName();

                if ("wsdl".equals(wsdl)) {
                    String url = e.attributeValue("url");

                    if (url != null && url.length() > 0) {
                        try {
                            final URL u = makeURL(url);
                            if (!"file".equals(u.getProtocol())) {
                                throw new ConfigException("DVDV WSDL URL must point to a local file");
                            }
                            server.addWsdlUrl(u.getFile());
                        } catch (MalformedURLException e1) {
                            throw new ConfigException("Invalid QSDL URL <" + url + ">", e1);
                        }
                    } else {
                        throw new ConfigException("Unable to get WSDL-URL for DVDV-Server '" + name + "'");
                    }
                } else {
                    throw new ConfigException("Unexpected element in DVDV-Server configuration '" + wsdl + "'");
                }
            }
            dvdvServerList.add(server);
        } else {
            throw new ConfigException("DVDV-Server configuration is missing the 'name' attribute");
        }
    }

    private KeyStore loadKeystore(Element element) throws ConfigException {
        final String refid;
        if ((refid = element.attributeValue("refid")) != null) {
            KeyStore ks = (KeyStore)keystoreMap.get(refid);
            if (ks == null) {
                throw new ConfigException("Unknown KeyStore id '" + refid + "'");
            }
            return ks;
        }

        final String type = element.attributeValue("type");
        final String url = element.attributeValue("url");
        final String password = element.attributeValue("password");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Loading KeyStore from " + url);
        }

        try {
            KeyStore ks = KeyStore.getInstance(type);
            final URL ksURL = makeURL(url);
            if (ksURL == null) {
                LOG.warn("KeyStore URL " + url + " did not resolve to a local resource");
                return null;
            }
            ks.load(ksURL.openStream(), password.toCharArray());

            return ks;
        } catch (Exception ex) {
            throw new ConfigException(ex);
        }
    }

    private URL makeURL(String url) throws MalformedURLException {
        final URL ksURL;
        if (url.startsWith("res:/")) {
            ksURL = getClass().getClassLoader().getResource(url.substring(5));
        } else if (url.indexOf(':') == -1) {
            ksURL = new URL(getURL(), url);
        } else {
            ksURL = new URL(url);
        }
        return ksURL;
    }

    private boolean isIssuer(final X509Certificate issuer, X509Certificate sender) {
        final String s = "[" + issuer.getSubjectDN().getName() + "] is issuer of [" + sender.getSubjectDN().getName() + "]";
        if (LOG.isDebugEnabled()) {
            LOG.debug("Testing if " + s);
        }

        if (sender.getIssuerDN().equals(issuer.getSubjectDN())) {
            try {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Positive. Checking if the signature matches.");
                }

                sender.verify(issuer.getPublicKey());

                if (LOG.isDebugEnabled()) {
                    LOG.debug("Positive. " + s);
                }
                return true;
            } catch (SignatureException e) {
                LOG.warn("Signature doesn't match, even though the DNs match.", e);
                return false;
            } catch (InvalidKeyException e) {
                LOG.warn("Signature doesn't match, even though the DNs match.", e);
                return false;
            } catch (Exception e) {
                LOG.error("Problem checking certificate issuer", e);
                return false;
            }
        } else {
            return false;
        }
    }

    public LookupEntry lookup(String messageType, String key) throws UnknownDestinationException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Lookup for messageType '" + messageType + "' and key '" + key + "'");
        }

        LookupKey lk = LookupKey.create(messageType, key);

        synchronized (lookupEntryCache) {
            if (lookupEntryCache.containsKey(lk)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found cached entry.");
                }

                LookupEntry entry = (LookupEntry)lookupEntryCache.get(lk);

                // if entry is expired (not valid) -> reload
                if (entry.isValid()) {
                    return entry;
                } else {
                    lookupEntryCache.remove(lk);
                }
            }
        }
        return getLookupEntry(messageType, key, lk);
    }

    private LookupEntry getLookupEntry(String messageType, String key, LookupKey lk) throws UnknownDestinationException {
        String service = translateMessageTypeToService(messageType);

        for (Iterator iterator = lookupProvider.iterator(); iterator.hasNext();) {
            LookupEntryProvider lep = (LookupEntryProvider)iterator.next();
            LookupEntry entry = lep.getLookupEntry(service, key);

            if (entry != null && entry.isValid() && entry.getIntermediaryURL() != null) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found entry: " + entry);
                }
                synchronized (lookupEntryCache) {
                    lookupEntryCache.put(lk, entry);
                    return entry;
                }
            }
        }
        // if there is no or no valid entry, throw exception
        throw new UnknownDestinationException(messageType, key);
    }

    public String translateMessageTypeToService(String messageType) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Search service for message-type '" + messageType + "'");
        }
        String service = null;

        synchronized (typeMapCache) {
            if (typeMapCache.containsKey(messageType)) {
                service = (String)typeMapCache.get(messageType);
            } else {
                final Iterator iterator = typeMap.keySet().iterator();

                while (iterator.hasNext()) {
                    String messageTypeName = (String)iterator.next();
                    String exp = PatternUtil.convertToRegex(messageTypeName);

                    if (messageType.matches(exp)) {
                        service = (String)typeMap.get(messageTypeName);
                        typeMapCache.put(messageType, service);
                        break;
                    }
                }
            }
        }

        if (service == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("There is no mapping for '" + messageType + "'. Use messageType as service-name");
            }
            service = messageType;
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Got mapping for '" + messageType + "'. Service-Name is '" + service + "'");
            }
        }
        return service;
    }

    public AccessDescriptor checkAccess(X509Certificate sender, String messageType) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Check access for '" + sender.getSubjectDN().getName() + "' for message-type '" + messageType + "'");
        }
        final String service = translateMessageTypeToService(messageType);
        PermissionEntry permission = (PermissionEntry)permissions.get(service);

        if (permission == null) {
            permission = (PermissionEntry)permissions.get("#any");
        }

        if (permission != null) {
            return permission.checkAccess(sender);
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Got no permission-entry for '" + messageType + "' (" + service + ")");
            }
            return AccessDescriptor.UNDEFINED;
        }
    }

    /** @noinspection ReturnOfNull */
    public X509Certificate[] getCertPath(X509Certificate certificate) {
        if (trustanchors.isEmpty()) {
            LOG.warn("No trusted roots configured. Cannot build a CertPath");
            return null;
        }

        if (certificate.getIssuerX500Principal().equals(certificate.getSubjectX500Principal())) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Self-signed certificate: <" + certificate.getSubjectDN().getName() + ">");
            }
            return null;
        }

        try {
            final CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
            final X509CertSelector selector = new X509CertSelector();
            selector.setCertificate(certificate);
            selector.setSubject(certificate.getSubjectX500Principal().getEncoded());
            selector.setIssuer(certificate.getIssuerX500Principal().getEncoded());
            final PKIXBuilderParameters params = new PKIXBuilderParameters(trustanchors, selector);
            params.setCertStores(certStores);
            params.addCertStore(singletonStore(certificate));
            params.setRevocationEnabled(false);
            params.setDate(certificate.getNotBefore()); // CertPathBuilder should not check certificate validity because OSCI does this itself
            final CertPath certPath = builder.build(params).getCertPath();
            final X509Certificate root = ((PKIXCertPathBuilderResult)builder.build(params)).getTrustAnchor().getTrustedCert();

            if (root != certificate) {
                X509Certificate[] ret = new X509Certificate[certPath.getCertificates().size() + 1];
                certPath.getCertificates().toArray(ret);
                ret[ret.length - 1] = root;
                return ret;
            } else {
                // should not happen
                X509Certificate[] ret = new X509Certificate[certPath.getCertificates().size()];
                certPath.getCertificates().toArray(ret);
                return ret;
            }
        } catch (NoSuchAlgorithmException e) {
            throw new Error("This should not happen.");
        } catch (CertPathBuilderException e) {
            LOG.error("Could not build CertPath for <" + certificate.getSubjectDN().getName() + ">: " + e.getMessage());
            LOG.debug("Building CertPath failed", e);
            return null;
        } catch (GeneralSecurityException e) {
            LOG.error("Could not build CertPath for <" + certificate.getSubjectDN().getName() + ">", e);
            return null;
        } catch (IOException e) {
            LOG.error("Could not build CertPath for <" + certificate.getSubjectDN().getName() + ">", e);
            return null;
        }
    }

    private CertStore singletonStore(X509Certificate cert) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        return CertStore.getInstance("Collection", new CollectionCertStoreParameters(Collections.singletonList(cert)));
    }

    public boolean isTrustedRoot(X509Certificate certificate) {
        for (Iterator iterator = trustanchors.iterator(); iterator.hasNext();) {
            final TrustAnchor trustAnchor = (TrustAnchor)iterator.next();

            if (certificate.equals(trustAnchor.getTrustedCert())) {
                return true;
            }
        }
        return false;
    }

    public X509CRL[] getCRLs(X509Certificate tbvCert) {
        try {
            synchronized (myCRLCacheLock) {
                if (myCRLRootStore == null) {
                    final Set r = new HashSet();

                    for (Iterator iterator1 = trustanchors.iterator(); iterator1.hasNext();) {
                        TrustAnchor trustAnchor = (TrustAnchor)iterator1.next();
                        r.add(trustAnchor.getTrustedCert());
                    }

                    // This code basically iterates over all locally configured certificates and adds those to the
                    // trusted CRL issuer store that can be traced to a trusted root-certificate.
                    // A simpler approach might be to just allow *any* locally configured certificate to be a valid CRL
                    // issuer root though.
                    int rootCount;
                    MyCRLTrustSelector selector;
                    do {
                        rootCount = r.size();
                        selector = new MyCRLTrustSelector(r);

                        for (Iterator iterator = certStores.iterator(); iterator.hasNext();) {
                            final CertStore store = (CertStore)iterator.next();
                            r.addAll(store.getCertificates(selector));
                        }
                    } while (rootCount != r.size());

                    myCRLRootStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(r));
                    if (LOG.isDebugEnabled()) {
                        for (Iterator it = r.iterator(); it.hasNext();) {
                            X509Certificate cert = (X509Certificate)it.next();
                            LOG.debug("Trusted CRL issuer: " + cert.getSubjectDN().getName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Getting CRL", e);
            return new X509CRL[0];
        }

        final List crls = new ArrayList();
        final byte[] issuer = tbvCert.getIssuerX500Principal().getEncoded();
        for (Iterator iterator = certStores.iterator(); iterator.hasNext();) {
            try {
                final CertStore store = (CertStore)iterator.next();
                final X509CRLSelector selector = new X509CRLSelector();
                selector.addIssuerName(issuer);
                final Collection c = store.getCRLs(selector);

                for (Iterator iterator1 = c.iterator(); iterator1.hasNext();) {
                    final X509CRL crl = (X509CRL)iterator1.next();
                    verifyAndAddCRL(crl, myCRLRootStore, crls);
                }
            } catch (Exception e) {
                LOG.error("Getting CRL", e);
            }
        }

        return (X509CRL[])crls.toArray(new X509CRL[crls.size()]);
    }

    private void verifyAndAddCRL(final X509CRL crl, final CertStore rootstore, final List crls) throws IOException, CertStoreException {
        if (crl instanceof CachedCRL && !((CachedCRL)crl).isLoaded()) {
            LOG.warn("CRL from '" + ((CachedCRL)crl).getUrl() + " hasn't been loaded yet.");
            return;
        }
        final String crlName = crl.getIssuerDN().getName();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Verifying CRL: " + crlName);
        }

        final X509CertSelector cs = new X509CertSelector();
        cs.setSubject(crl.getIssuerX500Principal());
        final Collection certificates = rootstore.getCertificates(cs);

        for (Iterator iterator2 = certificates.iterator(); iterator2.hasNext();) {
            X509Certificate certificate = (X509Certificate)iterator2.next();
            if (!certificate.getSubjectX500Principal().equals(crl.getIssuerX500Principal())) {
                LOG.warn("CertSelector returned non-matching certificate for CRL - verification will fail: " + certificate.getSubjectDN().getName() + " vs. " + crlName);
            }

            if (LOG.isDebugEnabled()) {
                LOG.debug("Verifying CRL: " + crlName + " with certificate " + certificate.getSubjectDN().getName());
            }

            try {
                crl.verify(certificate.getPublicKey());
                crls.add(crl);
                return;
            } catch (InvalidKeyException e) {
                if (LOG.isDebugEnabled()) {
                    LOG.warn("CRL(" + crlName + ") not signed by <" + certificate.getSubjectDN().getName() + ">", e);
                } else {
                    LOG.warn("CRL(" + crlName + ") not signed by <" + certificate.getSubjectDN().getName() + ">");
                }
            } catch (Exception e) {
                LOG.warn("CRL(" + crlName + ") verification error with " + certificate.getSubjectDN().getName(), e);
            }

            if (Boolean.getBoolean("ecs.directory.ignore-crl-verification")) {
                LOG.warn("Adding unverified CRL (ecs.directory.ignore-crl-verification=true)");
                crls.add(crl);
                return;
            }
        }
        LOG.warn("Could not find a trusted root for CRL: <" + crlName + ">");
    }

    public boolean isValidInbox(X509Certificate cert) {
        if (myInboxConfig == null) {
            return true;
        } else if (myInboxConfig.isPermitted(cert)) {
            return true;
        } else if (myXtaConfig.getAccounts(cert.getSubjectDN().getName()).length > 0) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Implicit inbox for XTA account <" + cert.getSubjectDN().getName() + ">");
            }
            return true;
        }
        return false;
    }

    public XtaConfig getXtaAccountConfig() {
        return myXtaConfig;
    }

    public void clearCaches() {
        synchronized (lookupEntryCache) {
            LOG.info("Clearing lookup caches (" + lookupEntryCache.size() + " entries)");
            lookupEntryCache.clear();
        }
        synchronized (typeMapCache) {
            LOG.info("Clearing typemap caches (" + lookupEntryCache.size() + " entries)");
            typeMapCache.clear();
        }
    }

    private static class InboxConfig {
        final Map myIssuerMap = new HashMap();
        final Set myCertificates = new HashSet();

        public boolean isPermitted(X509Certificate cert) {
            if (myCertificates.contains(cert)) {
                return true;
            }
            final PublicKey key = (PublicKey)myIssuerMap.get(cert.getIssuerDN().getName());
            if (key != null) {
                return CertUtil.isIssuer(key, cert);
            }
            return false;
        }

        public String toString() {
            return "InboxConfig{" +
                    "myCertificates=" + myCertificates +
                    ", myIssuerMap=" + myIssuerMap +
                    '}';
        }
    }

    private class MyCRLTrustSelector implements CertSelector {
        private final Collection myRoots;

        public MyCRLTrustSelector(Collection r) {
            myRoots = new ArrayList(r);
        }

        /** @noinspection CloneDoesntCallSuperClone*/
        public Object clone() {
            return new MyCRLTrustSelector(new ArrayList(myRoots));
        }

        public boolean match(Certificate cert) {
            final X509Certificate c = (X509Certificate)cert;
            boolean[] keyUsage = c.getKeyUsage();
            if (keyUsage == null || !keyUsage[6/*cRLSign*/]) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Cert <" + c.getSubjectDN().getName() + "> doesn't have cRLSign KeyUsage set");
                }

                return false;
            }
            // filters certificates that are issued by any certificate in myRoots
            for (Iterator it = myRoots.iterator(); it.hasNext();) {
                X509Certificate test = (X509Certificate)it.next();
                if (isIssuer(test, c)) {
                    return true;
                }
            }
            return false;
        }
    }


    public static class XtaConfigImpl implements XtaConfig {

        private final Map/*<X509Certificate, List<XtaAccount>>*/ myAccounts = new HashMap();
        private final Map/*<String, XtaAccount>*/ myReverseLookup = new HashMap();

        private Set myServices = Collections.EMPTY_SET;

        public Set getServices() {
            return myServices;
        }

        public Account[] getAccounts() {
            final List accounts = new ArrayList();
            final Collection values = myAccounts.values();
            for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
                final List next = (List)iterator.next();
                accounts.addAll(next);
            }
            return (Account[])accounts.toArray(new Account[accounts.size()]);
        }

        void setServices(Set services) {
            myServices = services;
        }

        public synchronized Account[] getAccounts(String mappedCertDN) {
            final Account[] cachedAccounts = (Account[])myReverseLookup.get(mappedCertDN);
            if (cachedAccounts != null) {
                return cachedAccounts;
            }

            try {
                final DName dn = new DName(mappedCertDN);
                final List _accounts = new ArrayList();
                for (Iterator iterator = myAccounts.values().iterator(); iterator.hasNext(); ) {
                    final List next = (List)iterator.next();
                    for (Iterator it = next.iterator(); it.hasNext(); ) {
                        final Account account = (Account)it.next();
                        final CertificateReference certRef = account.getMappedCertificate();
                        if (dn.matches(certRef.getCertificate().getSubjectDN().getName())) {
                            _accounts.add(account);
                        }
                    }
                }
                final Account[] accounts = (Account[])_accounts.toArray(new Account[_accounts.size()]);
                myReverseLookup.put(mappedCertDN, accounts);
                return accounts;
            } catch (DNParseException e) {
                throw new RuntimeException(e);
            }
        }

        public Account getAccount(X509Certificate sslCert, String accountPrefix, String accountId) {
            final List xtaAccounts = (List)myAccounts.get(sslCert);
            if (xtaAccounts == null || xtaAccounts.size() == 0) {
                LOG.info("No account for certificate <" + sslCert.getSubjectDN().getName() + ">");
                return null;
            }

            if (xtaAccounts.size() == 1) {
                final Account a = (Account)xtaAccounts.get(0);
                if (a.prefix.equals(accountPrefix) && a.name.equals(accountId)) {
                    LOG.debug("Account for certificate <" + sslCert.getSubjectDN().getName() + ">: " + a);
                    return a;
                } else if (accountPrefix != null || accountId != null) {
                    LOG.warn("Account for certificate <" + sslCert.getSubjectDN().getName() + ">: " + a + " does not match requested account: <" + accountPrefix + ":" + accountId + ">");
                    return null;
                } else {
                    LOG.debug("No prefix & id. Single account for certificate <" + sslCert.getSubjectDN().getName() + ">: " + a);
                    return a;
                }
            }

            Account fallback = null;
            for (int i = 0; i < xtaAccounts.size(); i++) {
                final Account a = (Account)xtaAccounts.get(i);
                if (a.prefix.equals(accountPrefix) && a.name.equals(accountId)) {
                    LOG.debug("Account for certificate <" + sslCert.getSubjectDN().getName() + ">: " + a);
                    return a;
                } else if (a.getFullName() == null || a.getFullName().length() == 0) {
                    fallback = a;
                }
            }
            if (fallback != null) {
                return fallback;
            }

            LOG.warn("No account for certificate <" + sslCert.getSubjectDN().getName() + "> matches requested account: <" + accountPrefix + ":" + accountId + ">");
            return null;
        }

        void addAccount(X509Certificate cert, Account xtaAccount) {
            final List o = (List)myAccounts.get(cert);
            if (o == null) {
                myAccounts.put(cert, new ArrayList(Arrays.asList(new Object[]{ xtaAccount })));
            } else {
                o.add(xtaAccount);
            }
        }

        public String toString() {
            return "XtaConfigImpl{myAccounts=" + myAccounts + '}';
        }
    }
}
