package cs.impl.directory;

import cs.api.directory.UnknownDestinationException;

public interface LookupEntryProvider {

    LookupEntry getLookupEntry(String Service, String alias) throws UnknownDestinationException;
    LookupEntryProvider copyAs(String service, String alias);
}
