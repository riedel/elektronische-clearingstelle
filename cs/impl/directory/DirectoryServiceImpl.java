/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 18:24:52
 */
package cs.impl.directory;

import cs.api.Message;
import cs.api.directory.AccessDescriptor;
import cs.api.directory.DirectoryEntry;
import cs.api.directory.UnknownDestinationException;
import cs.api.directory.XtaConfig;
import cs.impl.util.Tracing;
import cs.persistence.management.PersistenceSupport;
import org.apache.log4j.Logger;

import java.net.URL;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;

/**
 * Simple implementation of a DirectoryService that is based on an XML configuration file.
 */
public class DirectoryServiceImpl extends PersistenceSupport implements DirectoryServiceImplMBean {
    private static final Logger LOG = Logger.getLogger(DirectoryServiceImpl.class);

    private SimpleDirectory directory = SimpleDirectory.EMPTY_DIRECORY;

    public void start() throws Exception {
        super.start();
        if (getConfigURL() == null) {
            final String property = System.getProperty("cs.impl.directory.SimpleDirectory.config-file");
            if (property != null) {
                setConfigURL(new URL(property));
            }
        }
    }

    public void stop() throws Exception {
        if (directory != null) {
            directory.dispose();
        }
        super.stop();
        directory = null;
    }

    public DirectoryEntry lookup(String type, String alias) throws UnknownDestinationException {
        Tracing.startOperation("Directory Lookup");
        try {
            return directory.lookup(type, alias);
        } finally {
            Tracing.endOperation("Directory Lookup");
        }
    }

    public String translateMessageTypeToService(String messageType) {
        return directory.translateMessageTypeToService(messageType);
    }

    public AccessDescriptor checkAccess(Message message, X509Certificate sender) {
        Tracing.startOperation("Access Check");
        try {
            return directory.checkAccess(sender, message.getType());
        } finally {
            Tracing.endOperation("Access Check");
        }
    }

    public boolean isValidInbox(X509Certificate cert) {
        return directory.isValidInbox(cert);
    }

    public void setConfigURL(URL url) throws Exception {
        try {
            directory = url != null ? new SimpleDirectory(url) : SimpleDirectory.EMPTY_DIRECORY;
        } catch (Exception e) {
            LOG.error("Cannot set config URL (isStarted = " + isStarted() + ")", e);
            if (isStarted()) {
                throw e;
            }
        }
    }

    public URL getConfigURL() {
        return directory.getURL();
    }

    public void reload() throws Exception {
        if (directory.getURL() != null) {
            directory = new SimpleDirectory(directory.getURL());
        }
    }

    public void clearCaches() throws Exception {
        directory.clearCaches();
    }

    public X509Certificate[] getCertPath(X509Certificate cert) {
        return directory.getCertPath(cert);
    }

    public boolean isTrustedRoot(X509Certificate cert) {
        return directory.isTrustedRoot(cert);
    }

    public X509CRL[] getCRLs(X509Certificate tbvCert) {
        return directory.getCRLs(tbvCert);
    }

    public XtaConfig getXtaAccountConfig() {
        return directory.getXtaAccountConfig();
    }
}