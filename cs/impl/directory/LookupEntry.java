/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 30.07.2004
 * Time: 15:04:19
 */
package cs.impl.directory;

import cs.api.directory.DirectoryEntry;
import cs.impl.util.PatternUtil;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.regex.Pattern;

class LookupEntry implements DirectoryEntry, LookupEntryProvider, Cloneable, Serializable {
    private static final Logger LOG = Logger.getLogger(LookupEntry.class);

    private String service;
    private String alias;

    private URL imURL;
    private String clientURI;
    private Boolean myOneWay;

    private X509Certificate imCert;

    private boolean hasCipherCert = false;
    private X509Certificate cipherCert;

    private boolean hasSigCert = false;
    private X509Certificate sigCert;

    private transient KeyStore ks;
    private Date expiringDate;

    LookupEntry(String service, String alias) {
        this.service = service;
        this.alias = alias;
    }

    public URL getIntermediaryURL() {
        return imURL;
    }

    public String getContentReceiver() {
        return clientURI;
    }

    public Boolean oneWay() {
        return myOneWay;
    }

    public X509Certificate getSigCert() {
        return hasSigCert ? sigCert = getCert(sigCert, "SigCert") : null;
    }

    public X509Certificate getCipherCert() {
        return hasCipherCert ? cipherCert = getCert(cipherCert, "CipherCert") : null;
    }

    private X509Certificate getCert(X509Certificate cert, String which) {
        if (cert != null) {
            return cert;
        } else if (ks != null) {
            try {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Loading " + which + " from KeyStore: " + alias);
                }
                return (X509Certificate) ks.getCertificate(alias);
            } catch (KeyStoreException e) {
                LOG.error(which, e);
                return null;
            }
        } else {
            if (LOG.isEnabledFor(Level.WARN)) {
                LOG.warn("No " + which + " for " + alias);
            }
            return null;
        }
    }

    public X509Certificate getIntermediaryCert() {
        return imCert;
    }

    public void setIntermediaryURL(URL url) {
        this.imURL = url;
    }

    public void setContentReceiver(String uri, Boolean isOneWay) {
        this.clientURI = uri;
        this.myOneWay = isOneWay;
    }

    public void setIntermediaryCert(Certificate certificate) {
        imCert = (X509Certificate) certificate;
    }

    public void setCipherCert(Certificate certificate) {
        this.hasCipherCert = true;
        cipherCert = (X509Certificate) certificate;
    }

    public void setSigCert(X509Certificate sigCert) {
        this.hasSigCert = true;
        this.sigCert = sigCert;
    }

    public void setKeystore(KeyStore ks) {
        this.ks = ks;
    }

    /** @noinspection CloneDoesntDeclareCloneNotSupportedException*/
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    public LookupEntry copy(String service, String alias) {
        final LookupEntry o = (LookupEntry) clone();
        o.service = service;
        o.alias = alias;
        return o;
    }

    public String getService() {
        return service;
    }

    public String getAlias() {
        return alias;
    }


    public Date getExpiringDate() {
        return expiringDate;
    }

    public void setExpiringDate(Date expiringDate) {
        this.expiringDate = expiringDate;
    }

    public boolean matches(String service, String alias) {
        boolean match;

        boolean matchService = service.equals(this.service) || "#any".equals(this.service);
        if (matchService) {
            boolean matchAlias;

            if (!"*".equals(this.alias)) {
                String exp = PatternUtil.convertToRegex(this.alias);
                matchAlias = Pattern.compile(exp).matcher(alias).matches();
            } else {
                matchAlias = true;
            }
            match = matchAlias;
        } else {
            match = false;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("[" + this.service + ", " + this.alias + "].matches(" + service + ", " + alias + "): " + match);
        }
        return match;
    }

    public LookupEntry normalize(String service, String alias) {
        return copy(service, alias);
    }

    public String toString() {
        return "LookupEntry[service=" + service + ", AGS=" + alias + ", Intermed=" + imURL + ", ContentReceiver=" + clientURI + "]";
    }

    private Object writeReplace() throws java.io.ObjectStreamException {
        assert !("#any".equals(service) || alias.indexOf("*") > -1) : "Wildcard object not supposed to be serialized";

        final LookupEntry o = (LookupEntry) clone();
        o.sigCert = getSigCert();
        o.cipherCert = getCipherCert();
        return o;
    }

    public LookupEntryProvider copyAs(String service, String alias) {
        return copy(service, alias);
    }

    public LookupEntry getLookupEntry(String service, String key) {
        if (this.matches(service, key)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Found entry: " + this);
            }
            return this.normalize(service, key);
        } else {
            return null;
        }
    }

    public boolean isValid() {
        return expiringDate == null || new Date().before(expiringDate);
    }
}