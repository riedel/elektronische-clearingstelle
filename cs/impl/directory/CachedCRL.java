package cs.impl.directory;

import cs.api.ErrorCode;
import cs.impl.NotificationMail;
import cs.impl.util.Tracing;
import org.apache.log4j.Logger;

import javax.security.auth.x500.X500Principal;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

public class CachedCRL extends X509CRL {
    public static Logger LOG = Logger.getLogger(CachedCRL.class);
    private static final String CRL_PROVIDER = System.getProperty("cs.impl.directory.SimpleDirectory.crl-provider", "BC");

    private final URL url;
    private final boolean receiveImmediately;
    private CRLReloader reloader;

    private int ntrys = 0;
    private volatile X509CRL crl;
    private boolean hasNotificationMailSent;

    public CachedCRL(URL url, boolean receiveImmediately, CRLReloader reloader) {
        this.url = url;
        this.receiveImmediately = receiveImmediately;
        this.reloader = reloader;
        loadCRL();
        reloader.addCRL(this);
    }

    public byte[] getEncoded() throws CRLException {
        X509CRL c = getCRL();
        if (c == null) {
            throw new CRLException("CRL not available");
        }
        return c.getEncoded();
    }

    public X500Principal getIssuerX500Principal() {
        final X509CRL c = getCRL();
        if (c != null) {
            return c.getIssuerX500Principal();
        }
        return new X500Principal("O=Dummy, CN=" + url);
    }

    public Principal getIssuerDN() {
        final X509CRL c = getCRL();
        if (c == null) {
            return new Principal() {
                public String getName() {
                    return "O=Dummy, CN=" + url;
                }
            };
        }
        return c.getIssuerDN();
    }

    public Date getNextUpdate() {
        final X509CRL c = getCRL();
        if (c == null) return null;
        return c.getNextUpdate();
    }

    public X509CRLEntry getRevokedCertificate(BigInteger serialNumber) {
        final X509CRL c = getCRL();
        if (c == null) {
            return null;
        }
        return c.getRevokedCertificate(serialNumber);
    }

    public Set getRevokedCertificates() {
        final X509CRL c = getCRL();
        if (c == null) {
            return Collections.EMPTY_SET;
        }
        return c.getRevokedCertificates();
    }

    public String getSigAlgName() {
        return getCRL().getSigAlgName();
    }

    public String getSigAlgOID() {
        return getCRL().getSigAlgOID();
    }

    public byte[] getSigAlgParams() {
        return getCRL().getSigAlgParams();
    }

    public byte[] getSignature() {
        return getCRL().getSignature();
    }

    public byte[] getTBSCertList() throws CRLException {
        return getCRL().getTBSCertList();
    }

    public Date getThisUpdate() {
        final X509CRL c = getCRL();
        if (c == null) return new Date();
        return c.getThisUpdate();
    }

    public int getVersion() {
        final X509CRL c = getCRL();
        if (c == null) return -1;
        return c.getVersion();
    }

    public void verify(PublicKey key) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final X509CRL c = getCRL();
        if (c == null) {
            LOG.warn("Cannot verify. CRL is not loaded");
            return;
        }
        c.verify(key);
    }

    public void verify(PublicKey key, String sigProvider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final X509CRL c = getCRL();
        if (c == null) {
            LOG.warn("Cannot verify. CRL is not loaded");
            return;
        }
        c.verify(key, sigProvider);
    }

    public boolean isRevoked(Certificate cert) {
        final X509CRL c = getCRL();
        if (c == null) {
            LOG.warn("CRL[" + url + "] could not be loaded. isRevoked(" + ((X509Certificate)cert).getSubjectDN().getName() + ") = false");
            return false;
        }
        return c.isRevoked(cert);
    }

    public String toString() {
        final X509CRL c = getCRL();
        if (c == null) {
            return "CRL[" + url + "]. State: not loaded";
        }
        return c.toString();
    }

    public Set getCriticalExtensionOIDs() {
        return getCRL().getCriticalExtensionOIDs();
    }

    public byte[] getExtensionValue(String oid) {
        return getCRL().getExtensionValue(oid);
    }

    public Set getNonCriticalExtensionOIDs() {
        return getCRL().getNonCriticalExtensionOIDs();
    }

    public boolean hasUnsupportedCriticalExtension() {
        return getCRL().hasUnsupportedCriticalExtension();
    }

    public boolean equals(Object other) {
        if (!(other instanceof CachedCRL)) {
            return false;
        }
        final CachedCRL c = ((CachedCRL)other);
        if (crl != null && c.crl != null) {
            return crl.getIssuerDN().equals(c.crl.getIssuerDN()) &&
                    crl.getVersion() == c.crl.getVersion();
        } else {
            if (url.toExternalForm().equals(c.url.toExternalForm())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return url.toExternalForm().hashCode();
    }

    private void loadCRL() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Load CRL from '" + url + "'");
        }
        load();
    }

    private void reloadCRL() {
        if (LOG.isInfoEnabled()) {
            LOG.info("CRL is outdated, reload CRL from '" + url + "'");
        }
        load();
    }

    private void load() {
        Tracing.startOperation("Loading CRL");
        InputStream inputStream = null;
        try {
            if (crl != null) {
                final int version = crl.getVersion();
                LOG.debug("Version before update [" + url + "]: " + version);
            }

            CertificateFactory cf = CertificateFactory.getInstance("X.509", CRL_PROVIDER);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Pragma", "no-cache");
//            urlConnection.setRequestProperty("User-Agent", "MS-IE-6.0");

            inputStream = new BufferedInputStream(urlConnection.getInputStream());
            final X509CRL c = (X509CRL)cf.generateCRL(inputStream);

            if (c != null) {
                crl = c;

                ntrys = 0;
                hasNotificationMailSent = false;

                LOG.debug("Version after update [" + url + "]: " + crl.getVersion());
                final Date nextUpdate = crl.getNextUpdate();
                if (nextUpdate.before(new Date())) {
                    LOG.warn("Updated CRL [" + url + "] has NextUpdate that is in the past: " + nextUpdate);
                }
            } else if (crl == null) {
                LOG.warn("CRL[" + url + "] was not loaded!");
            } else {
                LOG.warn("CRL[" + url + "] was not updated. Current version is " + crl.getVersion());
            }
        } catch (Exception e) {
            LOG.error("Error while receive CRL from " + url, e);
            ntrys++;
            if (ntrys > 3 && !hasNotificationMailSent) {
                new NotificationMail(ErrorCode.CRL_LOAD_PROBLEM, "Fehler beim Laden einer CRL")
                        .addContent("Eine CRL konnte zum wiederholten Mal nicht von " + url + " geladen werden.\n" +
                                "Der Vorgang wird fortgesetzt, es werden jedoch keine weiteren Benachrichtigungen erzeugt.")
                        .addContent(e)
                        .send();

                hasNotificationMailSent = true;
            }
        } finally {
            Tracing.endOperation("Loading CRL");

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    LOG.error("Cannot close stream from " + url, e);
                }
            }
        }
    }

    private X509CRL getCRL() {
        if (crl != null) {
            if (isOutdated()) {
                if (receiveImmediately) {
                    if (LOG.isInfoEnabled()) {
                        LOG.info("CRL from " + url + " is outdated. Try to reload");
                    }
                    loadCRL();
                } else {
                    if (LOG.isInfoEnabled()) {
                        LOG.info("CRL from " + url + " is outdated. Return last received crl and try to reload.");
                    }
                    synchronized (reloader) {
                        reloader.notify();
                    }
                }
            }
            return crl;
        } else {
            loadCRL();

            if (crl == null) {
                LOG.warn("Couldn't load CRL from " + url);
            }
            return crl;
        }
    }

    public boolean isOutdated() {
        if (crl == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("CRL[" + url + "] has not been loaded yet.");
            }
            return true;
        }

        final Date update = crl.getNextUpdate();
        if (LOG.isDebugEnabled()) {
            LOG.debug("NextUpdate[" + crl.getIssuerDN() + "] = " + update);
        }

        return new Date().after(update);
    }

    public URL getUrl() {
        return url;
    }

    public void update() {
        if (isOutdated()) {
            reloadCRL();
        }
    }

    public boolean isLoaded() {
        return crl != null;
    }

    public void dispose() {
        reloader = null;
        crl = null;
    }
}
