package cs.impl.directory;

import cs.impl.util.PatternUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class TypemapEntry {
    private final String messageType;
    private String service;
    private String keyPrefix;
    private final Set allowedCategories = new HashSet();

    public TypemapEntry(String name) {
        this.messageType = name;
    }

    public String getMessageType() {
        return messageType;
    }

    public void addAllowedCategory(String category) {
        allowedCategories.add(category);
    }

    public Set getAllowedCategories() {
        return allowedCategories;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public boolean match(String messageType) {
        String exp = PatternUtil.convertToRegex(messageType);
        return Pattern.compile(exp).matcher(this.messageType).matches();
    }
}
