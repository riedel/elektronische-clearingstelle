/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 18:21:45
 */
package cs.impl.directory;

import javax.management.PersistentMBean;
import java.net.URL;

public interface DirectoryServiceImplMBean extends cs.api.directory.DirectoryService, PersistentMBean {
    void start() throws Exception;
    void stop() throws Exception;

    void setConfigURL(URL url) throws Exception;
    URL getConfigURL();

    void reload() throws Exception;

    void clearCaches() throws Exception;
}
