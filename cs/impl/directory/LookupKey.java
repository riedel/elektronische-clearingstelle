/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 30.07.2004
 * Time: 15:02:28
 */
package cs.impl.directory;

class LookupKey implements Comparable {
    private final String service;
    private final String alias;

    private LookupKey(String s, String a) {
        this.service = s;
        this.alias = a;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LookupKey)) return false;

        final LookupKey lookupKey = (LookupKey)o;

        if (!alias.equals(lookupKey.alias)) return false;
        if (!service.equals(lookupKey.service)) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = service.hashCode();
        result = 29 * result + alias.hashCode();
        return result;
    }

    public int compareTo(Object o) {
        if (equals(o)) {
            return 0;
        }
        final LookupKey lookupKey = ((LookupKey)o);
        return (service + "/" + alias).compareTo(lookupKey.service + "/" + lookupKey.alias);
    }


    public static LookupKey create(String s, String a) {
        return new LookupKey(s, a);
    }
}