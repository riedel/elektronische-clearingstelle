package cs.impl.directory;

import cs.api.directory.AccessDescriptor;
import cs.impl.util.CertUtil;
import org.apache.log4j.Logger;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.*;

public class PermissionEntry {
    public static Logger LOG = Logger.getLogger(PermissionEntry.class);

    private String service;
    private boolean unrestricted = false;
    private final Set disallowedCerificates = new HashSet();
    private final Map disallowedIssuer = new HashMap();
    private final Set allowedCerificates = new HashSet();
    private final Map allowedIssuer = new HashMap();
    private final Map issuerCache = new HashMap();
    private DVDVServer dvdvServer;
    private Set dvdvCategories = new HashSet();

    public PermissionEntry(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public boolean isUnrestricted() {
        return unrestricted;
    }

    public void setUnrestricted(boolean unrestricted) {
        this.unrestricted = unrestricted;
    }

    public DVDVServer getDVDVServer() {
        return dvdvServer;
    }

    public void setDVDVServer(DVDVServer dvdvServer) {
        this.dvdvServer = dvdvServer;
    }

    public void addCategory(String category) {
        dvdvCategories.add(category);
    }

    public Set getAllowedCerificates() {
        return allowedCerificates;
    }

    public Map getAllowedIssuer() {
        return allowedIssuer;
    }

    public Set getDisallowedCerificates() {
        return disallowedCerificates;
    }

    public Map getDisallowedIssuer() {
        return disallowedIssuer;
    }

    public AccessDescriptor checkAccess(X509Certificate certificate) {
        if (unrestricted) {
            return AccessDescriptor.UNRESTRICTED;
        } else if (certificate != null) {
            final String key = certificate.getIssuerDN().getName();

            if (disallowedCerificates.contains(certificate)) {
                return AccessDescriptor.NO_ACCESS;
            } else if (issuerCache.containsKey(certificate)) {
                return (AccessDescriptor)issuerCache.get(certificate);
            } else if (disallowedIssuer.containsKey(key)) {
                if (CertUtil.isIssuer((PublicKey)disallowedIssuer.get(key), certificate)) {
                    final AccessDescriptor result = AccessDescriptor.NO_ACCESS;
                    issuerCache.put(certificate, result);
                    return result;
                }
            } else if (allowedCerificates.contains(certificate)) {
                return AccessDescriptor.EXPLICITELY_KNOWN;
            } else if (allowedIssuer.containsKey(key)) {
                if (CertUtil.isIssuer((PublicKey)allowedIssuer.get(key), certificate)) {
                    final AccessDescriptor result = AccessDescriptor.CERT_ISSUER_KNOWN;
                    issuerCache.put(certificate, result);
                    return result;
                }
            } else if (dvdvServer != null) {
                if (dvdvServer.checkAccess(certificate, dvdvCategories)) {
                    return AccessDescriptor.EXPLICITELY_KNOWN;
                } else {
                    return AccessDescriptor.NO_ACCESS;
                }
            }
        } else {
            return AccessDescriptor.NO_ACCESS;
        }
        return AccessDescriptor.UNDEFINED;
    }

    public String toString() {
        final StringBuffer sb = new StringBuffer();
        if (unrestricted) {
            sb.append("unrestricted");
        } else {
            if (disallowedCerificates.size() > 0) {
                sb.append("disallowed: ").append(CertUtil.toString(disallowedCerificates));
            }
            if (allowedCerificates.size() > 0) {
                if (sb.length() > 0) sb.append(", ");
                sb.append("allowed: ").append(CertUtil.toString(allowedCerificates));
            }
            if (allowedIssuer.size() > 0) {
                if (sb.length() > 0) sb.append(", ");
                sb.append("allowed issuer: ").append(allowedIssuer.keySet());
            }
            if (dvdvServer != null) {
                if (sb.length() > 0) sb.append(", ");
                sb.append("by DVDV server: ").append(dvdvServer.getName()).append(" for ").append(dvdvCategories);
            }
        }
        return sb.toString();
    }
}
