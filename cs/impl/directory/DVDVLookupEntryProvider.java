package cs.impl.directory;

import cs.api.directory.NegativeDVDVLookup;
import cs.impl.util.Enumeration;
import cs.impl.util.Tracing;
import de.dvdv.DVDVException;
import de.dvdv.object.message.FindServiceDescriptionResponse;
import de.dvdv.object.osci.OsciAddresseeElement;
import de.dvdv.object.osci.OsciIntermediaryElement;
import org.apache.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;

public class DVDVLookupEntryProvider implements LookupEntryProvider, Cloneable {
    private static final Logger LOG = Logger.getLogger(DVDVLookupEntryProvider.class);

    private static final class EntryType extends Enumeration {
        private static final boolean PREFER_INTERNET_TYPE = Boolean.getBoolean("ecs.dvdv.prefer-internet-type");

        static final EntryType Internet = new EntryType("Internet");
        static final EntryType TESTA = new EntryType("TESTA");

        EntryType(String value) {
            super(value);
        }

        static EntryType primary() {
            return PREFER_INTERNET_TYPE ? Internet : TESTA;
        }
        static EntryType secondary() {
            return PREFER_INTERNET_TYPE ? TESTA : Internet;
        }

        public String intermedElement() {
            return getValue() + "Intermediär";
        }

        public String addresseeElement() {
            return getValue() + "Empfänger";
        }
    }


    private String service;
    private String alias;
    private KeyStore keyStore;
    private String keyPrefix = "";
    private DVDVServer dvdvServer;

    // LookupEntry used for check if service/ags matchs
    private LookupEntry lookupEntry;

    public DVDVLookupEntryProvider(String service, String alias, DVDVServer server) {
        init(alias, service);
        this.dvdvServer = server;
    }

    private void init(String alias, String service) {
        this.alias = alias;
        this.service = service;
        lookupEntry = new LookupEntry(this.service, this.alias);
    }

    public String getAlias() {
        return alias;
    }

    public String getService() {
        return service;
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public LookupEntryProvider copyAs(String service, String alias) {
        final DVDVLookupEntryProvider o = (DVDVLookupEntryProvider)clone();
        o.init(service, alias);
        return o;
    }

    public LookupEntry getLookupEntry(String service, String key) throws NegativeDVDVLookup {
        // toDo: dvdv-request, don't forget to test validUntil + validFrom (currently not possible - the SDk doesn't provide this information)
        // Reuse match-method of LookupEntry

        if (lookupEntry.matches(service, key)) {
            LOG.info("Doing DVDV-lookup for key: " + key + ", service: " + service);

            final LookupEntry le = lookupEntry.normalize(service, key);

            Tracing.startOperation("DVDV-Lookup");
            try {
                final String lookup = key.startsWith(keyPrefix) ? key : keyPrefix + key;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Lookup Key = " + lookup);
                }

                final FindServiceDescriptionResponse response = dvdvServer.lookup(service, lookup);

                if (response != null) {
                    final EntryType primaryType = EntryType.primary();
                    final EntryType secondaryType = EntryType.secondary();

                    if (trySetup(le, response, primaryType)) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Use " + primaryType + " intermediary & addressee for [" + service + "/" + key + "]");
                        }
                    } else if (trySetup(le, response, secondaryType)) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Use " + secondaryType + " intermediary & addressee [" + service + "/" + key + "]");
                        }
                    } else {
                        LOG.warn("DVDV-response does not contain any usable information for " + service + "/" + key + " @" + dvdvServer.getName());
                    }
                }
                return le;
            } catch (NegativeDVDVLookup e) {
                throw e;
            } catch (Exception e) {
                LOG.error("Exception during DVDV-lookup", e);
            } finally {
                Tracing.endOperation("DVDV-Lookup");
            }
        }

        // dvdv-entry is not responsible for this parameter
        return null;
    }

    private boolean trySetup(LookupEntry le, FindServiceDescriptionResponse response, EntryType type) throws DVDVException {
        final OsciIntermediaryElement intermed = response.getOsciIntermediaryElement(type.intermedElement());
        final OsciAddresseeElement addressee = response.getOsciAddresseeElement(type.addresseeElement());
        if (intermed == null || addressee == null) {
            LOG.debug("No Intermed and/or Addresse element of type " + type);
            return false;
        }

        final X509Certificate intermedCipherCertificate;
        try {
            intermedCipherCertificate = intermed.getCipherCertificate();
            if (intermedCipherCertificate == null) {
                LOG.error("Missing intermed cipher certificate in " + type);
                return false;
            }
        } catch (CertificateException e) {
            LOG.error("Invalid intermed cipher certificate in " + type, e);
            return false;
        }

        final X509Certificate addresseeCipherCertificate;
        try {
            addresseeCipherCertificate = addressee.getCipherCertificate();
            if (addresseeCipherCertificate == null) {
                LOG.error("Missing addressee cipher certificate in " + type);
                return false;
            }
        } catch (CertificateException e) {
            LOG.error("Invalid intermed cipher certificate in " + type, e);
            return false;
        }

        final URI intermedUri = intermed.getUri();
        try {
            if (intermedUri == null) {
                LOG.error("Missing intermed URI in " + type);
                return false;
            }

            setupLookupEntry(le, intermedCipherCertificate, intermedUri.toURL(), addresseeCipherCertificate, addressee.getSignatureCertificate(), addressee.getUri());
            return true;
        } catch (CertificateException e) {
            LOG.error("Invalid addressee signature certificate in " + type, e);
        } catch (MalformedURLException e) {
            LOG.error("Invalid URL: " + intermedUri, e);
        }

        return false;
    }

    public String toString() {
        return "DVDV Lookup: " + lookupEntry.toString() + "@" + dvdvServer.getName();
    }

    private void setupLookupEntry(LookupEntry le, X509Certificate intermedCipherCertificate, URL intermedUrl, X509Certificate addresseeCipherCertificate, X509Certificate addresseeSignatureCertificate, URI uri) {
        le.setIntermediaryCert(intermedCipherCertificate);
        le.setIntermediaryURL(intermedUrl);
        le.setCipherCert(addresseeCipherCertificate);
        le.setSigCert(addresseeSignatureCertificate);

        if (uri != null && uri.toString().length() > 0) {
            le.setContentReceiver(uri.toString(), null);
        }

        final Calendar cal = Calendar.getInstance();
        final int day = cal.get(Calendar.DAY_OF_YEAR);

        // up to 12 hours, limited to 23:59h on the same day
        cal.add(Calendar.HOUR_OF_DAY, 12);
        if (day != cal.get(Calendar.DAY_OF_YEAR)) {
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
        }
        le.setExpiringDate(cal.getTime());
    }

    /** @noinspection CloneDoesntDeclareCloneNotSupportedException */
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }
}
