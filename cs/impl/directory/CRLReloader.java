package cs.impl.directory;

import org.apache.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CRLReloader extends Thread {
    private static final Logger LOG = Logger.getLogger(CRLReloader.class);

    private static final int TIMEOUT = Integer.getInteger("cs.impl.directory.SimpleDirectory.crl-reload-interval", 60 * 60 * 1000).intValue();

    private final List crls = new ArrayList();

    public CRLReloader() {
        super(CRLReloader.class.getName());
        setDaemon(true);
    }

    public synchronized void addCRL(CachedCRL crl) {
        crls.add(new WeakReference(crl));
    }

    public void interrupt() {
        synchronized (this) {
            crls.clear();
        }
        super.interrupt();
    }

    public void run() {
        try {
            _run();
        } finally {
            LOG.info("CRLReloader stopped.");
            setContextClassLoader(null);

            synchronized (this) {
                final Iterator iterator = crls.iterator();
                while (iterator.hasNext()) {
                    WeakReference ref = (WeakReference)iterator.next();
                    CachedCRL crl = (CachedCRL)ref.get();

                    if (crl != null) {
                        crl.dispose();
                    }
                }
                crls.clear();
            }
        }
    }

    private void _run() {
        while (!isInterrupted()) {
            try {
                synchronized (this) {
                    wait(TIMEOUT);

                    Iterator iterator = crls.iterator();

                    while (iterator.hasNext()) {
                        WeakReference ref = (WeakReference)iterator.next();
                        CachedCRL crl = (CachedCRL)ref.get();

                        if (crl != null) {
                            crl.update();
                        } else {
                            iterator.remove();
                        }
                    }
                }
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
