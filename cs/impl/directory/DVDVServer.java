package cs.impl.directory;

import cs.api.directory.NegativeDVDVLookup;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.config.ContextMgmtData;
import de.dvdv.DVDVException;
import de.dvdv.manager.BatchResult;
import de.dvdv.manager.DVDVConfigurationException;
import de.dvdv.manager.DVDVManager;
import de.dvdv.manager.DVDVNoResponseException;
import de.dvdv.object.message.*;
import de.dvdv.osci.osci12.Osci12DVDVManager;
import de.osci.osci12.common.OSCICancelledException;
import de.osci.osci12.encryption.Crypto;
import de.osci.osci12.encryption.OSCICipherException;
import de.osci.osci12.extinterfaces.crypto.Decrypter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class DVDVServer {
    private static final Logger LOG = Logger.getLogger(DVDVServer.class);

    // set of DVDV SDK error codes that indicate an unsuccesful lookup instead of a technical failure
    private static final Set UNSUCCESSFUL_RESULTS = new HashSet();
    static {
        UNSUCCESSFUL_RESULTS.add(ErrorResponse.Code.LDAP_NO_PROVIDER_HITS.toString());
        UNSUCCESSFUL_RESULTS.add(ErrorResponse.Code.LDAP_NO_SERVICE_DESC_HITS.toString());
        UNSUCCESSFUL_RESULTS.add(ErrorResponse.Code.LDAP_NO_SERVICE_HITS.toString());
        UNSUCCESSFUL_RESULTS.add(ErrorResponse.Code.LDAP_NO_SERVICEELEMNT_HITS.toString());
        UNSUCCESSFUL_RESULTS.add(ErrorResponse.Code.LDAP_MISSING_SERVICE_ELEMENT.toString());
    }

    private final String name;
    private final List wsdlUrls = new ArrayList();

    private static final ThreadLocal perThreadManager = new ThreadLocal() {
        protected Object initialValue() {
            return new Osci12DVDVManager(new MyDecryptor());
        }
    };

    public DVDVServer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addWsdlUrl(String wsdlUrl) {
        wsdlUrls.add(wsdlUrl);
    }

    public String toString() {
        return "DVDVServer[" + name + ", URIs = " + wsdlUrls + "]";
    }

    public FindServiceDescriptionResponse lookup(String service, String key) throws DVDVException, URISyntaxException, IOException, NegativeDVDVLookup {
        DVDVManager dvdvManager = null;
        try {
            dvdvManager = getDVDVManager();
            final FindServiceDescriptionRequest findServiceDescriptionRequest =
                    dvdvManager.createFindServiceDescriptionRequest(key, new URI(service));

            final DVDVResponse dvdvResponse = dvdvManager.processRequest(findServiceDescriptionRequest);

            if (dvdvResponse instanceof FindServiceDescriptionResponse) {
                return (FindServiceDescriptionResponse)dvdvResponse;
            } else {
                if (dvdvResponse instanceof ErrorResponse) {
                    final ErrorResponse response = (ErrorResponse)dvdvResponse;
                    LOG.error("Got ErrorResponse from DVDV: " + response.getError() + ": " + response.getDescription());
                    
                    if (UNSUCCESSFUL_RESULTS.contains(response.getError())) {
                        throw new NegativeDVDVLookup(service, key, response.getError(), response.getDescription());
                    }
                } else {
                    LOG.error("Unable to lookup for service: '" + service + "' key: '" + key + "'");
                }
                return null;
            }
        } catch (DVDVNoResponseException e) {
            throw (IOException)new IOException(e.getMessage()).initCause(e);
        } catch (DVDVConfigurationException e) {
            LOG.error("Unable to load DVDV parameters from " + wsdlUrls, e);
            throw e;
        } catch (IOException e) {
            if (dvdvManager == null || dvdvManager.getDVDVServerParameters().size() == 0) {
                LOG.error("Unable to load DVDV parameters from " + wsdlUrls, e);
            }
            throw e;
        } catch (GeneralSecurityException e) {
            LOG.error("Failed to initialize DVDV Manager", e);
            return null;
        }
    }

    public boolean checkAccess(X509Certificate certificate, Set categories) {
        try {
            final DVDVManager dvdvManager = getDVDVManager();
            final Iterator iterator = categories.iterator();
            final List list = new ArrayList();

            while (iterator.hasNext()) {
                final String category = (String)iterator.next();
                VerifyCategoryRequest request = dvdvManager.createVerifyCategoryRequest(certificate, category);
                list.add(request);
            }
            final BatchResult batchResult = dvdvManager.processRequests(list);

            for (int i = 0; i < batchResult.size(); i++) {
                DVDVResponse dvdvResponse = batchResult.get(i);

                if (dvdvResponse instanceof VerifyCategoryResponse) {
                    final VerifyCategoryResponse response = ((VerifyCategoryResponse)dvdvResponse);
                    if (response.getResult()) {
                        return true;
                    } else {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("DVDV did not authorize <" + certificate.getSubjectDN().getName() + "> for category " + response.getCategory());
                        }
                    }
                } else if (dvdvResponse instanceof ErrorResponse) {
                    LOG.error("Unable to verify <" + certificate.getSubjectDN().getName() + ">: " + ((ErrorResponse)dvdvResponse).getDescription());
                } else {
                    LOG.error("Unable to verify <" + certificate.getSubjectDN().getName() + ">");
                    LOG.error("DVDV response was: " + dvdvResponse);
                }
            }
        } catch (Exception e) {
            LOG.error("Couldn't verify access through DVDV request for <" + certificate.getSubjectDN().getName() + ">", e);
        }
        return false;
    }

    private Osci12DVDVManager getDVDVManager() throws NoSuchAlgorithmException, IOException, CertificateException, UnrecoverableKeyException, KeyStoreException, DVDVConfigurationException {
        final Osci12DVDVManager manager = (Osci12DVDVManager)perThreadManager.get();
        final List parameters = manager.getDVDVServerParameters();
        if (parameters == null || parameters.size() == 0) {
            manager.loadDvdvServerParameters((String[])wsdlUrls.toArray(new String[wsdlUrls.size()]));
        }
        return manager;
    }

    static class MyDecryptor extends Decrypter {
        private PrivateKey key;
        private X509Certificate certificate;

        private void init() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, NoSuchProviderException {
            final Configuration instance = ConfigurationImpl.getInstance();
            final ContextMgmtData clientMgmtData = instance.getClientMgmtData();
            final String keystoreType = clientMgmtData.getOsciKeystoreType();
            final URL keystoreLocation = clientMgmtData.getOsciKeystore();
            final String keyAlias = clientMgmtData.getOsciKeyAlias();
            final String keyPassword = clientMgmtData.getOsciKeyPassword();
            final String keystorePassword = clientMgmtData.getOsciKeystorePassword();

            if (keystoreLocation != null) {
                final KeyStore keystore;
                if ("PKCS12".equals(keystoreType)) {
                    // The Sun PKCS12 keystore seems to have some problems...
                    keystore = KeyStore.getInstance(keystoreType, "BC");
                } else {
                    keystore = KeyStore.getInstance(keystoreType);
                }

                keystore.load(keystoreLocation.openStream(), keystorePassword.toCharArray());
                key = (PrivateKey)keystore.getKey(keyAlias, keyPassword.toCharArray());
                certificate = (X509Certificate)keystore.getCertificate(keyAlias);
            }
        }

        public String getVersion() {
            return "1.0";
        }

        public String getVendor() {
            return "cit GmbH";
        }

        public X509Certificate getCertificate() {
            try {
                init();
                return certificate;
            } catch (Exception e) {
                LOG.error("Keystore initialization failed", e);
                return null;
            }
        }

        @Override
        public byte[] decrypt(byte[] data, String mgfAlgorithm, String digestAlgorithm) throws OSCICipherException {
            try {
                init();

                return Crypto.doRSADecryption(this.key, data, "http://www.w3.org/2009/xmlenc11#rsa-oaep", mgfAlgorithm, digestAlgorithm, null);
            } catch (Exception var5) {// 117
                throw (OSCICipherException)new OSCICipherException("decryption_error").initCause(var5);
            }
        }

        public byte[] decrypt(byte[] bytes) throws OSCICipherException {
            try {
                init();

                return Crypto.doRSADecryption(key, bytes);
            } catch (Exception ex) {
                throw (OSCICipherException)new de.osci.osci12.encryption.OSCICipherException("Unable to decrypt").initCause(ex);
            }
        }
    }
}
