package cs.impl;

import cs.impl.config.ConfigurationImpl;
import cs.api.ErrorCode;
import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.NameNotFoundException;
import javax.xml.soap.SOAPMessage;
import java.io.*;
import java.util.Date;

public class NotificationMail {
    private static final Logger LOG = Logger.getLogger(NotificationMail.class);

    private static final String JNDI_MAIL_SESSION_NAME = "eCS/Mail";
    private static final int MAX_INLINE_STACK_LENGTH = 10;

    private MimeMultipart mmp = new MimeMultipart();
    private MimeMessage mail;


    public NotificationMail(ErrorCode code, String subject) {
        this("[" + code + "] " + subject);
    }

    public NotificationMail(String subject) {
        try {
            final String recipient = getEmail();
            if (recipient != null && recipient.length() > 0) {
                final Session session = getMailSession();
                if (session != null) {
                    mail = new MimeMessage(session);
                    mail.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
                    mail.setSubject(subject);
                    mail.setContent(mmp);
                } else {
                    LOG.warn("Couldn't find mail session '" + JNDI_MAIL_SESSION_NAME + "'. " +
                            "Please configure the application server properly to send notification e-mails.");
                }
            } else {
                LOG.info("Es ist keine Administrations-Adresse konfiguriert. Es werden keine E-Mail Benachrichtigungen gesendet.");
            }
        } catch (Exception e) {
            LOG.error("Error creating notification mail", e);
        }
    }

    protected String getEmail() {
        final String email = System.getProperty("ecs.admin-email");
        if (email != null) {
            return email;
        }
        return ConfigurationImpl.getInstance().getAdministrationEMail();
    }

    private static Session getMailSession() throws NamingException {
        final String host = System.getProperty("mail.host");
        if (host != null) {
            return Session.getInstance(System.getProperties());
        }
        try {
            final InitialContext context = new InitialContext();
            try {
                return (Session)context.lookup(JNDI_MAIL_SESSION_NAME);
            } finally {
                context.close();
            }
        } catch (NameNotFoundException e) {
            LOG.debug("Mail Session not found in JNDI", e);
            return null;
        }
    }

    public NotificationMail addContent(String msg) {
        try {
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setText(msg + "\n");
            mmp.addBodyPart(mbp);
        } catch (MessagingException e) {
            LOG.error("Error adding content", e);
        }
        return this;
    }

    public NotificationMail addContent(Throwable t) {
        if (t == null) {
            return this;
        }
        try {
            final StringWriter sw = new StringWriter();
            t.printStackTrace(new PrintWriter(sw));

            final String stacktrace = sw.toString();
            final String[] strings = stacktrace.split("\r\n|[\r\n]", MAX_INLINE_STACK_LENGTH + 1);
            final StringBuffer sb = new StringBuffer("\n\n");

            for (int i = 0; i < Math.min(strings.length, MAX_INLINE_STACK_LENGTH); i++) {
                sb.append(strings[i]).append("\r\n");
            }
            if (strings.length > MAX_INLINE_STACK_LENGTH) {
                sb.append("\t[...]\r\n\r\n");
            }

            final MimeBodyPart mbp = new MimeBodyPart();
            mbp.setText(sb.toString().replaceAll("\t", "    "));
            mmp.addBodyPart(mbp);

            addAttachment("stacktrace.txt", "text/plain", sw.toString());
        } catch (MessagingException e) {
            LOG.error("Error adding content", e);
        }
        return this;
    }

    public NotificationMail addAttachment(String fileName, byte[] data) {
        try {
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setDataHandler(new DataHandler(new ByteArrayDataSource("application/octet-stream", data)));
            mbp.setFileName(fileName);
            mmp.addBodyPart(mbp);
        } catch (MessagingException e) {
            LOG.error("Error adding attachment", e);
        }
        return this;
    }

    public NotificationMail addAttachment(String fileName, String contentType, String messageText) {
        try {
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setText(messageText);
            mbp.setFileName(fileName);
            mmp.addBodyPart(mbp);
        } catch (MessagingException e) {
            LOG.error("Error adding attachment", e);
        }
        return this;
    }

    public NotificationMail addAttachment(SOAPMessage message) {
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            message.writeTo(out);

            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setDataHandler(new DataHandler(new ByteArrayDataSource("application/octet-stream", out.toByteArray())));
            mbp.setFileName("Nachricht.osci");
            mmp.addBodyPart(mbp);
        } catch (Exception e) {
            LOG.error("Error adding attachment", e);
        }
        return this;
    }

    public void send() {
        try {
            if (mail != null) {
                mail.setSentDate(new Date());
                Transport.send(mail);
            }
        } catch (Exception e) {
            LOG.error("Error sending notification mail", e);
        }
    }

    class ByteArrayDataSource implements DataSource {
        private byte[] data;
        private String mimetype;

        public ByteArrayDataSource(String mimetype, byte[] data) {
            this.data = data;
            this.mimetype = mimetype;
        }

        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(data);
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("Outputstream is not supported");
        }

        public String getContentType() {
            return mimetype;
        }

        public String getName() {
            return getClass().getName();
        }
    }

    public static void send(String subject, String msg) {
        new NotificationMail(subject).addContent(msg).send();
    }
    
    public static void send(String subject, String msg, Throwable t) {
        new NotificationMail(subject).addContent(msg).addContent(t).send();
    }

    public static void main(String[] args) {
        new NotificationMail("Test").addContent(new Throwable().initCause(new Throwable().initCause(new Throwable()))).send();
    }
}
