/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 25.08.2004
 * Time: 10:53:32
 */
package cs.impl;

import cs.api.*;
import cs.api.directory.UnknownDestinationException;
import cs.api.transport.Result;
import cs.api.validation.ValidationException;

import javax.management.NotificationBroadcaster;
import java.util.Map;
import java.util.Set;

public interface ClearingstelleImplMBean extends Clearingstelle, NotificationBroadcaster {

    void start() throws Exception;
    void stop() throws Exception;

    void shutdown();

    Set<String> listConnectors();

    Map<String, Set<String>> getConnectorInfo(String id);

    Result handleSendMessage(String sender, Message m, int mode, String jobID)
            throws CsException, ConnectorException, UnknownTypeException, InvalidMessageException, UnknownDestinationException, ValidationException;

    void notifyException(Throwable e);

    void returnToSender(Message m, String senderId, String errorCode, String jobID) throws ConnectorException;

    boolean handleInvalidMessage(Message message, ErrorCode errorCode, Throwable exception) throws ConnectorException;

    byte[] sendMessage(String sender, byte[] m, int mode, String jobID, String originator)
            throws CsException, ConnectorException, UnknownTypeException, InvalidMessageException, UnknownDestinationException, ValidationException;
}