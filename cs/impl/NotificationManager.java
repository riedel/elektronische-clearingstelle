/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 17.09.2004
 * Time: 10:13:06
 */
package cs.impl;

import cs.api.Message;
import org.apache.log4j.Logger;

import javax.management.MBeanNotificationInfo;
import javax.management.Notification;

class NotificationManager {
    private static final Logger LOG = Logger.getLogger(NotificationManager.class);
    private static final Logger TRACE = Logger.getLogger("trace." + LOG.getName());

    private final ClearingstelleImpl clearingstelle;

    NotificationManager(ClearingstelleImpl clearingstelle) {
        this.clearingstelle = clearingstelle;
    }

    public MBeanNotificationInfo[] getNotificationInfo() {
        final String name = MyNotification.class.getName();
        final MBeanNotificationInfo[] infos = new MBeanNotificationInfo[]{
            new MBeanNotificationInfo(new String[]{"cs.sendMessage"}, name, "Notification about incoming messages"),
            new MBeanNotificationInfo(new String[]{"cs.registerConnector", "cs.unregisterConnector"}, name, "Notification about Connector un-/registration"),
            new MBeanNotificationInfo(new String[]{"cs.shutdown"}, name, "Shutdown notification"),
            new MBeanNotificationInfo(new String[]{"cs.errorNotification.sendMessage"}, name, "Notification about errors while processing a message"),
            new MBeanNotificationInfo(new String[]{"cs.errorNotification.external"}, name, "Notification about external errors"),
        };
        return infos;
    }

    private void sendNotification(MyNotification myNotification) {
        // Possibility to disable certain notifications by setting the system-property
        // "cs.impl.notifications.log4j" to true. If this is set, a notification is only sent
        // if the Logger-instance for "#" + notificationType is INFO-enabled. Setting such a logger
        // to higher than INFO (WARN, ERROR) disables the according notifications.

        if (Boolean.getBoolean("cs.impl.notifications.log4j") && !Logger.getLogger("#" + myNotification.getType()).isInfoEnabled()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Suppressed notification because log4j category is not INFO-enabled: #" + myNotification.getType());
            }
        } else {
            if (TRACE.isDebugEnabled()) {
                TRACE.debug("Sending notification: [" +
                        "Type = " + myNotification.getType() +
                        ", Message = " + myNotification.getMessage() +
                        ", UserData = " + myNotification.getUserData() +
                        "]");
            }
            clearingstelle.sendNotification(myNotification);
        }
    }

    public void notifyConnectorRegistered(String connectorId) {
        sendNotification(new MyNotification("cs.registerConnector", clearingstelle, connectorId));
    }

    public void notifyConnectorUnregistered(String connectorId) {
        sendNotification(new MyNotification("cs.unregisterConnector", clearingstelle, connectorId));
    }

    public void notifySendMsgError(Message mi, String message) {
        sendNotification(new MyNotification("cs.errorNotification.sendMessage", clearingstelle, mi, message));
    }

    public void notifySendMsg(Message mi, String connectorId) {
        sendNotification(new MyNotification("cs.sendMessage", clearingstelle, mi, connectorId));
    }

    public void notifyShutdown() {
        sendNotification(new MyNotification("cs.shutdown", clearingstelle));
    }

    public void notifyException(Throwable e) {
        sendNotification(new MyNotification("cs.errorNotification.external", clearingstelle, e));
    }

    private static class MyNotification extends Notification {
        private static long notification;

        MyNotification(String type, Object source, Object userdata) {
            super(type, source, notification++);
            setUserData(userdata);
        }

        MyNotification(String type, Object source) {
            super(type, source, notification++);
        }

        MyNotification(String type, Object source, String message) {
            super(type, source, notification++, message);
        }

        MyNotification(String type, Object source, Object userdata, String message) {
            super(type, source, notification++, message);
            setUserData(userdata);
        }
    }

}