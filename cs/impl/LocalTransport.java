/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 28.01.2005
 * Time: 11:24:33
 */
package cs.impl;

import cs.api.Connector;
import cs.api.ConnectorException;
import cs.api.Message;
import cs.api.transport.Result;
import cs.api.transport.ResultImpl;
import cs.api.transport.Transport;

final class LocalTransport implements Transport {
    private final Connector connector;
    private final String name;

    public LocalTransport(final Connector connector, String name) {
        this.connector = connector;
        this.name = name;
    }

    public boolean isLocal() {
        return true;
    }

    public String getName() {
        return name;
    }

    public Result send(String sender, String jobID, Message m) throws ConnectorException {
        final byte[] bytes = connector.processMessage(m);
        return new ResultImpl(bytes);
    }
}