package cs.impl;

import cs.api.ErrorCode;
import cs.api.Message;
import cs.api.logging.LogMessage;
import cs.api.logging.MessageLogger;
import cs.impl.config.ConfigException;
import cs.impl.config.ConfigurationImpl;
import org.apache.log4j.Logger;

import java.util.Date;

/*
* Created by IntelliJ IDEA.
* User: sweinreuter
* Date: 10.09.2007
*/
public class Statistics {
    private static final Logger LOG = Logger.getLogger(Statistics.class);

    private static Statistics INSTANCE;

    private final MessageLogger myLoggerImpl;

    private Statistics() {
        MessageLogger logger = null;

        try {
            logger = ConfigurationImpl.getInstance().getLoggerImpl();
        } catch (ConfigException e) {
            LOG.error("Could not get Logger", e);
        }
        myLoggerImpl = logger;
    }

    private static synchronized Statistics getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Statistics();
        }
        return INSTANCE;
    }

    public static void message(LogMessage.Direction direction, String errorCode, String messageId, Message m) {
        getInstance().createMessage(new Date(), direction, errorCode, messageId, m);
    }

    public static void message(LogMessage.Direction direction, ErrorCode errorCode, String messageId, Message m) {
        getInstance().createMessage(new Date(), direction, (errorCode != null ? errorCode.toString() : (String)null), messageId, m);
    }

    public static void message(ErrorCode code, String messageId) {
        message(LogMessage.Direction.INCOMING, code, messageId, null);
    }

    private void createMessage(Date date, LogMessage.Direction direction, String errorCode, String messageId, Message m) {
        if (myLoggerImpl == null) {
            return;
        }

        try {
            String type = null;
            String sender = null;
            String receiver = null;
            String typeSpecificId = null;

            if (m != null) {
                type = m.getType();
                sender = m.getSender();
                receiver = m.getReceiver();
                typeSpecificId = m.getMessageId();
            }

            myLoggerImpl.logStatistics(date, direction, errorCode, messageId, type, sender, receiver, typeSpecificId);
        } catch (Throwable e) {
            LOG.error("Unexpected error while adding statistics for " + messageId, e);
        }
    }
}