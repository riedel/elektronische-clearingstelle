/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 03.09.2004
 * Time: 14:23:54
 */
package cs.impl.osci;

import cs.impl.util.Tracing;
import cs.persistence.osci.ejb.OsciPersistenceManager;
import cs.persistence.osci.ejb.OsciPersistenceManagerHome;
import cs.persistence.osci.ejb.StoreDeliveryVO;
import de.esslingen.mediakomm.osci.impl.transport.OSCIUtils;
import de.esslingen.mediakomm.osci.impl.transport.service.AbstractPersistenceProvider;
import de.esslingen.mediakomm.osci.impl.transport.utils.LoggingOutputStream;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.OSCISelectionRule;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.intermediary.OSCIIntermediaryDeliveryFactory;
import de.esslingen.mediakomm.osci.transport.intermediary.OSCIProcessCard;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIStoreDelivery;
import de.esslingen.mediakomm.osci.transport.service.MessageInputAdapter;
import de.esslingen.mediakomm.osci.transport.service.MessageOutputAdapter;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.security.cert.X509Certificate;
import java.util.*;

public class PersistenceProvider extends AbstractPersistenceProvider {
    private static final Logger LOG = Logger.getLogger(PersistenceProvider.class);

    private final OsciPersistenceManager persistenceManager;

    public PersistenceProvider(OSCIContext ctx) throws OSCIContextConfigurationException {
        super(ctx);

        try {
            final InitialContext context = new InitialContext();
            final OsciPersistenceManagerHome home = (OsciPersistenceManagerHome)PortableRemoteObject.narrow(context.lookup("ejb/PersistenceManagerEJB"), OsciPersistenceManagerHome.class);
            persistenceManager = home.create();
            context.close();
        } catch (Exception e) {
            throw new OSCIContextConfigurationException(e);
        }
    }

    public void storeStoreDelivery(OSCIStoreDelivery delivery, X509Certificate addressee, Date reception) {
        Tracing.startOperation("Store OSCI Message to DB");
        try {
            final ByteArrayOutputStream bos = new ByteArrayOutputStream();
            final SOAPMessage soapMessage = delivery.getDocument().marshal();

            context.getTransportProvider().messageToStream(soapMessage, new MessageOutputAdapter() {
                public void setHeader(String headerName, String value) {
                }

                public OutputStream getOutputStream() {
                    return bos;
                }
            });
            persistenceManager.storeStoreDelivery(delivery.getMessageId(), bos.toByteArray(), addressee, reception);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Store OSCI Message to DB");
        }
    }

    protected OSCIStoreDelivery getOldestStoreDelivery(X509Certificate addresse) {
        Tracing.startOperation("Retrieve OSCI Message from DB");
        try {
            final StoreDeliveryVO delivery = persistenceManager.getOldestStoreDelivery(addresse);
            return delivery != null ? createDelivery(delivery) : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve OSCI Message from DB");
        }
    }

    protected OSCIStoreDelivery getStoreDeliveryByMessageId(X509Certificate addresse, String messageId) {
        Tracing.startOperation("Retrieve OSCI Message from DB");
        try {
            final StoreDeliveryVO delivery = persistenceManager.getStoreDeliveryByMessageId(addresse, messageId);
            return delivery != null ? createDelivery(delivery) : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve OSCI Message from DB");
        }
    }

    protected OSCIStoreDelivery getStoreDeliveryByReception(X509Certificate addresse, Date receptionOfDelivery) {
        Tracing.startOperation("Retrieve OSCI Message from DB");
        try {
            final StoreDeliveryVO delivery = persistenceManager.getStoreDeliveryByReception(addresse, receptionOfDelivery);
            return delivery != null ? createDelivery(delivery) : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve OSCI Message from DB");
        }
    }

    protected boolean hasMoreDeliveries(X509Certificate addresse) {
        try {
            return persistenceManager.hasMoreDeliveries(addresse);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private OSCIStoreDelivery createDelivery(final StoreDeliveryVO delivery) throws SOAPException, IOException, OSCIException {
        OSCIStoreDelivery d = null;
        try {
            if (delivery.data == null || delivery.data.length == 0) {
                throw new IOException("No message data available for <" + delivery.messageId + ">");
            }

            final ByteArrayInputStream bytes = new ByteArrayInputStream(delivery.data);
            final SOAPMessage message = context.getTransportProvider().messageFromStream(bytes, new MessageInputAdapter() {
                private final String CONTENT_TYPE = "Content-Type";

                public Enumeration getHeaderNames() {
                    String[] s = {CONTENT_TYPE};
                    return Collections.enumeration(Arrays.asList(s));
                }

                public String getHeader(String headerName) {
                    return CONTENT_TYPE.equalsIgnoreCase(headerName) ? "Multipart/Related" : null;
                }
            });

            final OSCIIntermediaryDeliveryFactory factory = context.getIntermediaryDeliveryFactory();
            d = (OSCIStoreDelivery)factory.createMessage(context.getDocumentFactory().unmarshal(message));
        } finally {
            if (d == null) {
                LOG.error("Could not load message data from DB for <" + delivery.messageId + ">. The entry will be removed.");
                if (delivery.data != null && delivery.data.length > 0) {
                    LOG.error("=== START Message Data =============================================================");
                    final OutputStream log = LoggingOutputStream.wrap(new OSCIUtils.NullOutputStream(), Priority.ERROR, LOG);
                    try {
                        log.write(delivery.data);
                    } finally {
                        log.close();
                        LOG.error("=== END Message Data =============================================================");
                    }
                }
                try {
                    persistenceManager.removeStoreDelivery(delivery.messageId);
                } catch (Exception e) {
                    LOG.fatal("Removing the broken message <" + delivery.messageId + "> failed.", e);
                }
            }
        }

        return d;
    }

    public void removeStoreDelivery(OSCIStoreDelivery delivery) {
        Tracing.startOperation("Remove OSCI Message from DB");
        final String messageId = delivery.getMessageId();
        try {
            persistenceManager.removeStoreDelivery(messageId);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Remove OSCI Message from DB");
        }
    }

    public void saveProcessCard(X509Certificate originator, X509Certificate addressee, OSCIProcessCard processCard, boolean isUpdate) {
        Tracing.startOperation("Save ProcessCard to DB");
        try {
            persistenceManager.saveProcessCard(originator, addressee, processCard, isUpdate);
        } catch (RemoteException e) {
            LOG.error("Failed to update/save ProcessCard: " + processCard);
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Save ProcessCard to DB");
        }
    }

    protected OSCIProcessCard getProcessCardByMessageId(X509Certificate requestor, String messageId) {
        Tracing.startOperation("Retrieve ProcessCard from DB");
        try {
            return persistenceManager.getProcessCardByMessageId(requestor, messageId);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve ProcessCard from DB");
        }
    }

    protected Collection getProcessCardsByRecentModfication(X509Certificate requestor, Date date, int limit, boolean noReception, OSCISelectionRule.Role role) {
        Tracing.startOperation("Retrieve ProcessCard from DB");
        try {
            return persistenceManager.getProcessCardsByRecentModfication(requestor, date, limit, noReception, role);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve ProcessCard from DB");
        }
    }

    protected Collection getProcessCardsByReceptionOfDelivery(X509Certificate requestor, Date date, int limit, boolean noReception, OSCISelectionRule.Role role) {
        Tracing.startOperation("Retrieve ProcessCard from DB");
        try {
            return persistenceManager.getProcessCardsByCreation(requestor, date, limit, noReception, role);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        } finally {
            Tracing.endOperation("Retrieve ProcessCard from DB");
        }
    }
}