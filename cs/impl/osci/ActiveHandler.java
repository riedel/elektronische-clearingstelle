/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 10.08.2004
 * Time: 09:35:10
 */
package cs.impl.osci;

import cs.api.ErrorCode;
import cs.api.directory.XtaConfig;
import cs.impl.NotificationMail;
import cs.impl.ProblemMessageUtil;
import cs.impl.config.ConfigException;
import cs.impl.util.Lock;
import cs.impl.util.Tracing;
import cs.persistence.monitoring.ejb.client.ProblemMessageVO;
import de.esslingen.mediakomm.osci.impl.transport.interfaces.OSCIDocumentMessage;
import de.esslingen.mediakomm.osci.transport.OSCICode;
import de.esslingen.mediakomm.osci.transport.OSCIContentContainer;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.OSCIEncryptedData;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIDeliveryException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.user.OSCINonIntermediaryCertificates;
import de.esslingen.mediakomm.osci.transport.user.OSCIProcessCard;
import de.esslingen.mediakomm.osci.transport.user.client.OSCIDialog;
import de.esslingen.mediakomm.osci.transport.user.client.OSCIFetchDelivery;
import de.esslingen.mediakomm.osci.transport.user.client.OSCIResponseToFetchDelivery;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class ActiveHandler extends OSCIHandler {
    private static final Logger LOG = Logger.getLogger(ActiveHandler.class);

    public static final String ID = ID_PREFIX + "active";

    private static volatile ActiveHandler ourInstance;

    private final Lock lock = new Lock();

    public ActiveHandler() {
    }

    public static ActiveHandler getInstance() {
        if (ourInstance == null) {
            synchronized (ActiveHandler.class) {
                if (ourInstance == null) {
                    ourInstance = new ActiveHandler();
                }
            }
        }
        return ourInstance;
    }

    protected String getID() {
        return ID;
    }

    public void checkForNewDelivery(String addressee, boolean expectMessage) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Check for Delivery, expecting message = " + expectMessage);
        }

        try {
            if (!lock.enter(false)) {
                LOG.info("Another request is already in progress");
                // It SHOULD be ok to ignore any request while another is pending, but better safe than sorry
                if (!expectMessage || lock.getWaiters() > 0) {
                    LOG.debug("Request ignored");
                    return;
                }
                LOG.info("Waiting for completion");
                lock.enter(true);
            }
        } catch (InterruptedException e) {
            LOG.warn("Interrupted while waiting on completion. Request aborted");
            return;
        }

        // DO NOT merge the try blocks because of the lock.exit() in finally!
        boolean success = false;
        try {
            Tracing.startOperation("OSCI Fetch Messages");
            _checkForNewDelivery(expectMessage, addressee);
            success = true;
        } finally {
            lock.exit();
            Tracing.endOperation("OSCI Fetch Messages");

            if (!success) {
                LOG.warn("Error while checking for new deliveries...");
            } else {
                LOG.debug("Done checking for new deliveries.");
            }
        }
    }

    protected synchronized void _checkForNewDelivery(boolean expectMessage, String addressee) {
        if (!expectMessage && addressee == null) {
            try {
                final XtaConfig.Account[] accounts = config.getDirectoryServiceImpl().getXtaAccountConfig().getAccounts();
                final Set certs = new HashSet();
                for (int i = 0; i < accounts.length; i++) {
                    final XtaConfig.Account account = accounts[i];
                    certs.add(account.getMappedCertificate().getCertificate().getSubjectX500Principal().getName());
                }

                for (Iterator iterator = certs.iterator(); iterator.hasNext(); ) {
                    String cert = (String)iterator.next();
                    doCheck(false, cert);
                }
            } catch (ConfigException e) {
                throw new RuntimeException(e);
            }
        }
        doCheck(expectMessage, addressee);
    }

    protected synchronized void doCheck(boolean expectMessage, String addressee) {
        OSCIDialog dialog = null;
        
        final LinkedList queue = new LinkedList();
        try {
            // do not cache the context, otherwise the JMX configurability breaks
            final OSCIContext osciContext = config.getClientContext(config.getImURL(), config.getImAlias(), addressee);
            LOG.info("CheckInbox: " + addressee + " @ " + osciContext.getIntermediaryURI());

            final boolean isLocalAccount;
            if (addressee != null) {
                final OSCIContext rawContext = config.getClientContext(config.getImURL(), config.getImAlias(), null);
                final CryptoProvider provider = (CryptoProvider)osciContext.getCryptographyProvider();
                final CryptoProvider rawProvider = (CryptoProvider)rawContext.getCryptographyProvider();
                final X509Certificate addrCert = provider.getCipherCertificate(addressee);
                final X509Certificate cipherCertificate = rawProvider.getCipherCertificate();

                isLocalAccount = equals(addrCert, cipherCertificate);

                if (!isLocalAccount) {
                    final XtaConfig.Account[] accounts = config.getDirectoryServiceImpl().getXtaAccountConfig().getAccounts(addressee);
                    if (accounts.length == 0 && !isClientConnectorCert(provider, addrCert)) {
                        LOG.info("Not a local account: " + addressee + ". Not attempting to fetch OSCI messages");
                        return;
                    }
                }
            } else {
                isLocalAccount = false;
            }
            expectMessage = expectMessage && isLocalAccount;

            dialog = osciContext.createDialog();

            // TODO: get list of unreceived message-ids first via getProcessCards()
            boolean done;
            do {
                final OSCIFetchDelivery delivery = osciContext.getClientDeliveryFactory().createOSCIFetchDelivery();
                try {
                    final OSCIResponseToFetchDelivery response = delivery.send(dialog);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Got a response: " + response.getProcessCard().getMessageId() + " = " + response.getFeedback());
                    }
                    if (queue.size() > 0) {
                        processResponse((OSCIResponseToFetchDelivery)queue.removeFirst());
                    }
                    queue.add(response);

                    // Keep going if there are more deliveries waiting for us. Unlikely in case
                    // we were triggered by a JMS Notification from the intermediary, but it is also
                    // possible to turn that off and do a batch-like processing every hour/day/etc.
                    done = !response.getFeedback().contains(OSCICode.C3800);

                } catch (OSCIDeliveryException e) {
                    if (e.getResponse().getFeedback().getResult().getCode().equals(OSCICode.C9803)) {
                        if (expectMessage) {
                            LOG.warn("Got a notification but no delivery is available.");
                        } else {
                            // a timer or manual message. no warning
                            LOG.debug("Nothing to fetch from " + config.getImURL());
                        }
                    } else {
                        LOG.error("Error fetching delivery", e);
                    }
                    dialog = null; // set to null to avoid attempting to close the dialog in an invalid state
                    break; // no more deliveries or problems getting them, bail out for now.
                }
                expectMessage = false;
            } while (!done);

        } catch (OSCIException e) {
            LOG.fatal("OSCI exception while trying to contact intermediary", e);

            new NotificationMail(ErrorCode.OSCI_FAILURE, "OSCI-Kommunikationsfehler")
                    .addContent("W�hrend der Nachrichten-Abholung vom Intermedi�r ist ein OSCI-Fehler aufgetreten.\n" +
                            "Die genaue Fehlerursache befindet sich im Anhang.")
                    .addContent(e)
                    .send();

        } catch (IOException e) {
            LOG.fatal("I/O error", e);

            new NotificationMail(ErrorCode.IO_FAILURE, "I/O-Kommunikationsfehler")
                    .addContent("W�hrend der Nachrichten-Abholung vom Intermedi�r ist ein Eingabe-/Ausgabefehler aufgetreten.\n" +
                            "Die genaue Fehlerursache befindet sich im Anhang.")
                    .addContent(e)
                    .send();
        } catch (KeyStoreException e) {
            LOG.fatal("Keystore error", e);

            new NotificationMail(ErrorCode.CONFIG_PROBLEM, "Keystore Konfigurationsfehler (Passwort?)")
                    .addContent("W�hrend der Nachrichten-Abholung vom Intermedi�r ist ein Fehler beim Ermitteln des Absender-Zertifikates aufgetreten.\n" +
                            "Die genaue Fehlerursache befindet sich im Anhang.")
                    .addContent(e)
                    .send();
        } catch (ConfigException e) {
            new NotificationMail(ErrorCode.CONFIG_PROBLEM, "Konfigurationsfehler")
                    .addContent("W�hrend der Nachrichten-Abholung vom Intermedi�r ist ein Fehler aufgetreten.\n" +
                            "Die genaue Fehlerursache befindet sich im Anhang.")
                    .addContent(e)
                    .send();
        } finally {
            if (dialog != null && dialog.isOpen()) {
                boolean closed = false;
                OSCIResponseToFetchDelivery response = null;
                try {
                    dialog.close();
                    closed = true;

                    if (queue.size() > 0) {
                        response = (OSCIResponseToFetchDelivery)queue.removeFirst();
                        processResponse(response);
                    }
                    assert queue.size() == 0;
                } catch (Exception e) {
                    if (closed) {
                        if (response != null) {
                            LOG.fatal("Unable to process response for " + response.getProcessCard().getMessageId(), e);
                        } else {
                            LOG.fatal("No response", e);
                        }
                    } else {
                        LOG.warn("Unable to close OSCI dialog", e);
                    }
                }
            }
        }
    }

    public void processResponse(final OSCIResponseToFetchDelivery response) throws OSCIException, IOException {
        final OSCIProcessCard processCard = response.getProcessCard();
        final OSCIDocumentMessage osciMsg = (OSCIDocumentMessage)response;
        try {
            final OSCIContentContainer[] contentContainers = response.getContentContainers();
            final OSCIEncryptedData[] encryptedData = response.getEncryptedData();
            final AllContent content = new AllContent(contentContainers, encryptedData);

            final OSCINonIntermediaryCertificates certificates = response.getNonIntermediaryCertificates();
            final X509Certificate signatureCertificate = response.getIntermediaryCertificates().getSignatureCertificate();

            // TODO: As it is impossible to signal any error back to the orginator, we should queue
            // the message in case it can't be delivered currently and try to process it later a few times.
            final Response r = handleContent(osciMsg, processCard, certificates, signatureCertificate, content, false);
            if (r != null && r.responseBytes != null) {
                // TODO: try to send the response back to the sender?
                LOG.warn("ActiveHandler received response from Clearingstelle.sendMessage(). The discarded response is: " + Utils.fromUTF8(r.responseBytes));
            }
        } catch (OSCIException e) {
            ProblemMessageUtil.createMessage(getID(), null, response.getProcessCard().getMessageId(), osciMsg, ProblemMessageVO.Direction.INCOMING, ErrorCode.OSCI_FAILURE, e);
            throw e;
        } catch (IOException e) {
            ProblemMessageUtil.createMessage(getID(), null, response.getProcessCard().getMessageId(), osciMsg, ProblemMessageVO.Direction.INCOMING, ErrorCode.IO_FAILURE, e);
            throw e;
        }
    }
}
