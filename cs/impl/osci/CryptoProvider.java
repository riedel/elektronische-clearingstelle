/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 26.08.2004
 * Time: 16:38:01
 */
package cs.impl.osci;

import cs.api.directory.DirectoryService;
import cs.api.directory.XtaConfig;
import cs.impl.config.ConfigException;
import cs.impl.config.ConfigurationImpl;
import cs.impl.config.ContextMgmtData;
import cs.impl.util.DNParseException;
import cs.impl.util.KeyStoreUtil;
import cs.impl.util.Tracing;
import de.esslingen.mediakomm.osci.impl.transport.OSCIInspectionReportImpl;
import de.esslingen.mediakomm.osci.impl.transport.service.OSCIPKIProviderImpl;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIContextConfigurationException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCISecurityException;
import de.esslingen.mediakomm.osci.transport.service.ExternalReference;
import de.esslingen.mediakomm.osci.transport.service.ExternalReferenceResolver;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.activation.DataSource;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509Certificate;
import java.util.*;

public abstract class CryptoProvider extends OSCIPKIProviderImpl {
    protected final Logger myLogger;

    private final Map cipherCache = Collections.synchronizedMap(new HashMap());
    private final Map sigCache = Collections.synchronizedMap(new HashMap());
    protected final ContextMgmtData myMgmtData;

    public CryptoProvider(OSCIContext context, ContextMgmtData mgmtData) throws Exception {
        super(context);
        myMgmtData = mgmtData;
        myLogger = Logger.getLogger(CryptoProvider.class.getName() + role().trim());
    }

    protected KeyStore createKeystoreInstance() throws NoSuchProviderException, KeyStoreException {
        final String type = myMgmtData.getOsciKeystoreType();
        if ("PKCS12".equals(type)) {
            // The Sun PKCS12 keystore seems to have some problems...
            return KeyStore.getInstance(type, "BC");
        } else {
            return KeyStore.getInstance(type);
        }
    }

    protected URL getKeystoreLocation() {
        return myMgmtData.getOsciKeystore();
    }

    protected String getKeystorePassword() {
        return myMgmtData.getOsciKeystorePassword();
    }

    /** @noinspection ReturnOfNull*/
    protected char[] getPasswordFor(X509Certificate cert, int nthTry) {
        if (nthTry == 1) {
            if (myLogger.isDebugEnabled()) {
                myLogger.debug("#1 - trying configured password for <" + cert.getSubjectDN().getName() + "> (" + getAliasForCert(cert) + ")");
            }

            // configured pw
            return myMgmtData.getOsciKeyPassword().toCharArray();
        } else if (nthTry == 2) {
            if (myLogger.isDebugEnabled()) {
                myLogger.debug("#2 - trying password from properties for <" + cert.getSubjectDN().getName() + "> (" + getAliasForCert(cert) + ")");
            }

            // password from context props
            return super.getPasswordFor(cert, 1);
        } else {
            myLogger.info("#" + nthTry + " - no or invalid password provided for <" + cert.getSubjectDN().getName() + ">");
            // bail out
            return null;
        }
    }

    protected boolean verifyOnlineStatus(X509Certificate tbvCert, Date refTime, OSCIInspectionReportImpl.InspectionImpl report) {
        Tracing.startOperation("Certificate Online Check");
        try {
            X509CRL[] crls = ConfigurationImpl.getInstance().getDirectoryServiceImpl().getCRLs(tbvCert);
            if (crls != null && crls.length > 0) {
                for (int i = 0; i < crls.length; i++) {
                    X509CRL crl = crls[i];
                    final X509CRLEntry rc = crl.getRevokedCertificate(tbvCert.getSerialNumber());
                    if (rc != null) {
                        if (rc.getRevocationDate().before(refTime)) {
                            if (myLogger.isEnabledFor(Level.INFO)) {
                                myLogger.info("Certificate <" + tbvCert.getSubjectDN().getName() + "> " +
                                        "has been revoked by CRL on " + rc.getRevocationDate() + ".");
                            }
                            OSCIInspectionReportImpl.OnlineResultImpl result = new OSCIInspectionReportImpl.OnlineResultImpl(false);
                            result.setCRLDate(rc.getRevocationDate());
                            report.setOnlineResult(result);
                            return false;
                        }
                    }
                }
                report.setOnlineResult(new OSCIInspectionReportImpl.OnlineResultImpl(true));
            } else {
                report.setOnlineResult(null);
            }
            return true;
        } catch (ConfigException e) {
            myLogger.error("CRL check", e);
            return false;
        } finally {
            Tracing.endOperation("Certificate Online Check");
        }
    }

    public X509Certificate getCipherCertificate(String alias) throws KeyStoreException {
        Tracing.startOperation("Cipher Certificate Lookup" + role());
        try {
            synchronized (cipherCache) {
                if (cipherCache.containsKey(alias)) {
                    return (X509Certificate)cipherCache.get(alias);
                }
            }
            X509Certificate ret = null;
            if (alias.indexOf('=') != -1) {
                // Looks like a DN string
                ret = KeyStoreUtil.getCertByDN(getKeyStore(), alias, KeyStoreUtil.CIPHER_USAGE);
                if (ret == null) {
                    ret = KeyStoreUtil.getCertByDN(getKeyStore(), alias, KeyStoreUtil.ANY_USAGE);
                }
            }
            if (ret == null) {
                ret = super.getCipherCertificate(alias);
            }

            cipherCache.put(alias, ret);
            return ret;
        } finally {
            Tracing.endOperation("Cipher Certificate Lookup" + role());
        }
    }

    public X509Certificate getSignatureCertificate(String alias) throws KeyStoreException {
        Tracing.startOperation("Signature Certificate Lookup" + role());
        try {
            synchronized (sigCache) {
                if (sigCache.containsKey(alias)) {
                    return (X509Certificate)sigCache.get(alias);
                }
            }
            X509Certificate ret = null;
            if (alias.indexOf('=') != -1) {
                // Looks like a DN string
                ret = KeyStoreUtil.getCertByDN(getKeyStore(), alias, KeyStoreUtil.SIGNATURE_USAGE);
                if (ret == null) {
                    ret = KeyStoreUtil.getCertByDN(getKeyStore(), alias, KeyStoreUtil.ANY_USAGE);
                }
            }

            if (ret == null) {
                ret = super.getSignatureCertificate(alias);
            }

            sigCache.put(alias, ret);
            return ret;
        } finally {
            Tracing.endOperation("Signature Certificate Lookup" + role());
        }
    }

    private String role() {
        return " [" + getRole() + "]";
    }

    protected abstract String getRole();

    public boolean verifySignature(final Element signature, final ExternalReferenceResolver resolver) throws OSCISecurityException {
        Tracing.startOperation("OSCI Signaturprüfung" + role());
        try {
            return super.verifySignature(signature, resolver);
        } finally {
            Tracing.endOperation("OSCI Signaturprüfung" + role());
        }
    }

    public Element sign(Document doc, final ExternalReference[] references) throws OSCISecurityException, IOException {
        Tracing.startOperation("OSCI Transportsignatur" + role());
        try {
            return super.sign(doc, references);
        } finally {
            Tracing.endOperation("OSCI Transportsignatur" + role());
        }
    }

    public Element sign(String signer, Document doc, final ExternalReference[] references) throws OSCISecurityException, IOException {
        Tracing.startOperation("OSCI Signatur" + role());
        try {
            return super.sign(signer, doc, references);
        } finally {
            Tracing.endOperation("OSCI Signatur" + role());
        }
    }

    public Element encrypt(DataSource dataSource) throws OSCISecurityException, IOException {
        Tracing.startOperation("OSCI Verschlüsselung" + role());
        try {
            return super.encrypt(dataSource);
        } finally {
            Tracing.endOperation("OSCI Verschlüsselung" + role());
        }
    }

    public Element encrypt(DataSource dataSource, X509Certificate cert) throws OSCISecurityException, IOException {
        Tracing.startOperation("OSCI Transportverschlüsselung" + role());
        try {
            return super.encrypt(dataSource, cert);
        } finally {
            Tracing.endOperation("OSCI Transportverschlüsselung" + role());
        }
    }

    public static class Client extends CryptoProvider {

        private DirectoryService myDirectoryService;

        public Client(OSCIContext ctx) throws Exception {
            super(ctx, ConfigurationImpl.getInstance().getClientMgmtData());
            myDirectoryService = ConfigurationImpl.getInstance().getDirectoryServiceImpl();
        }

        public X509Certificate getCipherCertificate() throws OSCIContextConfigurationException, OSCISecurityException {
            final String originator = context.getOriginator();
            if (originator.indexOf("=") != -1) {
                try {
                    final XtaConfig.Account[] accounts = myDirectoryService.getXtaAccountConfig().getAccounts(originator);
                    if (accounts.length > 0) {
                        return accounts[0].getMappedCertificate().getCertificate();
                    }
                } catch (RuntimeException e) {
                    if (!(e.getCause() instanceof DNParseException)) {
                        throw e;
                    }
                }
            }
            return super.getCipherCertificate();
        }

        public X509Certificate getSignatureCertificate() throws OSCIContextConfigurationException, OSCISecurityException {
            final String originator = context.getOriginator();
            if (originator.indexOf("=") != -1) {
                try {
                    final XtaConfig.Account[] accounts = myDirectoryService.getXtaAccountConfig().getAccounts(originator);
                    if (accounts.length > 0) {
                        return accounts[0].getMappedCertificate().getCertificate();
                    }
                } catch (RuntimeException e) {
                    if (!(e.getCause() instanceof DNParseException)) {
                        throw e;
                    }
                }
            }
            return super.getSignatureCertificate();
        }

        public X509Certificate getSignatureCertificate(String alias) throws KeyStoreException {
            if (alias.indexOf("=") != -1) {
                try {
                    final XtaConfig.Account[] accounts = myDirectoryService.getXtaAccountConfig().getAccounts(alias);
                    if (accounts.length > 0) {
                        return accounts[0].getMappedCertificate().getCertificate();
                    }
                } catch (RuntimeException e) {
                    if (!(e.getCause() instanceof DNParseException)) {
                        throw e;
                    }
                }
            }
            return super.getSignatureCertificate(alias);
        }

        public Key findPrivateKeyFor(X509Certificate cert) throws OSCISecurityException {
            final XtaConfig.Account[] accounts = myDirectoryService.getXtaAccountConfig().getAccounts(cert.getSubjectDN().getName());
            if (accounts.length > 0) {
                try {
                   return accounts[0].getMappedCertificate().getKey();
                } catch (Exception e) {
                    myLogger.error("Could not obtain private key for XTA account " + accounts[0]);
                }
            }
            return super.findPrivateKeyFor(cert);
        }

        protected String getRole() {
            return "Client";
        }
    }

    abstract static class Supplier extends CryptoProvider {
        protected Supplier(OSCIContext ctx, ContextMgmtData data) throws Exception {
            super(ctx, data);
            myMgmtData.addObserver(new ClearCacheObserver(this));
        }
    }

    public static class PassiveClient extends Supplier {
        public PassiveClient(OSCIContext ctx) throws Exception {
            super(ctx, ConfigurationImpl.getInstance().getClientMgmtData());
        }
        protected String getRole() {
            return "PassiveClient";
        }
    }

    public static class Intermediary extends Supplier {
        public Intermediary(OSCIContext ctx) throws Exception {
            super(ctx, ConfigurationImpl.getInstance().getImMgmtData());
        }
        protected String getRole() {
            return "Intermed";
        }
    }

    private static class ClearCacheObserver implements Observer {
        private final WeakReference<CryptoProvider> myArg;

        public ClearCacheObserver(CryptoProvider arg) {
            myArg = new WeakReference<CryptoProvider>(arg);
        }

        public void update(Observable o, Object _) {
            final CryptoProvider prov = myArg.get();
            if (prov != null) {
                prov.myLogger.info("Clearing caches after modification on " + o);
                assert prov.myMgmtData == o;

                prov.cipherCache.clear();
                prov.sigCache.clear();
            }
        }
    }
}