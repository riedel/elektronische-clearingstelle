package cs.impl.osci;

import cs.impl.util.UUID;
import de.esslingen.mediakomm.osci.impl.transport.intermediary.MessageIdGenerator;

import javax.ejb.CreateException;
import javax.naming.NamingException;

/*
* Created by IntelliJ IDEA.
* User: sweinreuter
* Date: 13.08.2007
*/
public class GuidMessageIdGenerator implements MessageIdGenerator {

    public GuidMessageIdGenerator() throws NamingException, CreateException {
    }

    public String newMessageId() {
        return UUID.randomUUID().toString();
    }
}