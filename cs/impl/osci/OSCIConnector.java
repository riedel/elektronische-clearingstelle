/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 15:14:30
 */
package cs.impl.osci;

import cs.api.*;
import cs.api.directory.DirectoryEntry;
import cs.api.directory.MissingDirectoryInformation;
import cs.api.directory.NegativeDVDVLookup;
import cs.api.directory.UnknownDestinationException;
import cs.api.transport.Result;
import cs.api.transport.Transport;
import cs.api.transport.osci.OSCICommunicationType;
import cs.api.transport.osci.OSCIInfo;
import cs.api.transport.osci.OSCIResult;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.impl.ProblemMessageUtil;
import cs.impl.config.ConfigException;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.util.Tracing;
import cs.persistence.monitoring.ejb.client.ProblemMessageVO;
import de.esslingen.mediakomm.osci.transport.OSCICode;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.OSCIMessageIdSelectionRule;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIDeliveryException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIFatalException;
import de.esslingen.mediakomm.osci.transport.user.OSCIProcessCard;
import de.esslingen.mediakomm.osci.transport.user.client.*;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.security.cert.X509Certificate;

public class OSCIConnector implements Transport {
    private static final Logger LOG = Logger.getLogger(OSCIConnector.class);

    public static final String ID = "osci.outgoing";

    private OSCIConnector() {
    }

    public boolean isLocal() {
        return false;
    }

    public String getName() {
        return ID;
    }

    public Result send(String sender, String jobID, Message message) throws ConnectorException {

        final String type = message.getType();
        final String receiver = message.getReceiver();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Processing message: " + type + " from " + message.getSender());
        }

        Tracing.startOperation("Send OSCI Message");
        try {
            final Configuration instance = ConfigurationImpl.getInstance();
            final DirectoryEntry rd = instance.getDirectoryServiceImpl().lookup(type, receiver);
            final String addressee = type + "||" + receiver;
            final URL imURL = rd.getIntermediaryURL();
            final String originator = message instanceof OSCIRoutingInfo ? ((OSCIRoutingInfo)message).getOriginator() : null;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Intermediary for " + addressee + " = " + imURL);
                LOG.debug("Originator: " + originator);
            }

            final OSCIContext context = instance.getClientContext(imURL, addressee + "||intermediary", originator);

            if (message instanceof OSCIRoutingInfo && ((OSCIRoutingInfo)message).getCommunicationType() != null) {
                final OSCICommunicationType ctype = ((OSCIRoutingInfo)message).getCommunicationType();

                if (ctype == OSCICommunicationType.ASYNC) {
                    return sendAsync(message, rd, addressee, context, originator);
                } else if (ctype == OSCICommunicationType.ONE_WAY) {
                    if (rd.getContentReceiver() == null) {
                        throw new MissingDirectoryInformation(message.getType(), message.getReceiver(), "Missing ContentReceiver URI for synchronous OSCI message");
                    }
                    return sendOneWay(message, rd, addressee, context, originator);
                } else if (ctype == OSCICommunicationType.REQUEST_RESPONSE) {
                    if (rd.getContentReceiver() == null) {
                        throw new MissingDirectoryInformation(message.getType(), message.getReceiver(), "Missing ContentReceiver URI for synchronous OSCI message");
                    }
                    return sendRequestResponse(message, rd, addressee, context, originator);
                } else {
                    throw new IllegalStateException("Unknown communication type: " + ctype);
                }
            } else {
                if (rd.getContentReceiver() == null) {
                    return sendAsync(message, rd, addressee, context, originator);
                } else if (rd.oneWay() != null) {
                    if (Boolean.TRUE.equals(rd.oneWay())) {
                        return sendOneWay(message, rd, addressee, context, originator);
                    } else {
                        return sendRequestResponse(message, rd, addressee, context, originator);
                    }
                } else {
                    throw new ConnectorException("Cannot send synchronous message. Neither messagetype nor directory specify communication-type one-way/request-response.");
                }
            }
        } catch (RuntimeException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                prepareProblemMessage(sender, jobID, message, cause, ProblemMessageVO.Direction.OUTGOING, ErrorCode.IO_FAILURE);
                throw new ConnectorException(cause);
            } else if (cause instanceof CsException) {
                prepareProblemMessage(sender, jobID, message, cause, ProblemMessageVO.Direction.OUTGOING, ((CsException)cause).getCode());
                throw new ConnectorException(cause);
            } else if (cause != null) {
                prepareProblemMessage(sender, jobID, message, cause, ProblemMessageVO.Direction.OUTGOING, ErrorCode.UNKNOWN);
                throw new ConnectorException(cause);
            } else {
                prepareProblemMessage(sender, jobID, message, e, ProblemMessageVO.Direction.OUTGOING, ErrorCode.UNKNOWN);
                throw e;
            }
        } catch (SAXException e) {
            throw new ConnectorException(e);
        } catch (IOException e) {
            prepareProblemMessage(sender, jobID, message, e, ProblemMessageVO.Direction.OUTGOING, ErrorCode.IO_FAILURE);
            throw new ConnectorException(e);
        } catch (OSCIException e) {
            prepareProblemMessage(sender, jobID, message, e, ProblemMessageVO.Direction.OUTGOING, ErrorCode.OSCI_FAILURE);
            throw new ConnectorException(e);
        } catch (NegativeDVDVLookup e) {
            LOG.warn("Cannot resolve OSCI destination", e);
            // be sure to update resend-counter, but do not create a new message initially
            ProblemMessageUtil.updateMessage(sender, jobID, message, ProblemMessageVO.Direction.OUTGOING, e.getCode(), e);
            throw new CannotHandleException(getName(), message, e);
        } catch (UnknownDestinationException e) {
            LOG.warn("Cannot resolve OSCI destination", e);
            prepareProblemMessage(sender, jobID, message, e, ProblemMessageVO.Direction.OUTGOING, e.getCode());
            throw new CannotHandleException(getName(), message, e);
        } catch (ConfigException e) {
            LOG.error("Config problem", e);
            throw new CannotHandleException(getName(), message, e);
        } finally {
            ProblemMessageVO.RESENDABLE.set(null);
            Tracing.endOperation("Send OSCI Message");
        }
    }

    private Result sendRequestResponse(Message message, DirectoryEntry rd, String addressee, OSCIContext context, String originator) throws OSCIException, SAXException, IOException {
        ProblemMessageVO.RESENDABLE.set(Boolean.FALSE);

        // SYNCHRONOUS
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending MediateDelivery (synchronous) to " + addressee);
        }

        final OSCIMediateDelivery delivery = context.getClientDeliveryFactory().createOSCIMediateDelivery();
        delivery.setAddressee(addressee);
        delivery.setContentReceiver(rd.getContentReceiver());

        Utils.messageToContent(message, addressee, delivery, originator);

        final OSCIDialog dialog = context.createDialog();
        final OSCIResponseToMediateDelivery response = delivery.send(dialog);
        dialog.close();

        final Utils.ByteContent bytes = Utils.extractContent(response.getContentContainers(), false);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Response from supplier: " + Utils.asString(bytes.getBytes()));
        }

        final OSCIProcessCard processCard = response.getProcessCard();
        return new OSCIResult(bytes.getBytes(), createInfo(processCard, rd, delivery.getNonIntermediaryCertificates().getCipherCertificateOriginator()));
    }

    private Result sendOneWay(Message message, DirectoryEntry rd, String addressee, OSCIContext context, String originator) throws OSCIException, SAXException, IOException {
        ProblemMessageVO.RESENDABLE.set(Boolean.TRUE);

        // SYNCHRONOUS
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending ForwardDelivery (synchronous) to " + addressee);
        }

        final OSCIForwardDelivery delivery = context.getClientDeliveryFactory().createOSCIForwardDelivery();
        delivery.setAddressee(addressee);
        delivery.setContentReceiver(rd.getContentReceiver());

        Utils.messageToContent(message, addressee, delivery, originator);

        final OSCIResponseToForwardDelivery response = delivery.send();

        final OSCIProcessCard processCard = response.getProcessCard();
        return new OSCIResult(null, createInfo(processCard, rd, delivery.getNonIntermediaryCertificates().getCipherCertificateOriginator()));
    }

    private Result sendAsync(Message message, DirectoryEntry rd, String addressee, OSCIContext context, String originator) throws OSCIException, SAXException, IOException {
        ProblemMessageVO.RESENDABLE.set(Boolean.TRUE);

        // ASYNCHRONOUS
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending StoreDelivery (async) delivery to " + addressee);
        }

        final OSCIStoreDelivery delivery = context.getClientDeliveryFactory().createOSCIStoreDelivery();
        delivery.setAddressee(addressee);
        Utils.messageToContent(message, addressee, delivery, originator);

        final OSCIResponseToStoreDelivery response;
        try {
            response = delivery.send();

            final OSCIProcessCard processCard = response.getProcessCard();
            return new OSCIResult(null, createInfo(processCard, rd, delivery.getNonIntermediaryCertificates().getCipherCertificateOriginator()));
        } catch (IOException e) {
            final Result result = verifyReceptionOnError(rd, context, delivery, e);
            if (result == null) {
                throw e;
            }
            return result;
        } catch (OSCIException e) {
            final Result result = verifyReceptionOnError(rd, context, delivery, e);
            if (result == null) {
                throw e;
            }
            return result;
        }
    }

    private Result verifyReceptionOnError(DirectoryEntry rd, OSCIContext context, OSCIStoreDelivery delivery, Exception e) throws OSCIException, IOException {
        final String messageId = delivery.getMessageId();
        if (messageId == null) {
            LOG.info("OSCI error - Could not obtain MessageID");
            return null;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending failed for <" + messageId + ">: ", e);
        } else {
            LOG.warn("Sending failed for <" + messageId + ">: " + e.toString());
        }

        if (Boolean.getBoolean("ecs.osci.no-reception-verification-on-failure")) {
            LOG.info("OSCI error - Reception verification is turned off!");
            return null;
        }

        LOG.info("OSCI error - Verifying reception for <" + messageId + ">");
        try {
            final OSCIFetchProcessCard fetchProcessCard = context.getClientDeliveryFactory().createFetchProcessCard();
            fetchProcessCard.setSelectionRule(new OSCIMessageIdSelectionRule(messageId));
            final OSCIDialog dialog = context.createDialog();
            final OSCIResponseToFetchProcessCard responseToFetchProcessCard = fetchProcessCard.send(dialog);
            dialog.close();

            final OSCIProcessCard[] processCards = responseToFetchProcessCard.getProcessCards();
            if (processCards.length > 0) {
                if (!processCards[0].getMessageId().equals(messageId)) {
                    LOG.error(String.format("Received OSCI ProcessCard from %s for different message: %s vs. %s", rd.getIntermediaryURL(), processCards[0].getMessageId(), messageId));
                    return null;
                }

                LOG.info(String.format("Intermed %s did receive message <%s>. Delivery considered successful.", rd.getIntermediaryURL(), messageId));
                return new OSCIResult(null, createInfo(processCards[0], rd, fetchProcessCard.getNonIntermediaryCertificates().getCipherCertificateOriginator()));
            } else {
                LOG.info(String.format("Intermed %s did not receive message <%s>. Delivery was not successful.", rd.getIntermediaryURL(), messageId));
            }
        } catch (OSCIDeliveryException e1) {
            if (e1.getFeedback().contains(OSCICode.C9804)) {
                LOG.info(String.format("Intermed %s did not receive message <%s>. Delivery was not successful.", rd.getIntermediaryURL(), messageId));
            } else {
                LOG.error(String.format("Failed to fetch process card from %s for <%s> after send error. Will fail with previous exception", rd.getIntermediaryURL(), messageId), e1);
            }
        } catch (OSCIFatalException e1) {
            if (e1.getCode() == OSCICode.C9804) {
                LOG.info(String.format("Intermed %s did not receive message <%s>. Delivery was not successful.", rd.getIntermediaryURL(), messageId));
            } else {
                LOG.error(String.format("Failed to fetch process card from %s for <%s> after send error. Will fail with previous exception", rd.getIntermediaryURL(), messageId), e1);
            }
        } catch (Exception e1) {
            LOG.error(String.format("Failed to fetch process card from %s for <%s> after send error. Will fail with previous exception", rd.getIntermediaryURL(), messageId), e1);
        }
        return null;
    }

    private void prepareProblemMessage(String sender, String jobID, Message message, Throwable e, ProblemMessageVO.Direction direction, ErrorCode code) {
        ProblemMessageUtil.createMessage(sender, jobID, message, direction, code, e);
    }

    private OSCIInfo createInfo(final OSCIProcessCard processCard, final DirectoryEntry rd, X509Certificate originatorCertificate) {
        if (LOG.isInfoEnabled()) {
            LOG.info("OSCI Message-ID from " + rd.getIntermediaryURL() + ": " + processCard.getMessageId());
        }

        return new OSCIInfo(
                processCard.getMessageId(),
                processCard.getSubject(),
                processCard.getCreation().getDate(),
                rd.getCipherCert(),
                rd.getIntermediaryCert(),
                rd.getIntermediaryURL(),
                originatorCertificate);
    }

    public static OSCIConnector getInstance() {
        return new OSCIConnector();
    }
}