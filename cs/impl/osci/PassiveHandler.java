/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 09.08.2004
 * Time: 17:54:04
 */
package cs.impl.osci;

import cs.api.Message;
import cs.api.UnknownTypeException;
import cs.api.directory.NoAccessException;
import cs.api.directory.UnknownDestinationException;
import cs.api.transport.osci.OSCIInfo;
import cs.api.transport.osci.OSCIMessage;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.api.validation.ValidationException;
import cs.impl.MessageWrapper;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import de.esslingen.mediakomm.osci.impl.transport.OSCIUtils;
import de.esslingen.mediakomm.osci.impl.transport.interfaces.OSCIDocumentMessage;
import de.esslingen.mediakomm.osci.transport.OSCICode;
import de.esslingen.mediakomm.osci.transport.OSCIContentContainer;
import de.esslingen.mediakomm.osci.transport.OSCIEncryptedData;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.user.supplier.*;
import org.apache.log4j.Logger;

public class PassiveHandler extends OSCIHandler implements OSCIAcceptDeliveryHandler, OSCIProcessDeliveryHandler {
    private static final Logger LOG = Logger.getLogger(PassiveHandler.class);
    
    public static final String ID = ID_PREFIX + "passive";

    public PassiveHandler() {
    }

    protected String getID() {
        return ID;
    }

    public void handleAcceptDelivery(OSCIAcceptDelivery delivery, OSCIResponseToAcceptDelivery response) throws OSCIException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling AcceptDelivery");
        }

        try {
            final OSCIContentContainer[] contentContainers = delivery.getContentContainers();
            final OSCIEncryptedData[] encryptedData = delivery.getEncryptedData();
            final AllContent content = new AllContent(contentContainers, encryptedData);
            final Response b = handleContent((OSCIDocumentMessage)delivery, delivery.getProcessCard(), delivery.getNonIntermediaryCertificates(), response.getIntermediaryCertificates().getCipherCertificate(), content, true);

            if (b.responseBytes != null) {
                LOG.warn("PassiveHandler (AcceptDelivery) received response from Clearingstelle.sendMessage(). The discarded response is: " + Utils.fromUTF8(b.responseBytes));
            }
        } catch (OSCIException e) {
            LOG.fatal("Caught OSCIException", e);
            throw e;
        } catch (Exception e) {
            LOG.fatal("Caught exception during message processing", e);
            throw new OSCIException(e);
        }
    }

    public void handleProcessDelivery(OSCIProcessDelivery delivery, final OSCIResponseToProcessDelivery response) throws OSCIException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling ProcessDelivery");
        }

        try {
            final OSCIContentContainer[] contentContainers = delivery.getContentContainers();
            final OSCIEncryptedData[] encryptedData = delivery.getEncryptedData();
            final AllContent content = new AllContent(contentContainers, encryptedData);
            final Response r = handleContent((OSCIDocumentMessage)delivery, delivery.getProcessCard(), delivery.getNonIntermediaryCertificates(), response.getIntermediaryCertificates().getCipherCertificate(), content, true);
            if (r.responseBytes != null) {
                final OSCIContentContainer container = response.addContentContainer();

                container.addContent(OSCIUtils.makeDocument(r.responseBytes));
                try {
                    final MessageWrapper message = MessageWrapper.createMessage(r.responseBytes);
                    if (!message.isValid()) {
                        LOG.warn("Response message is not valid for type " + message.getMessage().getType());
                        LOG.warn(Utils.fromUTF8(r.responseBytes));
                    }
                    applyContainerId(container, response, message.getMessage());
                } catch (ValidationException e) {
                    LOG.error("Failed to validate response message", e);
                    LOG.error(Utils.fromUTF8(r.responseBytes));
                    applyContainerId(container, response, r.origMessage);
                } catch (UnknownTypeException e) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Could not determine message type for response to " + delivery.getProcessCard().getMessageId());
                    }
                    applyContainerId(container, response, r.origMessage);
                }

                final Configuration config = ConfigurationImpl.getInstance();
                if (config.isEncryptContent()) {
                    container.encryptFor(delivery.getOriginator());
                }
                if (config.isSignContent()) {
                    container.signWith(config.getClientMgmtData().getOsciKeyAlias());
                }
            } else {
                LOG.warn("No response to return in ResponseToProcessDelivery for " + delivery.getProcessCard().getMessageId());
            }
        } catch (OSCIException e) {
            LOG.fatal("Caught OSCIException", e);
            if (e.getCause() instanceof NoAccessException || e.getCause() instanceof UnknownDestinationException) {
                response.addFeedbackEntry(OSCICode.C9805);
            } else {
                throw e;
            }
        } catch (Exception e) {
            LOG.fatal("Caught exception during message processing", e);
            throw new OSCIException(e);
        }
    }

    private static void applyContainerId(OSCIContentContainer container, OSCIResponseToProcessDelivery response, Message msg) {
        if (msg instanceof OSCIRoutingInfo) {
            final OSCIRoutingInfo routingInfo = (OSCIRoutingInfo)msg;
            final String requiredId = routingInfo.getContainerId();
            if (requiredId != null) {
                container.setId(requiredId);
            } else {
                LOG.debug("Routing info for " + msg.getType() + " doesn't supply container-id");
            }
            if (msg instanceof OSCIInfo.Provider) {
                response.setSubject(((OSCIInfo.Provider)msg).getOsciInfo().getSubject());
            } else {
                response.setSubject(routingInfo.getSubject());
            }
        } else {
            if (msg instanceof OSCIMessage) {
                applyContainerId(container, response, ((OSCIMessage)msg).getMsg());
            } else if (msg instanceof OSCIInfo.Provider) {
                response.setSubject(((OSCIInfo.Provider)msg).getOsciInfo().getSubject());
            }
        }
    }
}