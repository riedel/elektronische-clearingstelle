/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 09.08.2004
 * Time: 17:58:20
 */
package cs.impl.osci;

import cs.api.Message;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import de.esslingen.mediakomm.osci.impl.transport.OSCIUtils;
import de.esslingen.mediakomm.osci.transport.OSCIAttachment;
import de.esslingen.mediakomm.osci.transport.OSCIContent;
import de.esslingen.mediakomm.osci.transport.OSCIContentContainer;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIBadSignatureException;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.user.client.OSCIContentDelivery;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    private static final Logger LOG = Logger.getLogger(Utils.class);

    private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();

    private Utils() {
    }

    public static String fromUTF8(byte[] bytes) {
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public static void messageToContent(Message message, String addressee, final OSCIContentDelivery delivery, String signer) throws SAXException, IOException, OSCIException {
        final Document document = OSCIUtils.makeDocument(message.getMessage());
        final OSCIContentContainer cc = delivery.addContentContainer();
        final Boolean encrypt;
        final Boolean sign;
        if (message instanceof OSCIRoutingInfo) {
            final OSCIRoutingInfo routingInfo = (OSCIRoutingInfo)message;
            cc.setId(routingInfo.getContainerId());
            delivery.setSubject(routingInfo.getSubject());
            encrypt = routingInfo.getEncrypt();
            sign = routingInfo.getSign();
        } else {
            cc.setId("XMELD_DATA");
            encrypt = null;
            sign = null;
        }
        cc.addContent(document);
        final Configuration config = ConfigurationImpl.getInstance();

        if (encrypt == null ? config.isEncryptContent() : encrypt) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Encrypting ContentContainer for " + addressee);
            }

            cc.encryptFor(addressee);
        }
        if (sign == null ? config.isSignContent() : sign) {
            final String author = signer != null ? signer : config.getClientMgmtData().getOsciKeyAlias();

            if (LOG.isDebugEnabled()) {
                LOG.debug("Signing ContentContainer as " + author);
            }

            cc.signWith(author);
        }
    }

    public static class ByteContent {
        private final byte[] myBytes;
        private final boolean myValidSignature;

        private ByteContent(byte[] bytes, boolean validSignature) {
            myBytes = bytes;
            myValidSignature = validSignature;
        }

        public byte[] getBytes() {
            return myBytes;
        }

        public boolean isValidSignature() {
            return myValidSignature;
        }

        public static ByteContent create(byte[] bytes, boolean validSignature) {
            return bytes != null ? new ByteContent(bytes, validSignature) : null;
        }
    }

    /** @noinspection ReturnOfNull*/
    public static ByteContent extractContent(final OSCIContentContainer[] contentContainers, boolean idCheck) throws OSCIException, IOException {
        final List idList = new ArrayList();

        OSCIContentContainer container = null;
        if (idCheck) {
            for (int i = 0; i < contentContainers.length; i++) {
                final String id = contentContainers[i].getId();
                if (id != null && id.length() > 0) {
                    // TODO: still a little hack
                    if (id.matches("X.*_DATA")) {
                        container = contentContainers[i];
                        break;
                    }
                    idList.add(id);
                }
            }
        } else {
            container = contentContainers[0];
        }

        try {
            if (container != null) {
                // TODO: Hack to be able to get signer certificates from actually used ContentContainer
                contentContainers[0] = container;
                return ByteContent.create(extractContentFromContainer(container), true);
            } else if (contentContainers.length > 0) {
                if (contentContainers.length > 1) {
                    LOG.warn("Encountered more than one ContentContainer: " + idList + ". Ignoring all except the first.");
                } else {
                    LOG.info("Got ContentContainer that doesn't have any recognized ID: " + contentContainers[0].getId());
                }
                return ByteContent.create(extractContentFromContainer(contentContainers[0]), true);
            }
            return null;
        } catch (OSCIBadSignatureException e) {
            return ByteContent.create(extractContentFromContainer(e.getContainer()), false);
        }
    }

    /** @noinspection ReturnOfNull*/
    private static byte[] extractContentFromContainer(OSCIContentContainer container) throws OSCIException, IOException {
        final OSCIContent[] contents = container.getContents();
        if (contents.length > 0) {
            return getBytesFromContent(contents);
        } else {
            final OSCIAttachment[] attachments = container.getAttachments();
            if (attachments.length > 0) {
                return getBytesFromContent(attachments);
            } else {
                LOG.warn("No Content found in ContentContainer: " + container.getId());
            }
        }
        return null;
    }

    public static String asString(final byte[] bytes) {
        return bytes == null ? null : fromUTF8(bytes);
    }

    public static byte[] getBytesFromContent(final OSCIContent[] contents) {
        assert contents.length > 0;

        OSCIContent content = contents[0];
        if (contents.length > 1) {
            LOG.warn("More than one Content in ContentContainer. Additional Content is discarded.");
        }
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            transformerFactory.newTransformer().transform(new DOMSource(content.getContent()), new StreamResult(bos));
        } catch (TransformerException e) {
            LOG.error("Unexpected Exception in identity transformation", e);
            throw new Error(e);
        } catch (TransformerFactoryConfigurationError e) {
            LOG.error("Unexpected Exception in identity transformation", e);
            throw new Error(e);
        }

        return bos.toByteArray();
    }

    public static byte[] getBytesFromContent(OSCIAttachment[] attachments) throws IOException {
        assert attachments.length > 0;

        OSCIAttachment attachment = attachments[0];
        if (attachments.length > 1) {
            LOG.warn("More than one Attachment in ContentContainer. Additional Attachments are discarded.");
        }

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            final InputStream inputStream = attachment.getInputStream();
            try {
                copy(inputStream, bos);
            } finally {
                inputStream.close();
            }
        } finally {
            attachment.dispose();
        }

        return bos.toByteArray();
    }

    /** @noinspection EmptyCatchBlock*/
    private static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        try {
            byte[] b = new byte[16*1024];
            int n;
            while ((n = inputStream.read(b)) != -1) {
                outputStream.write(b, 0, n);
            }
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) { }
            try {
                outputStream.close();
            } catch (IOException e) { }
        }
    }
}
