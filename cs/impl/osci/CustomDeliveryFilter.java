/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 12.08.2004
 * Time: 14:48:47
 */
package cs.impl.osci;

import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.util.DNParseException;
import cs.impl.util.DName;
import de.esslingen.mediakomm.osci.impl.transport.intermediary.AcceptAllFilter;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIStoreDelivery;
import org.apache.log4j.Logger;

import java.security.cert.X509Certificate;

/**
 * This filter implemenation is neither cryptographically secure, nor does
 * it provide the possibility of specifying multiple filter patterns. It's
 * just an idea for now.
 */
public class CustomDeliveryFilter extends AcceptAllFilter {
    private static final Logger LOG = Logger.getLogger(CustomDeliveryFilter.class);

    private final OSCIContext myContext;
    private final Configuration myConfig;

    public CustomDeliveryFilter(OSCIContext context) {
        myContext = context;
        myConfig = ConfigurationImpl.getInstance();
    }

    public boolean acceptStoreDelivery(OSCIStoreDelivery isd) {
        final X509Certificate addressee = isd.getAddressee();

        try {
            if (!myConfig.getDirectoryServiceImpl().isValidInbox(addressee)) {
                final OSCIContext eCsContext = myConfig.getClientContext(myConfig.getImURL(), myConfig.getImAlias(), null);
                if (addressee.equals(eCsContext.getTransportCryptographyProvider().getCipherCertificate())) {
                    return true;
                }
                LOG.info("Certificate <" + addressee.getSubjectDN().getName() + "> has no configured inbox");
                return false;
            }
        } catch (Exception e) {
            LOG.error("Cannot obtain DirectoryService instance. Delivery will be rejected", e);
            // TODO: Send mail?
            return false;
        }

        // legacy code
        boolean matches = true;
        final String addr = myContext.getProperty("intermediary.filter.addressee.dn");
        if (addr != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Addressee pattern = " + addr);
            }

            try {
                final DName addrDN = new DName(addressee.getSubjectDN().getName());
                matches = addrDN.matches(addr);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("matches = " + matches);
                }
            } catch (DNParseException e) {
                LOG.error("Cannot parse DN '" + e.getDN() + "'", e);
                return false;
            }
        }

        final String orig = myContext.getProperty("intermediary.filter.originator.dn");
        if (matches && orig != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Originator pattern = " + orig);
            }

            final String _origDN = isd.getCertificates().getCipherCertificateOriginator().getSubjectDN().getName();
            try {
                final DName origDN = new DName(_origDN);
                matches = origDN.matches(orig);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("matches = " + matches);
                }
            } catch (DNParseException e) {
                LOG.error("Cannot parse DN '" + e.getDN() + "'", e);
                return false;
            }
        }

        return matches;
    }
}
