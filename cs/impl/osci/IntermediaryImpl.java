package cs.impl.osci;

import cs.impl.util.TransactionRunner;
import de.esslingen.mediakomm.osci.impl.transport.intermediary.IntermediaryRuntime;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.intermediary.OSCIRecordableMessage;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIClientDelivery;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIClientResponse;
import edu.emory.mathcs.backport.java.util.concurrent.Callable;
import org.apache.log4j.Logger;

import java.io.IOException;

/*
* Created by IntelliJ IDEA.
* User: sweinreuter
* Date: 30.08.12
*/
public class IntermediaryImpl extends IntermediaryRuntime {
    private static final Logger LOG = Logger.getLogger(IntermediaryImpl.class);

    public static final boolean TRANSACTION_DISABLED = "true".equals(System.getProperty("ecs.intermediary.disable-transactions", "false"));
    public static final String TX_SCOPE = System.getProperty("ecs.intermediary.tx-scope", "small");

    public IntermediaryImpl(OSCIContext context) {
        super(context);

        if (TRANSACTION_DISABLED) {
            LOG.info("Transactions disabled");
        } else {
            LOG.info("Transaction-Scope: " + TX_SCOPE);
        }
    }

    protected void handleReceive(final OSCIClientDelivery delivery, final OSCIClientResponse response, final Parameters parameters) throws IOException, OSCIException {
        final Callable runnable = new Callable() {
            public Object call() throws Exception {
                IntermediaryImpl.super.handleReceive(delivery, response, parameters);
                return null;
            }
        };
        try {
            if ("small".equals(TX_SCOPE) && !TRANSACTION_DISABLED) {
                final String messageId = delivery instanceof OSCIRecordableMessage ? ((OSCIRecordableMessage)delivery).getMessageId() : delivery.getClass().getName();
                new TransactionRunner("OSCI Message " + messageId, runnable).run();
            } else {
                runnable.call();
            }
        } catch (IOException e) {
            throw e;
        } catch (OSCIException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}