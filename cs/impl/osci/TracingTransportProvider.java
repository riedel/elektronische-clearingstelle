/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 17.05.2006
 * Time: 14:51:47
 */
package cs.impl.osci;

import cs.impl.util.Tracing;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.document.OSCIDocument;
import de.esslingen.mediakomm.osci.transport.document.OSCIMessageDocument;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.service.MessageInputAdapter;
import de.esslingen.mediakomm.osci.transport.service.MessageOutputAdapter;
import de.esslingen.mediakomm.osci.transport.service.OSCITransportProvider;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.io.InputStream;

public class TracingTransportProvider implements OSCITransportProvider {
    private final OSCITransportProvider provider;

    /** @noinspection UnusedDeclaration */
    public TracingTransportProvider(OSCIContext context, OSCITransportProvider provider) {
        this.provider = provider;
    }

    public SOAPMessage createMessage() throws SOAPException {
        return provider.createMessage();
    }

    public SOAPMessage messageFromStream(InputStream is, MessageInputAdapter mia) throws SOAPException, IOException {
        Tracing.startOperation("TransportProvider.messageFromStream()");
        try {
            return provider.messageFromStream(is, mia);
        } finally {
            Tracing.endOperation("TransportProvider.messageFromStream()");
        }
    }

    public void messageToStream(SOAPMessage message, MessageOutputAdapter out) throws SOAPException, IOException {
        Tracing.startOperation("TransportProvider.messageToStream()");
        try {
            provider.messageToStream(message, out);
        } finally {
            Tracing.endOperation("TransportProvider.messageToStream()");
        }
    }

    public OSCIMessageDocument send(OSCIDocument document, String uri) throws IOException, SOAPException, OSCIException {
        Tracing.startOperation("TransportProvider.send()");
        try {
            return provider.send(document, uri);
        } finally {
            Tracing.endOperation("TransportProvider.send()");
        }
    }
}