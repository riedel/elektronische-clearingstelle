/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 02.02.2007
 * Time: 19:59:11
 */
package cs.impl.osci;

import de.esslingen.mediakomm.osci.transport.service.OSCITimestampProvider;
import de.esslingen.mediakomm.osci.transport.OSCICryptographicTimestamp;
import de.esslingen.mediakomm.osci.transport.OSCITimestamp;
import de.esslingen.mediakomm.osci.transport.OSCITimestampQuality;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.document.OSCIDocument;
import de.esslingen.mediakomm.osci.impl.transport.service.OSCITimestampImpl;

import java.io.IOException;
import java.util.Date;

public class TimestampProvider implements OSCITimestampProvider {

    /** @noinspection UnusedDeclaration*/
    public TimestampProvider(OSCIContext context) {
    }

    public boolean supports(OSCITimestampQuality quality) {
        return quality == OSCITimestampQuality.PLAIN;
    }

    public OSCITimestamp createTimestamp(OSCITimestampQuality quality, OSCIDocument document) throws IOException, UnsupportedAlgorithmException {
        if (!supports(quality)) {
            throw new UnsupportedOperationException();
        }
        return createPlainTimestamp(new Date());
    }

    public boolean verifyTimestamp(OSCICryptographicTimestamp timestamp, OSCIDocument doc) throws IOException, UnsupportedAlgorithmException {
        throw new UnsupportedAlgorithmException(timestamp.getAlgorithm());
    }

    public OSCITimestamp createPlainTimestamp(Date date) {
        return new OSCITimestampImpl(date);
    }

    public OSCICryptographicTimestamp createCryptographicTimestamp(String algorithm, String data) throws IOException, UnsupportedAlgorithmException {
        throw new UnsupportedAlgorithmException(algorithm);
    }
}