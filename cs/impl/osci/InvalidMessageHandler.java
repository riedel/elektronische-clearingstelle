package cs.impl.osci;

import cs.impl.NotificationMail;
import cs.api.ErrorCode;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.service.OSCIInvalidMessageHandler;
import org.apache.log4j.Logger;

import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;

public class InvalidMessageHandler implements OSCIInvalidMessageHandler {
    private static final Logger LOG = Logger.getLogger(InvalidMessageHandler.class);

    /** @noinspection UnusedDeclaration,FieldCanBeLocal */
    private final OSCIContext context;

    public InvalidMessageHandler(OSCIContext context) {
        this.context = context;
    }

    public void validationFailed(int direction, SOAPMessage message) {
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            message.writeTo(baos);

            final NotificationMail mail;
            if (InvalidMessageHandler.INCOMING == direction) {
                mail = new NotificationMail(ErrorCode.INVALID_OSCI, "Ung�ltige OSCI Nachricht");
            } else {
                mail = new NotificationMail(ErrorCode.INVALID_OSCI_OUT, "Ung�ltige ausgehende OSCI Nachricht");
            }

            mail.addContent("Es wurde eine nicht spezifikationskonforme OSCI-Nachricht erkannt. Die genaue Ursache ist " +
                    "nicht n�her bekannt. Die OSCI-Nachricht befindet sich im Anhang.")
                    .addAttachment("osci-message.osci", baos.toByteArray())
                    .send();

        } catch (Throwable t) {
            LOG.warn("Error while sending Notifcation-Mail", t);
        }
    }

    public void validationFailed(int direction, SOAPMessage message, Throwable t) {
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            message.writeTo(baos);

            final NotificationMail mail;
            if (InvalidMessageHandler.INCOMING == direction) {
                mail = new NotificationMail(ErrorCode.INVALID_OSCI, "Ung�ltige OSCI Nachricht");
            } else {
                mail = new NotificationMail(ErrorCode.INVALID_OSCI_OUT, "Ung�ltige ausgehende OSCI Nachricht");
            }

            mail.addContent("Es wurde eine nicht spezifikationskonforme OSCI-Nachricht erkannt. " +
                    "Die OSCI-Nachricht sowie die Ursache f�r den Fehler befinden sich im Anhang.")
                    .addContent(t)
                    .addAttachment("osci-message.osci", baos.toByteArray())
                    .send();

        } catch (Throwable t2) {
            LOG.warn("Error while sending Notifcation-Mail", t2);
        }
    }
}
