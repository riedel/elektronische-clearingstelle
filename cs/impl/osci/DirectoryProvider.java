/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 27.07.2004
 * Time: 18:29:04
 */
package cs.impl.osci;

import cs.api.directory.UnknownDestinationException;
import cs.impl.config.ConfigException;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.config.ContextMgmtData;
import de.esslingen.mediakomm.osci.impl.transport.service.AbstractPKIProvider;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIDirectoryException;
import de.esslingen.mediakomm.osci.transport.service.OSCIDirectoryProvider;
import org.apache.log4j.Logger;

import java.net.URL;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;

public abstract class DirectoryProvider implements OSCIDirectoryProvider {
    private static final Logger LOG = Logger.getLogger(DirectoryProvider.class);

    private final Configuration config = ConfigurationImpl.getInstance();
    private final OSCIContext ctx;
    private final ContextMgmtData mgmtData;

    public DirectoryProvider(OSCIContext ctx, final ContextMgmtData mgmtData) {
        this.ctx = ctx;
        this.mgmtData = mgmtData;
    }

    public String resolve(String uri) {
        final URL supplierURL = config.getSupplierURL(uri);
        if (supplierURL != null) {
            return supplierURL.toExternalForm();
        }
        return uri;
    }

    /**
     * @deprecated see super method
     */
    public X509Certificate getSignatureCertificate(String alias) throws OSCIDirectoryException {
        LOG.warn("This method is supposed to be deprecated. Who's calling it??", new Throwable("<stacktrace>"));

        final String[] s = alias.split("\\|\\|");
        try {
            if (s.length > 1) {
                return config.getDirectoryServiceImpl().lookup(s[0], s[1]).getSigCert();
            } else if (alias.equals(mgmtData.getOsciKeyAlias())) {
                return ((AbstractPKIProvider)ctx.getCryptographyProvider()).getSignatureCertificate(alias);
            } else {
                LOG.error("Don't know what to do with alias: " + alias);
                return null;
            }
        } catch (UnknownDestinationException e) {
            LOG.warn("Lookup on signature certificate for '" + alias + "' failed", e);
            return null;
        } catch (KeyStoreException e) {
            LOG.error("SignatureCertificate", e);
            throw new OSCIDirectoryException(e);
        } catch (ConfigException e) {
            LOG.error("Config problem", e);
            throw new OSCIDirectoryException(e);
        }
    }

    public X509Certificate getCipherCertificate(String alias) throws OSCIDirectoryException {
        // aliases can be encoded as
        //   <service>||<ags>               indicates adressee's certificate
        //   <service>||<ags>||intermediary  indicates IM's certificate for this addressee
        //   <plain alias>                 plain keystore alias
        //   <DN string>                   DN of a certificate to use

        final String service;
        final String ags;
        final boolean isIntermediaryLookup;

        if (alias.startsWith("http://")) {
            isIntermediaryLookup = alias.endsWith("||intermediary");
            alias = alias.replaceFirst("\\|\\|intermediary$", "");
            final int i = alias.lastIndexOf("||");
            if (i > 6) {
                service = alias.substring(0, i);
                ags = alias.substring(i + 2);
            } else {
                service = null;
                ags = null;
                LOG.error("Malformed alias: " + alias);
            }
        } else if (alias.toUpperCase().contains("C=") || alias.toUpperCase().contains("CN=") || alias.toUpperCase().contains("O=") || alias.toUpperCase().contains("OU=")) {
            // DN string
            isIntermediaryLookup = false;
            service = ags = null;
        } else {
            final String[] s = alias.split("\\|\\|");
            if (s.length > 1) {
                service = s[0];
                ags = s[1];
            } else {
                service = null;
                ags = null;
            }
            isIntermediaryLookup = s.length > 2 && "intermediary".equals(s[2]);
        }

        try {

            if (isIntermediaryLookup) {
                return config.getDirectoryServiceImpl().lookup(service, ags).getIntermediaryCert();
            } else if (service != null && ags != null) {
                return config.getDirectoryServiceImpl().lookup(service, ags).getCipherCert();
            } else {
                // OSCI-Impl is looking up the Intermediary certificate using the DirectoryProvider
                // but the eCS is supposed to have this certificate in its own local keystore.
                // Therefore this is delegated to the Crypto-Provider that has access to this keystore.
                final X509Certificate cipherCertificate = ((CryptoProvider)ctx.getCryptographyProvider()).getCipherCertificate(alias);
                if (cipherCertificate == null) {
                    LOG.warn("Could not obtain certificate '" + alias + "' from crypto-provider's keystore");
                }
                return cipherCertificate;
            }
        } catch (UnknownDestinationException e) {
            throw new OSCIDirectoryException("Lookup on cipher certificate for '" + alias + "' failed", e);
        } catch (KeyStoreException e) {
            throw new OSCIDirectoryException(e);
        } catch (ConfigException e) {
            LOG.error("Config problem", e);
            throw new OSCIDirectoryException(e);
        }
    }

    public static class Client extends DirectoryProvider {
        public Client(OSCIContext ctx) {
            super(ctx, ConfigurationImpl.getInstance().getClientMgmtData());
        }

        public X509Certificate[] getCertPath(X509Certificate cert) {
            throw new UnsupportedOperationException("Intermediary only");
        }

        public boolean isTrustedRoot(X509Certificate cert) {
            throw new UnsupportedOperationException("Intermediary only");
        }
    }

    public static class Intermediary extends DirectoryProvider {
        private final Configuration config = ConfigurationImpl.getInstance();

        public Intermediary(OSCIContext ctx) {
            super(ctx, ConfigurationImpl.getInstance().getImMgmtData());
        }

        public X509Certificate[] getCertPath(X509Certificate cert) throws OSCIDirectoryException {
            try {
                return config.getDirectoryServiceImpl().getCertPath(cert);
            } catch (ConfigException e) {
                LOG.error("Config error", e);
                throw new OSCIDirectoryException(e);
            }
        }

        public boolean isTrustedRoot(X509Certificate cert) throws OSCIDirectoryException {
            try {
                return config.getDirectoryServiceImpl().isTrustedRoot(cert);
            } catch (ConfigException e) {
                LOG.error("Config error", e);
                throw new OSCIDirectoryException(e);
            }
        }
    }
}