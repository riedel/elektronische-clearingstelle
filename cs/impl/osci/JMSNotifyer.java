/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 09.08.2004
 * Time: 18:46:07
 */
package cs.impl.osci;

import de.esslingen.mediakomm.osci.impl.transport.intermediary.NotificationAdapter;
import de.esslingen.mediakomm.osci.transport.OSCIContext;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIResponseToStoreDelivery;
import de.esslingen.mediakomm.osci.transport.intermediary.supplier.OSCIStoreDelivery;
import org.apache.log4j.Logger;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JMSNotifyer extends NotificationAdapter {
    private static final Logger LOG = Logger.getLogger(JMSNotifyer.class);

    private TopicSession topicSession;
    private TopicPublisher topicSender;
    private final OSCIContext context;

    public JMSNotifyer(OSCIContext context) {
        this.context = context;
        if ("true".equals(context.getProperty("jms.notification.disabled"))) {
            LOG.info("JMS Notifications have been disabled. Not trying to get ConnectionFactory.");
            return;
        }

        try {
            final InitialContext jndiContext = new InitialContext();

            try {
                final String connectionFactory = this.context.getProperty("jms.notification.connection-factory", "javax/jms/TopicConnectionFactory");
                final TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)jndiContext.lookup(connectionFactory);

                TopicConnection topicConnection = topicConnectionFactory.createTopicConnection();
                topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
                Topic topic = (Topic)jndiContext.lookup(this.context.getProperty("jms.notification.topic", "topic/osciNotificationTopic"));
                topicSender = topicSession.createPublisher(topic);
            } finally {
                jndiContext.close();
            }
        } catch (NamingException e) {
            LOG.error("Cannot get JMS ConnectionFactory", e);
        } catch (JMSException e) {
            LOG.error("Cannot set up JMS communication", e);
        }
    }

    public void notifyStoreDelivery(OSCIStoreDelivery delivery, OSCIResponseToStoreDelivery response) {
        if ("true".equals(context.getProperty("jms.notification.disabled"))) {
            if (LOG.isInfoEnabled()) {
                LOG.info("JMS Notifications have been disabled.");
            }
            return;
        }
        if (topicSession == null || topicSender == null) {
            LOG.error(String.format("Invalid JMS configuration: %s, %s Please check application server configuration.", topicSession, topicSender));
            return;
        }
        try {
            final Message rm = topicSession.createMessage();
            rm.setStringProperty("NotificationType", "osci");
            rm.setStringProperty("addressee", delivery.getAddressee().getSubjectDN().getName());
            rm.setStringProperty("originator", delivery.getCertificates().getCipherCertificateOriginator().getSubjectDN().getName());
            topicSender.publish(rm);
        } catch (JMSException e) {
            LOG.error("Error notifying JMS Topic", e);
        }
    }
}