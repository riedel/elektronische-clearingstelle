/*
 * Created by IntelliJ IDEA.
 * User: sweinreuter
 * Date: 09.09.2004
 * Time: 16:35:00
 */
package cs.impl.osci;

import cs.api.*;
import cs.api.directory.*;
import cs.api.logging.LogMessage;
import cs.api.transport.osci.OSCIInfo;
import cs.api.transport.osci.OSCIMessage;
import cs.api.transport.osci.OSCIRoutingInfo;
import cs.impl.*;
import cs.impl.config.ConfigException;
import cs.impl.config.Configuration;
import cs.impl.config.ConfigurationImpl;
import cs.impl.util.GenericMBeanWrapper;
import cs.impl.util.Tracing;
import cs.persistence.monitoring.ejb.client.ProblemMessageVO;
import de.esslingen.mediakomm.osci.impl.transport.interfaces.OSCIDocumentMessage;
import de.esslingen.mediakomm.osci.impl.transport.utils.Base64;
import de.esslingen.mediakomm.osci.transport.OSCIContentContainer;
import de.esslingen.mediakomm.osci.transport.OSCIEncryptedData;
import de.esslingen.mediakomm.osci.transport.exceptions.OSCIException;
import de.esslingen.mediakomm.osci.transport.user.OSCIInspectionReport;
import de.esslingen.mediakomm.osci.transport.user.OSCINonIntermediaryCertificates;
import de.esslingen.mediakomm.osci.transport.user.OSCIProcessCard;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.security.cert.X509Certificate;

public abstract class OSCIHandler {
    private static final Logger LOG = Logger.getLogger(OSCIHandler.class);
    private static final Logger MESSAGE_LOG = Logger.getLogger("cs.impl.INVALID_MESSAGE_LOG");

    public static final String ID_PREFIX = "osci.incoming.";

    private final ClearingstelleImplMBean clearingstelle;
    protected final Configuration config;

    public static boolean isClientConnectorCert(CryptoProvider provider, X509Certificate addrCert) {
        try {
            final IClientConnector o = (IClientConnector)GenericMBeanWrapper.create(new ObjectName("cs.client.server:service=ClientConnector"), IClientConnector.class);
            if (o.isConnected()) {
                final String certAliasOrDN = o.getCertificate();
                if (certAliasOrDN != null && certAliasOrDN.length() > 0) {
                    final X509Certificate certificate = provider.getCipherCertificate(certAliasOrDN);
                    if (equals(addrCert, certificate)) {
                        return true;
                    }
                }
            }
        } catch (InstanceNotFoundException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("ClientConnector not bound", e);
            } else {
                LOG.info("ClientConnector not bound.");
            }
        } catch (Exception e) {
            LOG.error("Failed to check if <" + addrCert.getSubjectDN().getName() + "> belongs to ClientConnector", e);
        }
        return false;
    }

    private interface IClientConnector {
        String getCertificate();

        boolean isConnected();
    }

    public static class AllContent {
        public final OSCIContentContainer[] contentContainers;
        public final OSCIEncryptedData[] encryptedData;

        protected AllContent(OSCIContentContainer[] contentContainers, OSCIEncryptedData[] encryptedData) {
            this.contentContainers = contentContainers;
            this.encryptedData = encryptedData;
        }
    }

    OSCIHandler() {
        this.clearingstelle = ClearingstelleImpl.getInstance();
        this.config = ConfigurationImpl.getInstance();
    }

    protected Response handleContent(OSCIDocumentMessage osciMsg, OSCIProcessCard processCard, OSCINonIntermediaryCertificates certificates, X509Certificate intermediaryCertificate, AllContent content, boolean signalError) throws OSCIException, IOException {
        Tracing.startOperation("Process OSCI Message");
        org.apache.log4j.NDC.push("OSCI-Message [ID=" + processCard.getMessageId() + "]");
        Exception ex;
        try {
            return _handleContent(osciMsg, processCard, certificates, intermediaryCertificate, content);
        } catch (NoAccessException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Message has been rejected due to insufficient access privileges", e);
            } else {
                LOG.info("Message has been rejected due to insufficient access privileges");
            }
            clearingstelle.notifyException(ex = e);
        } catch (InvalidMessageException e) {
            LOG.fatal("Received Message is not a valid message", e);
            clearingstelle.notifyException(ex = e);
        } catch (UnknownTypeException e) {
            LOG.fatal("Received Message is of unknown type", e);
            clearingstelle.notifyException(ex = e);
        } catch (ConnectorException e) {
            LOG.fatal("The receiving connector had a problem while handling the message", e);
            clearingstelle.notifyException(ex = e);
        } catch (UnknownDestinationException e) {
            LOG.fatal("The received message could not be handled by a local connector", e);
            clearingstelle.notifyException(ex = e);
        } catch (CsException e) {
            LOG.fatal("Problem while handling the message", e);
            clearingstelle.notifyException(ex = e);
        } catch (ConfigException e) {
            LOG.fatal("Configuration problem while handling the message", e);
            clearingstelle.notifyException(ex = e);
        } catch (OSCIException e) {
            LOG.fatal("Error in OSCI Communication", e);
            clearingstelle.notifyException(ex = e);
        } catch (SOAPException e) {
            LOG.fatal("Error in OSCI Communication", e);
            clearingstelle.notifyException(ex = e);
        } finally {
            org.apache.log4j.NDC.pop();
            Tracing.endOperation("Process OSCI Message");
        }
        if (signalError) {
            throw new OSCIException(ex); // TODO: change this when there are error responses defined
        } else {
            return null;
        }
    }

    protected Response _handleContent(OSCIDocumentMessage osciMsg, OSCIProcessCard processCard, OSCINonIntermediaryCertificates certificates, X509Certificate intermediaryCertificate, AllContent content) throws OSCIException, ConnectorException, CsException, IOException, ConfigException, SOAPException {

        final String messageId = processCard.getMessageId();
        final Utils.ByteContent _bytes = extractContent(content, messageId, osciMsg);
        if (_bytes == null) {
            return null;
        }

        // TODO: Hack, see cs.impl.osci.Utils.extractContent()
        final X509Certificate[] signers = content.contentContainers[0].getSignerCertificates();

        final byte[] bytes = _bytes.getBytes();
        MessageWrapper message = null;
        try {
            message = MessageWrapper.createMessage(bytes);
            message.setMessageId(messageId);

            final Message msg = message.getMessage();
            if (msg instanceof OSCIRoutingInfo) {
                final OSCIRoutingInfo routingInfo = (OSCIRoutingInfo)msg;
                final String requiredId = routingInfo.getContainerId();
                if (requiredId != null && !requiredId.equals(content.contentContainers[0].getId())) {
                    LOG.warn("Message type '" + msg.getType() + "' requires ContentContainer ID '" + requiredId + "' but got '" + content.contentContainers[0].getId() + "'");
                }
            }

            final OSCIMessage osciMessage = checkAccess(message, certificates, signers, processCard, intermediaryCertificate);

            if (!_bytes.isValidSignature()) {
                handleInvalidMessage(osciMessage, ErrorCode.XMELD_T011, null, messageId, null);
                return null;
            }

            if (!message.isValid()) {
                LOG.warn("Got an invalid message [Type: " + msg.getType() + "] with Id = " + messageId);
                MESSAGE_LOG.warn("Invalid message content for <" + messageId + "> follows:");

                final String messageText = Utils.fromUTF8(bytes);
                MESSAGE_LOG.warn(messageText);

                final NotificationMail mail = new NotificationMail(ErrorCode.XMELD_X001, "Nachricht mit ung�ltigem Inhalt empfangen")
                        .addContent("Die Nachricht vom Typ '" + msg.getType() + "' " +
                                "mit der Nachrichten-ID '" + messageId + "' ist laut Schemapr�fung inhaltlich ung�ltig.\n" +
                                "Der Inhalt der Nachricht befindet sich im Anhang.")
                        .addContent(message.getValidityError())
                        .addAttachment("Nachricht.xml", "text/xml", messageText);

                Statistics.message(ErrorCode.XMELD_X001, messageId);

                if (config.isRelaxedInvalidMessagePolicy()) {
                    LOG.info("Processing of invalid message continues. See JMX setting RelaxedInvalidMessagePolicy");
                    mail.send();
                } else {
                    handleInvalidMessage(osciMessage, ErrorCode.XMELD_X001, message.getValidityError(), messageId, mail);
                    return null; // TODO: return a proper error message;
                }
            }

            final int mode;
            if (Boolean.getBoolean("ecs.osci.legacy-xta-certificate-routing")) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Legacy XTA certificate routing enabled. Message might not be delivered to destination matching the recipient's certificate");
                }
                mode = Clearingstelle.MODE_LOCAL_ONLY;
            } else {
                final CryptoProvider cryptoProvider = (CryptoProvider)config.getClientContext(config.getImURL(), config.getImAlias(), null).getCryptographyProvider();
                final X509Certificate ecsCert = cryptoProvider.getCipherCertificate();
                final X509Certificate addrCert = osciMessage.getOsciInfo().getLocalCertificate();
                if (equals(ecsCert, addrCert)) {
                    mode = Clearingstelle.MODE_LOCAL_ONLY;
                } else if (isClientConnectorCert(cryptoProvider, addrCert)) {
                    mode = Clearingstelle.MODE_STANDARD | Clearingstelle.MODE_NO_XTA;
                    final OSCIInfo osciInfo = osciMessage.getOsciInfo();
                    final OSCIInfo newInfo = new OSCIInfo(osciInfo.getMessageId(), osciInfo.getSubject(), osciInfo.getCreation(), osciInfo.getClientCertificate(), osciInfo.getIntermedCertificate(), osciInfo.isSigned(), ecsCert);

                    return new Response(osciMessage, clearingstelle.handleSendMessage(getID(), new OSCIMessage(msg, newInfo, osciMessage.getAccessDescriptor()), mode, null).getResponse());
                } else {
                    final String name = addrCert.getSubjectDN().getName();
                    final XtaConfig.Account[] accounts = config.getDirectoryServiceImpl().getXtaAccountConfig().getAccounts(name);
                    if (accounts.length > 0) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Got message belonging to XTA account's certfificate: " + name + ". Handling with MODE_XTA to ensure delivery to XTA");
                        }
                        mode = Clearingstelle.MODE_XTA;
                    } else {
                        LOG.warn("Got OSCI message that neither belongs to eCS itself nor an XTA account: " + name);
                        mode = Clearingstelle.MODE_LOCAL_ONLY | Clearingstelle.MODE_NO_XTA;
                    }
                }
            }

            return new Response(osciMessage, clearingstelle.handleSendMessage(getID(), osciMessage, mode, null).getResponse());
        } catch (UnknownDestinationException e) {
            final String messageText = Utils.fromUTF8(bytes);

            new NotificationMail(ErrorCode.UNKNOWN_DESTINATION, "Nicht zustellbare Nachricht empfangen")
                    .addContent("Die Nachricht vom Typ '" + message.getMessage().getType() + "' " +
                            "mit der Nachrichten-ID '" + messageId + "' konnte keinem lokalen Plugin zugestellt werden.\n" +
                            "Der Inhalt der Nachricht befindet sich im Anhang.")
                    .addContent(message.getValidityError())
                    .addAttachment("Nachricht.xml", "text/xml", messageText)
                    .send();

            Statistics.message(LogMessage.Direction.INCOMING, ErrorCode.UNKNOWN_DESTINATION, messageId, message.getMessage());

            throw e;
        } catch (UnknownTypeException e) {
            MESSAGE_LOG.warn("Message of unknown type with Id <" + messageId + "> follows:");

            final String messageText = Utils.fromUTF8(bytes);
            MESSAGE_LOG.warn(messageText);

            new NotificationMail(ErrorCode.UNKNOWN_TYPE, "Nachricht mit unbekanntem Typ empfangen")
                    .addContent("Der Typ der Nachricht mit der ID '" + messageId + "' konnte nicht ermittelt werden.\n" +
                            "Der Inhalt der Nachricht befindet sich im Anhang.")
                    .addContent(e)
                    .addAttachment("Nachricht.xml", "text/xml", messageText)
                    .send();

            ProblemMessageUtil.createMessage(getID(), null, messageId, bytes, ProblemMessageVO.Type.XML,
                    ProblemMessageVO.Direction.INCOMING, ErrorCode.UNKNOWN_TYPE, e);

            Statistics.message(ErrorCode.UNKNOWN_TYPE, messageId);

            throw e;
        } catch (DocumentException e) {
            MESSAGE_LOG.warn("Non well-formed XML content for <" + messageId + "> follows (base64 encoded):");
            MESSAGE_LOG.warn(Base64.encodeBytes(bytes));

            new NotificationMail(ErrorCode.XMELD_X000, "Nachricht mit ung�ltiger XML-Struktur empfangen")
                    .addContent("Die Nachricht mit der ID '" + messageId + "' enth�lt nicht wohlgeformtes XML.\n" +
                            "Der Inhalt der Nachricht befindet sich im Anhang.")
                    .addContent(e)
                    .addAttachment("Nachricht.dat", bytes)
                    .send();

            ProblemMessageUtil.createMessage(getID(), null, messageId, bytes, ProblemMessageVO.Type.UNKNOWN,
                    ProblemMessageVO.Direction.INCOMING, ErrorCode.XMELD_X000, e);

            Statistics.message(ErrorCode.XMELD_X000, messageId);

            throw new InvalidMessageException(e, bytes);
        }
    }

    protected Utils.ByteContent extractContent(AllContent content, String messageId, OSCIDocumentMessage osciMsg) throws IOException, OSCIException, SOAPException {
        if (content.contentContainers.length == 0) {
            if (content.encryptedData.length > 0) {
                LOG.warn("The data contained in the message <" + messageId + "> " +
                        "cannot be decrypted by this client. Returning empty response");

                new NotificationMail(ErrorCode.UNDECRYPTABLE_DATA, "OSCI Nachrichteninhalt nicht entschl�sselbar")
                        .addContent("Der Inhalt der Nachricht mit der ID '" + messageId + "' kann nicht entschl�sselt werden.\n" +
                                "Der Inhalt der OSCI-Nachricht befindet sich im Anhang.")
                        .addAttachment(osciMsg.getDocument().marshal())
                        .send();

                ProblemMessageUtil.createMessage(getID(), null, messageId, osciMsg,
                        ProblemMessageVO.Direction.INCOMING, ErrorCode.UNDECRYPTABLE_DATA, null);

                Statistics.message(ErrorCode.UNDECRYPTABLE_DATA, messageId);
            } else {
                LOG.warn("The message <" + messageId + "> doesn't contain any content at all. Returning empty response");

                new NotificationMail(ErrorCode.NO_DATA, "OSCI Nachricht ohne Inhalt empfangen")
                        .addContent("Die Nachricht mit der ID '" + messageId + "' enth�lt keine verarbeitbaren Daten.\n" +
                                "Der Inhalt der OSCI-Nachricht befindet sich im Anhang.")
                        .addAttachment(osciMsg.getDocument().marshal())
                        .send();

                Statistics.message(ErrorCode.NO_DATA, messageId);
            }
            return null;
        }

        Utils.ByteContent _bytes = Utils.extractContent(content.contentContainers, true);
        if (_bytes == null) {
            LOG.warn("Could not extract any content from the message <" + messageId + ">. Returning empty response");

            new NotificationMail(ErrorCode.NO_DATA, "OSCI Nachricht ohne Inhalt empfangen")
                    .addContent("Die Nachricht mit der ID '" + messageId + "' enth�lt keine verarbeitbaren Daten.\n" +
                            "Der Inhalt der OSCI-Nachricht befindet sich im Anhang.")
                    .addAttachment(osciMsg.getDocument().marshal())
                    .send();

            Statistics.message(ErrorCode.NO_DATA, messageId);

            return null;
        }
        return _bytes;
    }

    /** @noinspection ReturnOfNull */
    private OSCIMessage checkAccess(MessageWrapper wrapper, OSCINonIntermediaryCertificates certificates, X509Certificate[] signers, OSCIProcessCard processCard, X509Certificate intermediaryCertificate) throws ConfigException, NoAccessException {
        // TODO: access lookup involved DVDV requests that can fail and message should be put into
        final DirectoryService directoryService = config.getDirectoryServiceImpl();

        final X509Certificate origCert =
                certificates.getSignatureCertificateOriginator() != null ? certificates.getSignatureCertificateOriginator() :
                        certificates.getCipherCertificateOriginator();
        final X509Certificate addressee = certificates.getCipherCertificateAddressee();

        final Message msg = wrapper.getMessage();

        AccessDescriptor accessDescriptor;
        OSCIMessage osciMessage = null;
        if (signers != null && signers.length > 0) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Message is signed. Checking access...");
            }
            final StringBuilder sb = new StringBuilder();
            for (final X509Certificate signer : signers) {
                sb.append("[").append(signer.getSubjectDN().getName()).append("]");

                accessDescriptor = directoryService.checkAccess(msg, signer);
                final boolean b = accessDescriptor.isAccessAllowed();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("  - SubjId: " + signer.getSubjectDN().getName() + ": " + b + " " + accessDescriptor);
                }

                OSCIInfo info = createInfo(processCard, signer, intermediaryCertificate, true, addressee);
                osciMessage = new OSCIMessage(msg, info, accessDescriptor);
                if (b) {
                    if (signer != origCert) {
                        if (!checkInspections(osciMessage, processCard, origCert)) {
                            throw new NoAccessException(origCert != null ? origCert.getSubjectDN().getName() : "<unknown>", msg);
                        }
                        if (!checkInspections(osciMessage, processCard, signer)) {
                            throw new NoAccessException(signer.getSubjectDN().getName(), msg);
                        }
                    } else if (!AccessDescriptor.EXPLICITELY_KNOWN.equals(accessDescriptor)) {
                        if (!checkInspections(osciMessage, processCard, signer)) {
                            throw new NoAccessException(signer.getSubjectDN().getName(), msg);
                        }
                    }
                    return osciMessage;
                }
            }
            assert osciMessage != null;

            handleInvalidMessage(osciMessage, ErrorCode.XMELD_T070, null, processCard.getMessageId(), null);

            throw new NoAccessException(sb.toString(), msg);
        } else if ((accessDescriptor = directoryService.checkAccess(msg, origCert)).isUnrestricted()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Message is not signed, but unrestricted access is allowed: " + accessDescriptor);
            }

            final OSCIInfo info = createInfo(processCard, origCert, intermediaryCertificate, false, addressee);
            return new OSCIMessage(msg, info, accessDescriptor);
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info("Message is not signed. Access is denied.");
            }

            final OSCIInfo info = createInfo(processCard, origCert, intermediaryCertificate, false, addressee);
            osciMessage = new OSCIMessage(msg, info, accessDescriptor);

            handleInvalidMessage(osciMessage, ErrorCode.XMELD_T010, null, processCard.getMessageId(), null);

            throw new NoAccessException(origCert.getSubjectDN().getName(), msg);
        }
    }

    private boolean checkInspections(Message msg, OSCIProcessCard processCard, X509Certificate cert) {
        if (cert == null) {
            LOG.error("Certificate is missing");
            handleInvalidMessage(msg, ErrorCode.XMELD_T003, null, processCard.getMessageId(), null);
            return false;
        }
        final OSCIInspectionReport.Inspection inspection = processCard.getInspectionReport().getInspection(cert);
        final String dn = cert.getSubjectDN().getName();
        if (inspection != null) {
            if (!inspection.getOfflineResult().isOK()) {
                LOG.error("Certificate <" + dn + "> is expired");
                handleInvalidMessage(msg, ErrorCode.XMELD_T000, null, processCard.getMessageId(), null);
                return false;
            }
            final OSCIInspectionReport.OnlineResult onlineResult = inspection.getOnlineResult();
            if (onlineResult != null && !onlineResult.isOK()) {
                LOG.error("Certificate <" + dn + "> is revoked");
                handleInvalidMessage(msg, ErrorCode.XMELD_T001, null, processCard.getMessageId(), null);
                return false;
            }
            if (!inspection.getMathResult().isOK()) {
                LOG.error("Certificate <" + dn + "> is invalid");
                handleInvalidMessage(msg, ErrorCode.XMELD_T002, null, processCard.getMessageId(), null);
                return false;
            }
        } else {
            LOG.warn("No OSCI inspection for <" + dn + ">");
        }
        return true;
    }

    private void handleInvalidMessage(Message message, ErrorCode errorCode, Throwable exception, String msgId, NotificationMail mail) {
        try {
            if (mail == null) {
                final String messageText = Utils.fromUTF8(message.getMessage());
                mail = new NotificationMail(errorCode, "Nicht zugestellte Nachricht")
                        .addContent("Die Nachricht vom Typ '" + message.getType() + "' " +
                                "mit der Nachrichten-ID '" + msgId + "' wurde NICHT zugestellt.\n" +
                                "M�gliche Gr�nde: Nicht-valides XML, Probleme mit Absender-Signatur bzw. -Zertifikaten\n\n" +
                                "Der Inhalt der Nachricht befindet sich im Anhang.")
                        .addContent(exception)
                        .addAttachment("Nachricht.xml", "text/xml", messageText);
            }

            boolean b;
            try {
                b = clearingstelle.handleInvalidMessage(message, errorCode, exception);
                Statistics.message(LogMessage.Direction.INCOMING, errorCode, msgId, message);
            } catch (RejectedMessageException e) {
                b = true;
                Statistics.message(LogMessage.Direction.INCOMING_REJECTED, e.getCode() != null ? e.getCode() : errorCode, msgId, message);
            }

            if (!b) {
                mail.send();
                ProblemMessageUtil.createMessage(getID(), null, message, ProblemMessageVO.Direction.INCOMING, errorCode, exception);
            } else {
                LOG.info("Invalid message [" + getID() + ": " + errorCode + "] handled, not going into monitoring");
            }
        } catch (ConnectorException e) {
            LOG.error("Could not handle invalid message [" + errorCode + "]", e);
            mail.send();
            ProblemMessageUtil.createMessage(getID(), null, message, ProblemMessageVO.Direction.INCOMING, errorCode, exception);
        }
    }

    private OSCIInfo createInfo(final OSCIProcessCard processCard, final X509Certificate clientCert, X509Certificate intermediaryCertificate, final boolean signed, X509Certificate addressee) {
        return new OSCIInfo(
                processCard.getMessageId(),
                processCard.getSubject(),
                processCard.getCreation().getDate(),
                clientCert,
                intermediaryCertificate,
                signed,
                addressee);
    }

    protected abstract String getID();

    protected static boolean equals(X509Certificate addrCert, X509Certificate cipherCertificate) {
        return addrCert != null && addrCert.getSerialNumber().equals(cipherCertificate.getSerialNumber()) && addrCert.getIssuerDN().equals(cipherCertificate.getIssuerDN());
    }

    protected class Response {
        final Message origMessage;
        final byte[] responseBytes;

        Response(Message orig, byte[] response) {
            this.origMessage = orig;
            this.responseBytes = response;
        }
    }
}

